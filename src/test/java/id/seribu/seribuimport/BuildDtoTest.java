package id.seribu.seribuimport;

import org.junit.Test;

import id.seribu.seribudto.demografi.DemografiDto;
import id.seribu.seribudto.demografi.op.OpWilayahDtoBuilder;
import id.seribu.seribumodel.transaction.TDemoOpWilayah;

public class BuildDtoTest {

    @Test
    public void testBuildDemoOpWilayahDto() {
        DemografiDto demoDto = new OpWilayahDtoBuilder().id().tahun("2019").build();
        TDemoOpWilayah tDemoOpWilayah = new TDemoOpWilayah();
        tDemoOpWilayah.setId(demoDto.getId());
        System.out.println("data " + tDemoOpWilayah.getId());

    }
}