package id.seribu.seribuimport;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.util.ExcelUtil;

public class ExcelUtilTest {

    ExcelUtil excelUtil = new ExcelUtil();
    Date date;    

    @Before
    public void setup() {
        date = new Date();
    }

    @Test
    public void testFormatDate() {
        String formattedDate = excelUtil.formatDate(date, ImportConstant.EXCEL_FORMAT_DATE);
        System.out.println(formattedDate);
    }
}