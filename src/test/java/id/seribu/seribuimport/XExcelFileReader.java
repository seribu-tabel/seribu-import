package id.seribu.seribuimport;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * XSSF and XML Stream Reader
 * 
 * If memory footprint is an issue, then for XSSF, you can get at the underlying
 * XML data, and process it yourself. This is intended for intermediate
 * developers who are willing to learn a little bit of low level structure of
 * .xlsx files, and who are happy processing XML in java. Its relatively simple
 * to use, but requires a basic understanding of the file structure. The
 * advantage provided is that you can read a XLSX file with a relatively small
 * memory footprint.
 * 
 * @author lchen
 * 
 */
@Slf4j
public class XExcelFileReader {
    private int rowNum = 1;
    private OPCPackage opcPkg;
    private ReadOnlySharedStringsTable stringsTable;
    private XMLStreamReader xmlReader;
    private Map<Integer, String> rowHeaderMap = new HashMap<>();

    public XExcelFileReader(String excelPath) throws Exception {
        opcPkg = OPCPackage.open(excelPath, PackageAccess.READ);
        this.stringsTable = new ReadOnlySharedStringsTable(opcPkg);

        XSSFReader xssfReader = new XSSFReader(opcPkg);
        XMLInputFactory factory = XMLInputFactory.newInstance();
        InputStream inputStream = xssfReader.getSheetsData().next();
        xmlReader = factory.createXMLStreamReader(inputStream);

        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals("sheetData"))
                    break;
            }
        }
    }

    public int rowNum() {
        return rowNum;
    }

    // public List<String[]> readRows(int batchSize) throws XMLStreamException {
    public List<Map<String, String>> readRows() throws XMLStreamException {
        String elementName = "row";
        List<Map<String, String>> dataRows = new ArrayList<>();
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals(elementName)) {
                    log.info("read data {}", rowNum);
                    if (rowNum > 1) {
                        dataRows.add(getDataRow());
                    } else {
                        getDataRow();
                    }
                    rowNum++;
                }
            }
        }
        return dataRows;
    }

    private Map<String, String> getDataRow() throws XMLStreamException {
        int rowCell = 1;
        // List<String> rowValues = new ArrayList<String>();
        Map<String, String> rowValueMap = new HashMap<>();
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals("c")) {
                    // CellReference cellReference = new CellReference(xmlReader.getAttributeValue(null, "r"));
                    // Fill in the possible blank cells!
                    // while (rowValueMap.size() < cellReference.getCol()) {
                    //     rowValueMap.put(rowHeaderMap.get(rowCell), "");
                    // }
                    String cellType = xmlReader.getAttributeValue(null, "t");
                    // set header
                    if (rowNum == 1) {
                        rowHeaderMap.put(rowCell, getCellValue(cellType).toUpperCase().replace(" ", "_"));
                    } else {
                        rowValueMap.put(rowHeaderMap.get(rowCell), getCellValue(cellType));
                    }
                    // rowValues.add(getCellValue(cellType));
                    rowCell++;
                }
            } else if (xmlReader.isEndElement() && xmlReader.getLocalName().equals("row")) {
                break;
            }
        }
        // return rowValues.toArray(new String[rowValues.size()]);
        return rowValueMap;
    }

    private String getCellValue(String cellType) throws XMLStreamException {
        String value = ""; // by default
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals("v")) {
                    if (cellType != null && cellType.equals("s")) {
                        int idx = Integer.parseInt(xmlReader.getElementText());
                        return stringsTable.getItemAt(idx).toString();
                        // return new XSSFRichTextString(stringsTable.getEntryAt(idx)).toString();
                    } else {
                        return xmlReader.getElementText();
                    }
                }
            } else if (xmlReader.isEndElement() && xmlReader.getLocalName().equals("c")) {
                break;
            }
        }
        return value;
    }

    @Override
    protected void finalize() throws Throwable {
        if (opcPkg != null)
            opcPkg.close();
        super.finalize();
    }

}