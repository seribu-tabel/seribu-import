package id.seribu.seribuimport;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import id.seribu.seribuimport.constant.HeaderEnum;
import id.seribu.seribumodel.transaction.TDemoOpWilayah;
import id.seribu.seribumodel.transaction.TDemoOpWilayahKlu;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

// @RunWith(SpringRunner.class)
// @SpringBootTest(classes = {MathUtil.class })
public class ExcelPivotTest {

    @Test
    public void firstTryStreamGroup() {
        List<String> list = new ArrayList<>();

        list.add("Hello");
        list.add("Hello");
        list.add("World");

        Map<String, Long> counted = list.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(counted);
    }

    @Test
    public void testPivotObject() {
        List<TDemoOpWilayah> listData = this.generateObject();
        Map<String, Map<String, Long>> pivot = listData.stream()
                .collect(Collectors.groupingBy(TDemoOpWilayah::getKanwil,
                        Collectors.groupingBy(TDemoOpWilayah::getJumlahTanggungan, Collectors.counting())));
        System.out.println(pivot);
    }

    @Test
    public void testPivotToListObject() {
        List<TDemoOpWilayah> listData = this.generateObject();
        Map<String, List<TDemoOpWilayah>> pivot = listData.stream()
                .collect(Collectors.groupingBy(TDemoOpWilayah::getKanwil));
        System.out.println(pivot);
    }

    @Data
    @NoArgsConstructor
    @RequiredArgsConstructor
    public class ObjectMapping {
        @NonNull
        private Object rows;
        @NonNull
        private PivotColumns columns;
        @NonNull
        private Long summary;

    }

    private ObjectMapping pivotTable(int columnIdx, int summaryIdx, Object[] arrObj, int... rowIdx) {
        List<List<Object>> listData = new ArrayList<>();
        ObjectMapping objectMapping = new ObjectMapping();
        listData.add(Arrays.asList(arrObj));
        Map<Object, Map<Object, IntSummaryStatistics>> pivotData = listData.stream().collect(
                Collectors.groupingBy(r -> r.get(columnIdx), Collectors.groupingBy(r -> new PivotColumns(r, rowIdx),
                        Collectors.summarizingInt(r -> (Integer) r.get(summaryIdx)))));
        objectMapping.setRows(pivotData.entrySet().iterator().next().getKey());
        objectMapping.setColumns(((PivotColumns) pivotData.entrySet().iterator().next().getValue().entrySet().iterator()
                .next().getKey()));
        objectMapping.setSummary(
                pivotData.entrySet().iterator().next().getValue().entrySet().iterator().next().getValue().getSum());

        return objectMapping;
    }

    @Test
    public void testSimplePivot() {
        List<TDemoOpWilayah> listModel = new ArrayList<>();
        TDemoOpWilayah model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setPulau("JAWA");
        model.setJumlahTanggungan(">3");
        model.setJumlah(4);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2016");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setPulau("JAWA");
        model.setJumlahTanggungan("1");
        model.setJumlah(10);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA UTARA");
        model.setPulau("JAWA");
        model.setJumlahTanggungan(">2");
        model.setJumlah(5);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA UTARA");
        model.setPulau("JAWA");
        model.setJumlahTanggungan(">5");
        model.setJumlah(50);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setPulau("JAWA");
        model.setJumlahTanggungan("8");
        model.setJumlah(20);
        listModel.add(model);
        // List<Object[]> listArray = new ArrayList<>();
        // System.out.println(this.groupListObjectArrayBy(convertModelToExcelPivot(listModel),
        // new String[] { "pulau", "kanwil" },
        // "jumlahTanggungan"));
        writeData(this.groupListObjectArrayBy(convertModelToExcelPivot(listModel),
                new String[] { "tahun", "pulau", "kanwil" }, "jumlahTanggungan"));
        // for (TDemoOpWilayah tDemoOpWilayah : listModel) {
        // listArray.add(this.modelToObjectArray(tDemoOpWilayah));
        // }
        // Object[] arrayField = new Object[] {4,6};
        // this.pivotMultiple(Arrays.asList(arrayField) , 13, listArray, 8);
        // System.out.println(this.groupListObjectBy(listModel, new String[] { "pulau"
        // }));
        // for(PivotDataTest pivot : convertModelToExcelPivot(listModel)){
        // System.out.println(pivot.toString());
        // }
    }

    @Test
    public void testWriteParentHeader() {
        String[] rowPivot = new String[] { "tahun", "pulau", "kanwil" };
        String columnPivot = "jumlahTanggungan";
        Object [] data = new Object[rowPivot.length+1];
        int i = 0;
        for (String obj : rowPivot) {
            data[i++] = HeaderEnum.valueOf(obj).getText();
        }
        data[i] = HeaderEnum.valueOf(columnPivot).getText();

        for (int j = 0; j < data.length; j++) {
            System.out.println("[" + j + "] : " + data[j]);
        }
    }

    private void writeData(Map<List<String>, Map<Object, IntSummaryStatistics>> pivotData) {
        Set<Object> columnSet = new HashSet<>();
        List<Object> listObject = new ArrayList<>();
        List<Object[]> listArrayObjects = new ArrayList<>();
        List<Long> stats = new ArrayList<>();
        int total = 0;
        Integer cellData = 0;
        boolean flagZero = true;

        
        for (final Map.Entry<List<String>, Map<Object, IntSummaryStatistics>> entry : pivotData.entrySet()) {
            columnSet.addAll(entry.getValue().keySet());
        }

        // fetch each row data
        for (final Map.Entry<List<String>, Map<Object, IntSummaryStatistics>> entry : pivotData.entrySet()) {
            total = 0;
            listObject = new ArrayList<>();
            listObject.addAll(entry.getKey());
            for (Object oHead : columnSet) {
                int j = 0;
                for (Object obj : entry.getValue().keySet()) {
                    stats = entry.getValue().values().stream().map(m -> m.getSum()).collect(Collectors.toList());
                    cellData = Math.toIntExact(stats.get(j));
                    if (oHead.equals(obj)) {
                        flagZero = false;
                        break;
                    } else {
                        flagZero = true;
                    }
                    j++;
                }
                if (flagZero) {
                    listObject.add(0);
                    cellData = 0;
                } else {
                    listObject.add(cellData);
                }
                total += cellData;
            }
            listObject.add(total);
            // store row
            listArrayObjects.add(listObject.toArray());

        }

        // print header
        for (Object oHead : columnSet) {
            System.out.println("header: " + oHead);
        }

        // print row
        for (int i = 0; i < listArrayObjects.size(); i++) {
            Object[] obj = listArrayObjects.get(i);
            for (int j = 0; j < obj.length; j++) {
                System.out.println(obj[j]);
            }
            System.out.println("=== new row ===");
        }

        // for (Object obj : columnSet) {
        // System.out.println("header : " + obj);
        // }

        // for (final Map.Entry<List<String>, Map<Object, IntSummaryStatistics>> entry :
        // pivotData.entrySet()) {
        // listObject.addAll(entry.getKey());
        // total = 0;
        // for(Object oHead : columnSet) {
        // int j = 0;
        // for (Object obj : entry.getValue().keySet()) {
        // stats = entry.getValue().values().stream().map(m ->
        // m.getSum()).collect(Collectors.toList());
        // cellData = Math.toIntExact(stats.get(j));
        // if(oHead.equals(obj)) {
        // flagZero = false;
        // break;
        // } else {
        // flagZero = true;
        // }
        // j++;
        // }
        // if(flagZero) {
        // listObject.add(0);
        // cellData = 0;
        // } else {
        // listObject.add(cellData);
        // }
        // total += cellData;
        // }
        // listObject.add(total);
        // // store row
        // listArrayObjects.add(listObject.toArray());
        // }

        // for (int i = 0; i < listArrayObjects.size(); i++) {
        // Object[] obj = listArrayObjects.get(i);
        // for (int j = 0; j < obj.length; j++) {
        // System.out.println(obj[j]);
        // }
        // System.out.println("=== new row ===");
        // }

    }

    private Set<String> getAllFields(final Class<?> type) {
        Set<String> fields = new HashSet<String>();
        // loop the fields using Java Reflections
        for (Field field : type.getDeclaredFields()) {
            fields.add(field.getName());
        }

        // recursive call to getAllFields
        if (type.getSuperclass() != null) {
            fields.addAll(getAllFields(type.getSuperclass()));
        }
        return fields;
    }

    private <T> List<PivotDataTest> convertModelToExcelPivot(List<T> t) {
        List<PivotDataTest> listExcelData = new ArrayList<>();
        listExcelData = t.stream().map(dt -> {
            PivotDataTest pivotDataTest = new PivotDataTest();
            try {
                Set<String> fields = new HashSet<>();
                fields = getAllFields(dt.getClass());
                Field fieldTahun = fields.contains("tahun") ? dt.getClass().getDeclaredField("tahun") : null;
                Field fieldKodeKpp = fields.contains("kodeKpp") ? dt.getClass().getDeclaredField("kodeKpp") : null;
                Field fieldNamaKpp = fields.contains("namaKpp") ? dt.getClass().getDeclaredField("namaKpp") : null;
                Field fieldKanwil = fields.contains("kanwil") ? dt.getClass().getDeclaredField("kanwil") : null;
                Field fieldProvinsi = fields.contains("provinsi") ? dt.getClass().getDeclaredField("provinsi") : null;
                Field fieldPulau = fields.contains("pulau") ? dt.getClass().getDeclaredField("pulau") : null;
                Field fieldKodeKategori = fields.contains("kodeKategori")
                        ? dt.getClass().getDeclaredField("kodeKategori")
                        : null;
                Field fieldNamaKategori = fields.contains("namaKategori")
                        ? dt.getClass().getDeclaredField("namaKategori")
                        : null;
                Field fieldStatusPerkawinan = fields.contains("statusPerkawinan")
                        ? dt.getClass().getDeclaredField("statusPerkawinan")
                        : null;
                Field fieldJumlahTanggungan = fields.contains("jumlahTanggungan")
                        ? dt.getClass().getDeclaredField("jumlahTanggungan")
                        : null;
                Field fieldJenisOp = fields.contains("jenisOp") ? dt.getClass().getDeclaredField("jenisOp") : null;
                Field fieldRangeUsia = fields.contains("rangeUsia") ? dt.getClass().getDeclaredField("rangeUsia")
                        : null;
                Field fieldJenisKelamin = fields.contains("jenisKelamin")
                        ? dt.getClass().getDeclaredField("jenisKelamin")
                        : null;
                Field fieldWargaNegara = fields.contains("wargaNegara") ? dt.getClass().getDeclaredField("wargaNegara")
                        : null;
                Field fieldKategoriWp = fields.contains("kategoriWp") ? dt.getClass().getDeclaredField("kategoriWp")
                        : null;
                Field fieldJenisWp = fields.contains("jenisWp") ? dt.getClass().getDeclaredField("jenisWp") : null;
                Field fieldModal = fields.contains("modal") ? dt.getClass().getDeclaredField("modal") : null;
                Field fieldStatusPusat = fields.contains("StatusPusat") ? dt.getClass().getDeclaredField("StatusPusat")
                        : null;
                Field fieldPeriodePembukuan = fields.contains("PeriodePembukuan")
                        ? dt.getClass().getDeclaredField("PeriodePembukuan")
                        : null;
                Field fieldTahunPendirian = fields.contains("tahunPendirian")
                        ? dt.getClass().getDeclaredField("tahunPendirian")
                        : null;
                Field fieldBadanHukum = fields.contains("badanHukum") ? dt.getClass().getDeclaredField("badanHukum")
                        : null;
                Field fieldJenis = fields.contains("jenis") ? dt.getClass().getDeclaredField("jenis") : null;
                Field fieldStatusNpwp = fields.contains("statusNpwp") ? dt.getClass().getDeclaredField("statusNpwp")
                        : null;
                Field fieldStatusPkp = fields.contains("statusPkp") ? dt.getClass().getDeclaredField("statusPkp")
                        : null;
                Field fieldMetodePembukuan = fields.contains("metodePembukuan")
                        ? dt.getClass().getDeclaredField("metodePembukuan")
                        : null;
                Field fieldJumlah = fields.contains("jumlah") ? dt.getClass().getDeclaredField("jumlah") : null;
                if (fieldTahun != null) {
                    fieldTahun.setAccessible(true);
                    pivotDataTest.setTahun(fieldTahun.get(dt) == null ? null : fieldTahun.get(dt).toString());
                }
                if (fieldKodeKpp != null) {
                    fieldKodeKpp.setAccessible(true);
                    pivotDataTest.setKodeKpp(fieldKodeKpp.get(dt) == null ? null : fieldKodeKpp.get(dt).toString());
                }
                if (fieldNamaKpp != null) {
                    fieldNamaKpp.setAccessible(true);
                    pivotDataTest.setNamaKpp(fieldNamaKpp.get(dt) == null ? null : fieldNamaKpp.get(dt).toString());
                }
                if (fieldKanwil != null) {
                    fieldKanwil.setAccessible(true);
                    pivotDataTest.setKanwil(fieldKanwil.get(dt) == null ? null : fieldKanwil.get(dt).toString());
                }
                if (fieldProvinsi != null) {
                    fieldProvinsi.setAccessible(true);
                    pivotDataTest.setProvinsi(fieldProvinsi.get(dt) == null ? null : fieldProvinsi.get(dt).toString());
                }
                if (fieldPulau != null) {
                    fieldPulau.setAccessible(true);
                    pivotDataTest.setPulau(fieldPulau.get(dt) == null ? null : fieldPulau.get(dt).toString());
                }
                if (fieldKodeKategori != null) {
                    fieldKodeKategori.setAccessible(true);
                    pivotDataTest.setKodeKategori(
                            fieldKodeKategori.get(dt) == null ? null : fieldKodeKategori.get(dt).toString());
                }
                if (fieldNamaKategori != null) {
                    fieldNamaKategori.setAccessible(true);
                    pivotDataTest.setNamaKategori(
                            fieldNamaKategori.get(dt) == null ? null : fieldNamaKategori.get(dt).toString());
                }
                if (fieldStatusPerkawinan != null) {
                    fieldStatusPerkawinan.setAccessible(true);
                    pivotDataTest.setStatusPerkawinan(
                            fieldStatusPerkawinan.get(dt) == null ? null : fieldStatusPerkawinan.get(dt).toString());
                }
                if (fieldJumlahTanggungan != null) {
                    fieldJumlahTanggungan.setAccessible(true);
                    pivotDataTest.setJumlahTanggungan(
                            fieldJumlahTanggungan.get(dt) == null ? null : fieldJumlahTanggungan.get(dt).toString());
                }
                if (fieldJenisOp != null) {
                    fieldJenisOp.setAccessible(true);
                    pivotDataTest.setJenisOp(fieldJenisOp.get(dt) == null ? null : fieldJenisOp.get(dt).toString());
                }
                if (fieldRangeUsia != null) {
                    fieldRangeUsia.setAccessible(true);
                    pivotDataTest
                            .setRangeUsia(fieldRangeUsia.get(dt) == null ? null : fieldRangeUsia.get(dt).toString());
                }
                if (fieldJenisKelamin != null) {
                    fieldJenisKelamin.setAccessible(true);
                    pivotDataTest.setJenisKelamin(
                            fieldJenisKelamin.get(dt) == null ? null : fieldJenisKelamin.get(dt).toString());
                }
                if (fieldWargaNegara != null) {
                    fieldWargaNegara.setAccessible(true);
                    pivotDataTest.setWargaNegara(
                            fieldWargaNegara.get(dt) == null ? null : fieldWargaNegara.get(dt).toString());
                }
                if (fieldKategoriWp != null) {
                    fieldKategoriWp.setAccessible(true);
                    pivotDataTest
                            .setKategoriWp(fieldKategoriWp.get(dt) == null ? null : fieldKategoriWp.get(dt).toString());
                }
                if (fieldJenisWp != null) {
                    fieldJenisWp.setAccessible(true);
                    pivotDataTest.setJenisWp(fieldJenisWp.get(dt) == null ? null : fieldJenisWp.get(dt).toString());
                }
                if (fieldModal != null) {
                    fieldModal.setAccessible(true);
                    pivotDataTest.setModal(fieldModal.get(dt) == null ? null : fieldModal.get(dt).toString());
                }
                if (fieldStatusPusat != null) {
                    fieldStatusPusat.setAccessible(true);
                    pivotDataTest.setStatusPusat(
                            fieldStatusPusat.get(dt) == null ? null : fieldStatusPusat.get(dt).toString());
                }
                if (fieldPeriodePembukuan != null) {
                    fieldPeriodePembukuan.setAccessible(true);
                    pivotDataTest.setPeriodePembukuan(
                            fieldPeriodePembukuan.get(dt) == null ? null : fieldPeriodePembukuan.get(dt).toString());
                }
                if (fieldTahunPendirian != null) {
                    fieldTahunPendirian.setAccessible(true);
                    pivotDataTest.setTahunPendirian(
                            fieldTahunPendirian.get(dt) == null ? null : fieldTahunPendirian.get(dt).toString());
                }
                if (fieldBadanHukum != null) {
                    fieldBadanHukum.setAccessible(true);
                    pivotDataTest
                            .setBadanHukum(fieldBadanHukum.get(dt) == null ? null : fieldBadanHukum.get(dt).toString());
                }
                if (fieldJenis != null) {
                    fieldJenis.setAccessible(true);
                    pivotDataTest.setJenis(fieldJenis.get(dt) == null ? null : fieldJenis.get(dt).toString());
                }
                if (fieldStatusNpwp != null) {
                    fieldStatusNpwp.setAccessible(true);
                    pivotDataTest
                            .setStatusNpwp(fieldStatusNpwp.get(dt) == null ? null : fieldStatusNpwp.get(dt).toString());
                }
                if (fieldStatusPkp != null) {
                    fieldStatusPkp.setAccessible(true);
                    pivotDataTest
                            .setStatusPkp(fieldStatusPkp.get(dt) == null ? null : fieldStatusPkp.get(dt).toString());
                }
                if (fieldMetodePembukuan != null) {
                    fieldMetodePembukuan.setAccessible(true);
                    pivotDataTest.setMetodePembukuan(
                            fieldMetodePembukuan.get(dt) == null ? null : fieldMetodePembukuan.get(dt).toString());
                }
                if (fieldJumlah != null) {
                    fieldJumlah.setAccessible(true);
                    pivotDataTest.setJumlah(fieldJumlah.getInt(dt));
                }
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return pivotDataTest;
        }).collect(Collectors.toList());
        return listExcelData;
    }

    private Map<List<String>, List<TDemoOpWilayah>> groupListBy(List<TDemoOpWilayah> data, String[] groupByFieldNames) {
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        List<MethodHandle> handles = Arrays.stream(groupByFieldNames).map(field -> {
            try {
                return lookup.findGetter(TDemoOpWilayah.class, field, String.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
        return data.stream().collect(Collectors.groupingBy(d -> handles.stream().map(handle -> {
            try {
                return (String) handle.invokeExact(d);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList())));
    }

    private Map<List<String>, Map<String, IntSummaryStatistics>> groupListObjectBy(List<TDemoOpWilayah> data,
            String[] groupByFieldNames) {
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        List<MethodHandle> handles = Arrays.stream(groupByFieldNames).map(field -> {
            try {
                return lookup.findGetter(TDemoOpWilayah.class, field, String.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
        return data.stream().collect(Collectors.groupingBy(d -> handles.stream().map(handle -> {
            try {
                return (String) handle.invokeExact(d);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList()), Collectors.groupingBy(TDemoOpWilayah::getJumlahTanggungan,
                Collectors.summarizingInt(TDemoOpWilayah::getJumlah))

        ));
    }

    private Map<List<String>, Map<Object, IntSummaryStatistics>> groupListObjectArrayBy(List<PivotDataTest> data,
            String[] groupByFieldNames, String columnData) {
        Map<List<String>, Map<Object, IntSummaryStatistics>> pivotedData = new HashMap<>();
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        List<MethodHandle> handles = Arrays.stream(groupByFieldNames).map(field -> {
            try {
                return lookup.findGetter(PivotDataTest.class, field, String.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
        Field fieldColumn;
        try {
            fieldColumn = PivotDataTest.class.getDeclaredField(columnData);
            fieldColumn.setAccessible(true);

            pivotedData = data.stream().collect(Collectors.groupingBy(d -> handles.stream().map(handle -> {
                try {
                    return (String) handle.invokeExact(d);
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList()), Collectors.groupingBy(dt -> {
                try {
                    return fieldColumn.get(dt);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }, Collectors.summarizingInt(PivotDataTest::getJumlah))

            ));

        } catch (NoSuchFieldException | SecurityException e1) {
            e1.printStackTrace();
        }

        return pivotedData;
    }

    private void pivotMultiple(List<Object> rowIdx, int summaryIdx, List<Object[]> listData, int columnIdx) {
        Map<List<Object>, Map<Object, IntSummaryStatistics>> pivotMultiData = listData.stream()
                .collect(Collectors.groupingBy(r -> rowIdx, Collectors.groupingBy(r -> r[columnIdx],
                        Collectors.summarizingInt(r -> ((Integer) r[summaryIdx])))));
        System.out.println(pivotMultiData.entrySet());
    }

    private void pivotSimpleTable(int rowIdx, int summaryIdx, List<Object[]> listData, int columnIdx) {
        Map<Object, Map<Object, IntSummaryStatistics>> pivotData = listData.stream().collect(Collectors.groupingBy(
                r -> r[rowIdx],
                Collectors.groupingBy(r -> r[columnIdx], Collectors.summarizingInt(r -> ((Integer) r[summaryIdx])))));

        int total = 0;
        List<Long> stats = new ArrayList<>();
        Set<Object> headerSet = new HashSet<>();
        List<Object> listObject = new ArrayList<>();
        List<Object[]> listArrayObjects = new ArrayList<>();
        boolean flagZero = true;
        int cellData = 0;
        for (final Map.Entry<Object, Map<Object, IntSummaryStatistics>> entry : pivotData.entrySet()) {
            // store header
            headerSet.addAll(entry.getValue().keySet());
        }

        // write
        for (Object object : headerSet) {
            System.out.println("header param: " + object);
        }

        for (final Map.Entry<Object, Map<Object, IntSummaryStatistics>> entry : pivotData.entrySet()) {
            listObject = new ArrayList<>();
            listObject.add(entry.getKey());
            total = 0;
            for (Object oHead : headerSet) {
                int j = 0;
                for (Object obj : entry.getValue().keySet()) {
                    stats = entry.getValue().values().stream().map(m -> m.getSum()).collect(Collectors.toList());
                    cellData = Math.toIntExact(stats.get(j));
                    if (oHead.equals(obj)) {
                        flagZero = false;
                        break;
                    } else {
                        flagZero = true;
                    }
                    j++;
                }
                if (flagZero) {
                    listObject.add(0);
                    cellData = 0;
                } else {
                    listObject.add(cellData);
                }
                total += cellData;
            }
            listObject.add(total);
            // store row
            listArrayObjects.add(listObject.toArray());
        }

        // write header

        // write row
        for (int i = 0; i < listArrayObjects.size(); i++) {
            Object[] obj = listArrayObjects.get(i);
            for (int j = 0; j < obj.length; j++) {
                System.out.println(obj[j]);
            }
            System.out.println("=== new row ===");
        }
    }

    @Test
    public void testDynamicColumn() {
        List<TDemoOpWilayah> listModel = new ArrayList<>();
        TDemoOpWilayah model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan(">3");
        model.setJumlah(4);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("1");
        model.setJumlah(10);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA UTARA");
        model.setJumlahTanggungan(">2");
        model.setJumlah(5);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("5");
        model.setJumlah(20);
        listModel.add(model);

        int countHeader = 0;
        int countCell = 0;
        List<String> headerCell = new ArrayList<>();
        List<Object> rowPivot = new ArrayList<>();
        Map<Object, Integer> mapCellHeader = new HashMap<>();
        Map<Object, List<Object>> mapRowData = new HashMap<>();
        List<Object> body = new ArrayList<>();
        listModel.sort(Comparator.comparing(TDemoOpWilayah::getKanwil));
        for (TDemoOpWilayah demo : listModel) {

            ObjectMapping om = this.pivotTable(4, 13, this.objectOpWilayah(demo), new int[] { 8 });
            if (countHeader == 0) {
                headerCell.add("KANWIL");
                mapCellHeader.put("KANWIL", countHeader++);
            }
            headerCell.add((String) om.getColumns().getColumns().iterator().next());
            if (countCell != 0 && !rowPivot.contains(om.getRows())) {
                body = new ArrayList<>();
            }
            mapCellHeader.put(om.getColumns().getColumns().iterator().next(), countHeader++);
            body.add(countHeader == 1 || headerCell.contains(om.getColumns().getColumns().iterator().next())
                    ? om.getSummary()
                    : 0);
            rowPivot.add(om.getRows());
            mapRowData.put(om.getRows(), body);
            countCell++;
        }
        System.out.println("map header: " + mapCellHeader.toString());
        System.out.println("map: " + mapRowData.toString());
    }

    @Data
    public class PivotColumns {
        ArrayList<Object> columns;

        public PivotColumns(List<Object> objs, int... pRows) {
            columns = new ArrayList<>();

            for (int i = 0; i < pRows.length; i++) {
                columns.add(objs.get(pRows[i]));
            }
        }

        public void addObject(Object obj) {
            columns.add(obj);
        }
    }

    @Test
    public void convertModelToArrayObject() {
        TDemoOpWilayah model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan(">3");
        model.setJumlah(4);

        Object[] arrayObjectOpWilayah = objectOpWilayah(model);

        for (int i = 0; i < arrayObjectOpWilayah.length; i++) {
            System.out.println(arrayObjectOpWilayah[i]);
        }
    }

    @Test
    public void genericTest() {
        TDemoOpWilayahKlu model = new TDemoOpWilayahKlu();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        // model.setJumlahTanggungan(">3");
        model.setJumlah(4);
        Object[] obj = modelToObjectArray(model);
        Assert.assertNull(obj);
        // for (int i = 0; i < obj.length; i++) {
        // System.out.println(obj[i]);
        // }
    }

    public <T> Object[] modelToObjectArray(T t) {
        if (t instanceof TDemoOpWilayah) {
            return new Object[] { ((TDemoOpWilayah) t).getId(), ((TDemoOpWilayah) t).getTahun(),
                    ((TDemoOpWilayah) t).getKodeKpp(), ((TDemoOpWilayah) t).getNamaKpp(),
                    ((TDemoOpWilayah) t).getKanwil(), ((TDemoOpWilayah) t).getProvinsi(),
                    ((TDemoOpWilayah) t).getPulau(), ((TDemoOpWilayah) t).getStatusPerkawinan(),
                    ((TDemoOpWilayah) t).getJumlahTanggungan(), ((TDemoOpWilayah) t).getJenisOp(),
                    ((TDemoOpWilayah) t).getRangeUsia(), ((TDemoOpWilayah) t).getJenisKelamin(),
                    ((TDemoOpWilayah) t).getWargaNegara(), ((TDemoOpWilayah) t).getJumlah(),
                    ((TDemoOpWilayah) t).getCreatedBy(), ((TDemoOpWilayah) t).getCreatedDate() };
        } else {
            return null;
        }
    }

    private Object[] objectOpWilayah(TDemoOpWilayah model) {
        return new Object[] { model.getId(), model.getTahun(), model.getKodeKpp(), model.getNamaKpp(),
                model.getKanwil(), model.getProvinsi(), model.getPulau(), model.getStatusPerkawinan(),
                model.getJumlahTanggungan(), model.getJenisOp(), model.getRangeUsia(), model.getJenisKelamin(),
                model.getWargaNegara(), model.getJumlah(), model.getCreatedBy(), model.getCreatedDate() };
    }

    private List<TDemoOpWilayah> generateObject() {
        ArrayList<TDemoOpWilayah> list = new ArrayList<>();
        TDemoOpWilayah model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan(">3");
        model.setJumlah(4);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan(">2");
        model.setJumlah(10);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("1");
        model.setJumlah(10);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("1");
        model.setJumlah(5);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA UTARA");
        model.setJumlahTanggungan(">3");
        model.setJumlah(2);
        list.add(model);

        return list;

    }

    private ArrayList<List<Object>> generateListObject() {
        ArrayList<Object> list = new ArrayList<>();
        ArrayList<List<Object>> listOfList = new ArrayList<>();
        TDemoOpWilayah model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan(">3");
        model.setJumlah(4);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan(">2");
        model.setJumlah(10);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("1");
        model.setJumlah(10);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("1");
        model.setJumlah(5);
        list.add(model);

        model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA UTARA");
        model.setJumlahTanggungan(">3");
        model.setJumlah(2);
        list.add(model);
        listOfList.add(list);

        return listOfList;

    }

}