package id.seribu.seribuimport;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ooxml.util.SAXHelper;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import id.seribu.seribudto.demografi.DemografiDto;
import id.seribu.seribudto.demografi.op.OpWilayahDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;;


public class ReadExcelFileTest {

    // @Test
    public void readWorkbookTest() {
        final String testFile = "/home/arieki/Documents/Testing File less data.xlsx";
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            XExcelFileReader excelReader = new XExcelFileReader(testFile);
            lDtos = excelReader.readRows().stream()
                .map(dto -> new OpWilayahDtoBuilder()
                            .tahun(dto.get(ImportConstant.Header.TAHUN))
                            .kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                            .namaKpp(dto.get(ImportConstant.Header.NAMAKPP))
                            .provinsi(dto.get(ImportConstant.Header.PROVINSI))
                            .pulau(dto.get(ImportConstant.Header.PULAU))
                            .kanwil(dto.get(ImportConstant.Header.KANWIL))
                            .statusPerkawinan(dto.get(ImportConstant.Header.STATUSPERKAWINAN))
                            .jumlahTanggungan(dto.get(ImportConstant.Header.JUMLAHTANGGUNGAN))
                            .jenisOp(dto.get(ImportConstant.Header.JENISOP))
                            .rangeUsia(dto.get(ImportConstant.Header.RANGEUSIA))
                            .jenisKelamin(dto.get(ImportConstant.Header.JENISKELAMIN))
                            .wargaNegara(dto.get(ImportConstant.Header.WARGANEGARA))
                            .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH)))
                            .build())
                .collect(Collectors.toList());
            for(DemografiDto demo : lDtos) {
                System.out.println("tahun " + demo.getTahun() + " " + demo.getJumlah());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public void processOneSheet(String filename) throws Exception {
        OPCPackage pkg = OPCPackage.open(filename);
        XSSFReader r = new XSSFReader( pkg );
        SharedStringsTable sst = r.getSharedStringsTable();
        XMLReader parser = fetchSheetParser(sst);
        // To look up the Sheet Name / Sheet Order / rID,
        //  you need to process the core Workbook stream.
        // Normally it's of the form rId# or rSheet#
        InputStream sheet2 = r.getSheet("rId2");
        InputSource sheetSource = new InputSource(sheet2);
        parser.parse(sheetSource);
        sheet2.close();
    }

    public XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException, ParserConfigurationException {
        XMLReader parser = SAXHelper.newXMLReader();
        ContentHandler handler = new SheetHandler(sst);
        parser.setContentHandler(handler);
        return parser;
    }
}