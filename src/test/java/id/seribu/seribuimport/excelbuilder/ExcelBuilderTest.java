package id.seribu.seribuimport.excelbuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import id.seribu.seribudto.transaction.WizardRequestDto;
import id.seribu.seribuimport.builder.ExcelBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.dto.ObjectMappingDto;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribumodel.transaction.TDemoOpWilayah;

// @RunWith(SpringRunner.class)
// @SpringBootTest(classes = {ExcelFactory.class})
// @ActiveProfiles(profiles = "sit")
public class ExcelBuilderTest {

    ExcelUtil excelUtil = new ExcelUtil();
    ExcelBuilder builder;

    List<TDemoOpWilayah> listOpWilayah;

    String rowParam = "kanwil";
    String colParam = "jumlah_tanggungan";
    String summaryParam = "jumlah";
    int rowIndex = -1;
    int colIndex = -1;
    int summaryIdx = -1;
    ObjectMappingDto[] out;
    protected XSSFWorkbook workbook;
    protected XSSFSheet sheet;
    protected int rowNum;
    private WizardRequestDto wizardRequestDto;

    @Before
    public void setup() {
        wizardRequestDto = new WizardRequestDto();
        wizardRequestDto.setDataset("penerimaan");
        wizardRequestDto.setSubyekData("op");
        wizardRequestDto.setSourceData("wilayah");
        wizardRequestDto.setRowPivot(new String[] {"tahun" , "kanwil"});
        wizardRequestDto.setColumnPivot("jumlahTanggungan");
    }

    @Test
    public void buildExcelTest() {
        workbook = new XSSFWorkbook();
        builder = new ExcelBuilder(workbook, new Date());
        // demoOpWilayahRepo.findAll().iterator().forEachRemaining(listOpWilayah::add);
        // listOpWilayah = demoOpWilayahRepo.findByTahun("2014");
        List<TDemoOpWilayah> listModel = new ArrayList<>();
        TDemoOpWilayah model = new TDemoOpWilayah();
        model.setTahun("2014");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan(">3");
        model.setJumlah(4);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("1");
        model.setJumlah(10);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA UTARA");
        model.setJumlahTanggungan(">2");
        model.setJumlah(5);
        listModel.add(model);
        model = new TDemoOpWilayah();
        model.setTahun("2015");
        model.setKanwil("KANTOR WILAYAH DJP JAKARTA TIMUR");
        model.setJumlahTanggungan("5");
        model.setJumlah(20);
        listModel.add(model);
        Assert.assertNotNull(listModel);
        // builder.buildTitle(excelUtil.excelTitle("penerimaan", "op", rowParam, colParam))
        //     .buildPivotTable(listModel, rowIndex, summaryIdx, colIndex, rowParam.toUpperCase().replace("_", " "))
        //         .sort(HeaderConstant.OPWILAYAH.length);
        System.out.println(wizardRequestDto.toString());
        builder.buildTitle(excelUtil.excelTitle(wizardRequestDto.getDataset(), wizardRequestDto.getSubyekData(), 
                wizardRequestDto.getColumnPivot()), excelUtil.formatDate(new Date(), ImportConstant.EXCEL_FORMAT_DATE))
                .buildPivotTabel(listModel, wizardRequestDto.getRowPivot(), wizardRequestDto.getColumnPivot())
                .sort();

    }
}