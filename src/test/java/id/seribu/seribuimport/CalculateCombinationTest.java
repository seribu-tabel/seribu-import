package id.seribu.seribuimport;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.Test;

import id.seribu.seribuimport.util.MathUtil;

public class CalculateCombinationTest {

    MathUtil mathUtil = new MathUtil();

    @Test
    public void testCombination() {
       BigDecimal comb3 = mathUtil.calcCombination(14, 2);
       System.out.println(comb3);
       BigDecimal comb7 = mathUtil.calcCombination(14, 7);
       System.out.println(comb7);

       int perm7 = mathUtil.calcPermutation(14, 2);
       System.out.println(perm7);
    }

    @Test
    public void testFactorial() {
        int nW = 12;
        int rW = 2;
        int resultWilayah = (factorial(nW).divide((factorial(rW).multiply(factorial(nW-rW))))).intValue();

        int nWK = 8;
        int rWK = 2;
        BigDecimal resultWilK = factorial(nWK).divide((factorial(rWK).multiply(factorial(nWK-rWK))));

        int nK = 9;
        int rK = 2;
        BigDecimal resultK = factorial(nK).divide((factorial(rK).multiply(factorial(nK-rK))));

        int nS = 10;
        int rS = 2;
        BigDecimal resultStatus = factorial(nS).divide((factorial(rS).multiply(factorial(nS-rS))));

        System.out.println(resultWilayah);
        // System.out.println("OP: " + resultWilayah.add(resultWilK).add(resultK).add(resultStatus));

        int nBK = 14;
        int rBK = 2;
        BigDecimal resultBK = factorial(nBK).divide((factorial(rBK).multiply(factorial(nBK-rBK))));

        int nBS = 16;
        int rBS = 2;
        BigDecimal resultBS = factorial(nBS).divide((factorial(rBS).multiply(factorial(nBS-rBS))));

        System.out.println("Badan " + resultBK.add(resultBS));

    
        int nBhK = 7;
        int rBhK = 2;
        BigDecimal resultBhK = factorial(nBhK).divide((factorial(rBhK).multiply(factorial(nBhK-rBhK))));

        int nBhS = 9;
        int rBhS = 2;
        BigDecimal resultBhS = factorial(nBhS).divide((factorial(rBhS).multiply(factorial(nBhS-rBhS))));
        System.out.println("Bendahara: :" + (resultBhK.add(resultBhS)));
    }

    private BigDecimal factorial(int n) {
        BigDecimal fact = new BigDecimal(1);
        int i = 1;
        while (i <= n) {
            fact = fact.multiply(new BigDecimal(i));
            i++;
        }
        return fact;
    }



    @Test
    public void testUUID() {
        System.out.println(UUID.randomUUID());
    }
}