package id.seribu.seribuimport.builder;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import id.seribu.seribudto.transaction.PivotDataDto;
import id.seribu.seribuimport.constant.HeaderEnum;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.factory.ExcelFactory;
import id.seribu.seribuimport.util.ClassUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelBuilder extends AbstractExcelBuilder {

    ExcelFactory excelFactory = new ExcelFactory();

    ClassUtil classUtil = new ClassUtil();

    public ExcelBuilder(final XSSFWorkbook workbook, final Date generateDate) {
        super();
        log.info("ExcelBuilder generateDate: {}", generateDate);
        this.workbook = workbook;
        this.sheet = workbook.createSheet(ImportConstant.SHEETNAME);
        this.rowNum = 0;
        this.columnSize = 0;
        this.date = generateDate;
        this.prepareCellStyle();
    }

    public ExcelBuilder buildTitle(String title, String createdDate) {
        log.info("ExcelBuilder.buildTitle");
        this.writeTitle(0, 4, title);
        this.writeTitle(0, 1, createdDate);
        return this;
    }

    public <T> ExcelBuilder buildPivotTabel(List<T> data, String[] rowPivot, String columnPivot) {
        log.info("ExcelBuilder.buildPivotTable");
        this.parentHeaderWriter(rowPivot, columnPivot);
        this.writePivotTable(this.groupListObjectArrayBy(classUtil.convertModelToExcelPivot(data), rowPivot, columnPivot));
        return this;
    }

    private void parentHeaderWriter(String[] rowPivot, String columnPivot) {
        Object [] data = new Object[rowPivot.length+1];
        int i = 0;
        for (String obj : rowPivot) {
            data[i++] = HeaderEnum.valueOf(obj).getText();
        }
        data[i] = HeaderEnum.valueOf(columnPivot).getText();
        this.writeParentHeader(data, 0, styleHeaderColumn);
        columnSize += data.length;
    }

    private void writePivotTable(Map<List<String>, Map<Object, IntSummaryStatistics>> pivotData) {
        Set<Object> columnSet = new HashSet<>();
        List<Object> listObject = new ArrayList<>();
        List<Object[]> listArrayObjects = new ArrayList<>();
        List<Long> stats = new ArrayList<>();
        int total = 0;
        boolean flagZero = true;
        Integer cellData = 0;

        for (final Map.Entry<List<String>, Map<Object, IntSummaryStatistics>> entry : pivotData.entrySet()) {
            columnSet.addAll(entry.getValue().keySet());
        }

        // write header
        this.writeHeader(columnSet.toArray(), columnSize-1, styleHeaderColumn);

        for (final Map.Entry<List<String>, Map<Object, IntSummaryStatistics>> entry : pivotData.entrySet()) {
            listObject = new ArrayList<>();
            total = 0;
            listObject.addAll(entry.getKey());
            for(Object oHead : columnSet) {
                int j = 0;
                for (Object obj : entry.getValue().keySet()) {
                    stats = entry.getValue().values().stream().map(m -> m.getSum()).collect(Collectors.toList());
                    cellData = Math.toIntExact(stats.get(j));
                    if(oHead.equals(obj)) {
                        flagZero = false;
                        break;
                    } else {
                        flagZero = true;
                    }
                    j++;
                }
                if(flagZero) {
                    listObject.add(0);
                    cellData = 0;
                } else {
                    listObject.add(cellData);
                }
                total += cellData;
            }            
            listObject.add(total);
            // store row
            listArrayObjects.add(listObject.toArray());
        }

        columnSize += columnSet.size() + 1;
        //write row
        for (int i = 0; i < listArrayObjects.size(); i++) {
            Object[] data = listArrayObjects.get(i);
            final Row row = sheet.createRow(++rowNum);
            this.writeRow(row, 0, data);
        }
    }

    private Map<List<String>, Map<Object, IntSummaryStatistics>> groupListObjectArrayBy(List<PivotDataDto> data,
            String[] groupByFieldNames, String columnData) {
        Map<List<String>, Map<Object, IntSummaryStatistics>> pivotedData = new HashMap<>();
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        List<MethodHandle> handles = Arrays.stream(groupByFieldNames).map(field -> {
            try {
                return lookup.findGetter(PivotDataDto.class, field, String.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
        try {
            Field fieldColumn = PivotDataDto.class.getDeclaredField(columnData);
            fieldColumn.setAccessible(true);

            pivotedData = data.stream().collect(Collectors.groupingBy(d -> handles.stream().map(handle -> {
                try {
                    return (String) handle.invokeExact(d);
                } catch (Throwable e) {
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList()), Collectors.groupingBy(dt -> {
                try {
                    return fieldColumn.get(dt);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }, Collectors.summarizingInt(PivotDataDto::getJumlah))

            ));

        } catch (NoSuchFieldException | SecurityException e1) {
            e1.printStackTrace();
        }

        return pivotedData;
    }
}