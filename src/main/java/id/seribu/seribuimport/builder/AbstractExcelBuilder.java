package id.seribu.seribuimport.builder;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.factory.ExcelFactory;

public abstract class AbstractExcelBuilder {

    @Autowired
    ExcelFactory factory ;

    protected XSSFWorkbook workbook;
    protected XSSFSheet sheet;
    protected int rowNum;
    protected Date date;
    protected CellStyle styleTitle;
    protected CellStyle styleHeaderColumn;
    protected CellStyle styleRowValue;
    protected CellStyle styleRowDateValue;
    protected int columnSize;

    public void writeTitle(final int firstCol, final int lastCol, final String title) {
        final Row row = sheet.createRow(rowNum);
        row.setHeightInPoints(2 * sheet.getDefaultRowHeightInPoints());
        final CellRangeAddress cellRangeAddress = new CellRangeAddress(rowNum, rowNum, firstCol, lastCol);
        row.getSheet().addMergedRegion(cellRangeAddress);
        final Cell cell = row.createCell(firstCol);
        cell.setCellStyle(styleTitle);
        cell.setCellValue(title);
        rowNum++;
    }

    public void writeParentHeader(final Object[] data, int firstCol, CellStyle cellStyle) {
        final Row row = sheet.createRow(++rowNum);
        for (final Object field : data) {
            final Cell cell = row.createCell(firstCol);
            if(field instanceof Integer) {
                cell.setCellValue(String.valueOf(field));
            } else if (field instanceof String) {
                cell.setCellValue((String) field);
            }
            cell.setCellStyle(cellStyle);
            firstCol++;
        }
        rowNum++;
    }

    public void writeHeader(final Object[] data, int firstCol, CellStyle cellStyle) {
        final Row row = sheet.createRow(rowNum++);
        for (final Object field : data) {
            final Cell cell = row.createCell(firstCol);
            if(field instanceof Integer) {
                cell.setCellValue(String.valueOf(field));
            } else if (field instanceof String) {
                cell.setCellValue((String) field);
            }
            cell.setCellStyle(cellStyle);
            firstCol++;
        }
        // always write total column
        final Cell lastCell = row.createCell(firstCol);
        lastCell.setCellValue(ImportConstant.TOTALFIELD);
        lastCell.setCellStyle(cellStyle);
    }

    public void writeRow(final Row row, int firstCol, final Object[] data) {
        for (final Object field : data) {
            final Cell cell = row.createCell(firstCol++);
            if (field instanceof String) {
                cell.setCellValue((String) field);
            } else if (field instanceof Integer) {
                cell.setCellValue((Integer) field);
            } else if (field instanceof Long) {
                cell.setCellValue((Long) field);
            } else if (field instanceof Date) {
                cell.setCellValue((Date) field);
            } else if (field instanceof BigDecimal) {
                cell.setCellValue(((BigDecimal) field).doubleValue());
            } else {
                cell.setCellValue(field == null ? "" : field.toString());
            }
            this.setRowCellStyle(field, cell);
        }
    }

    public void setRowCellStyle(final Object field, final Cell cell) {
        this.setCellStyle(cell, field, styleRowValue);
    }

    public void setCellStyle(final Cell cell, final Object field, CellStyle cellStyle) {
        cell.setCellStyle(cellStyle);
    }

    public void prepareCellStyle() {
        factory = new ExcelFactory();
        final short dateFormat = factory.createExcelDataFormat(workbook, ImportConstant.DATEFORMAT);
        final XSSFFont fontTitle = factory.createExcelFont(workbook, true, 12);
        final XSSFFont fontHeader = factory.createExcelFont(workbook, true, 12);
        final XSSFFont fontRowData = factory.createExcelFont(workbook, false, 12);

        styleTitle = factory.createExcelCellStyle(workbook, null, null, null, HorizontalAlignment.LEFT, null,
                fontTitle, true);
        styleHeaderColumn = factory.createExcelCellStyle(workbook, IndexedColors.LIGHT_GREEN.index, BorderStyle.THICK,
                BorderStyle.THICK, HorizontalAlignment.CENTER, null, fontHeader, false);
        styleRowValue = factory.createExcelCellStyle(workbook, null, null, null, null, null, fontRowData, false);
        styleRowDateValue = factory.createExcelCellStyle(workbook, null, null, null, null, dateFormat, fontRowData, false);
    }

    public void sort() {
        for (int col = 0; col < columnSize; col++) {
            sheet.autoSizeColumn(col);
        }
    }
}