package id.seribu.seribuimport.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ObjectMappingDto {
    @NonNull
    private Integer rowIndex;
    @NonNull
    private Integer colIndex;
    @NonNull
    private Integer summaryIndex;
    @NonNull
    private Integer sizeArray;
}