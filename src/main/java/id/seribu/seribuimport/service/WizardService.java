package id.seribu.seribuimport.service;

import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.WizardRequestDto;

public interface WizardService {

    DownloadResponseDto getExcelFile(WizardRequestDto wizardRequestDto);
}