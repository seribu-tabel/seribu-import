package id.seribu.seribuimport.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.transaction.ImportDto;
import id.seribu.seribudto.transaction.ImportResponseDto;
import id.seribu.seribuimport.exception.FileStorageException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractProcessFile {

    private Path path;
    
    protected void setPath(String archiveDir) {
        log.info("initiate directory AbstractProcessFile DIR: [{}]", archiveDir);
        this.path = Paths.get(archiveDir).toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.path);
            Files.setPosixFilePermissions(this.path, this.permissions());
        } catch (Exception ex) {
            throw new FileStorageException("Tidak dapat membuat folder direktori file.", ex);
        }
    }

    protected ImportResponseDto upload(MultipartFile file, ImportDto requestDto) {
        this.setPath(requestDto.getDirectory());
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        log.info("AbstractProcessFile.upload filename: {}, ", filename);
        try {
            if (filename.contains("..")) {
                throw new FileStorageException("Maaf, terdapat kesalahan pada nama file " + filename);
            }
            Path targetLocation = this.path.resolve(filename);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return new ImportResponseDto(requestDto.getDataset(), requestDto.getSubyekData(), filename,
                this.path.toString(), file.getContentType(), file.getSize());
        } catch (IOException e) {
            log.error("AbstractProcessFile.upload fail ", e);
            throw new FileStorageException("Tidak dapat menyimpan file " + filename + ". Silakan coba lagi", e);
        }
    }

    private Set<PosixFilePermission> permissions() {
        log.info("AbstractProcessFile.permissions...");
        Set<PosixFilePermission> perm = new HashSet<>();
        perm.add(PosixFilePermission.OWNER_EXECUTE);
        perm.add(PosixFilePermission.OWNER_READ);
        perm.add(PosixFilePermission.OWNER_WRITE);
        perm.add(PosixFilePermission.GROUP_READ);
        perm.add(PosixFilePermission.GROUP_EXECUTE);
        perm.add(PosixFilePermission.GROUP_WRITE);
        perm.add(PosixFilePermission.OTHERS_READ);
        perm.add(PosixFilePermission.OTHERS_EXECUTE);
        perm.add(PosixFilePermission.OTHERS_WRITE);
        return perm;
    }

}