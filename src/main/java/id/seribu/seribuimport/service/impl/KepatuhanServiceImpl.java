package id.seribu.seribuimport.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.factory.kepatuhan.BadanFactory;
import id.seribu.seribuimport.factory.kepatuhan.BendaharaFactory;
import id.seribu.seribuimport.factory.kepatuhan.OpFactory;
import id.seribu.seribuimport.factory.kepatuhan.UmumFactory;
import id.seribu.seribuimport.repository.ImportFileRepo;
import id.seribu.seribuimport.service.HeaderColumnService;
import id.seribu.seribuimport.service.KepatuhanService;
import id.seribu.seribuimport.util.FileUtil;
import id.seribu.seribuimport.util.MathUtil;
import id.seribu.seribumodel.transaction.TImportFile;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class KepatuhanServiceImpl implements KepatuhanService {

    @Autowired
    ImportFileRepo importFileRepo;

    @Autowired
    @Qualifier(value = "kepatuhanOpFactory")
    OpFactory opFactory;

    @Autowired
    @Qualifier(value = "kepatuhanBadanFactory")
    BadanFactory badanFactory;

    @Autowired
    @Qualifier(value = "kepatuhanBendaharaFactory")
    BendaharaFactory bendaharaFactory;

    @Autowired
    @Qualifier(value = "kepatuhanUmumFactory")
    UmumFactory umumFactory;

    @Autowired
    FileUtil fileUtil;

    @Autowired
    MathUtil mathUtil;

    @Autowired
    HeaderColumnService headerColumnService;

    private BigDecimal kepatuhanBadanAllCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanBadanStatusCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanBendaharaAllCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanBendaharaStatusCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanOpWilayahCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanOpWilayahKluCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanOpKluCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanOpStatusCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanUmumStatusCount = BigDecimal.ZERO;
    private BigDecimal kepatuhanUmumWilayahKluCount = BigDecimal.ZERO;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataOp() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.KEPATUHAN, ImportConstant.SubyekData.OP);
        if (listNotProcessedFile.size() == 0) {
            log.info("KepatuhanService.procesImportDataOp NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.OP);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("KepatuhanService.procesImportDataOp process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.WILAYAH:
                    opFactory.importOpWilayah(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.WILAYAHKLU:
                    opFactory.importOpWilayahKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.KLU:
                    opFactory.importOpKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    opFactory.importOpStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataBadan() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.KEPATUHAN, ImportConstant.SubyekData.BADAN);
        if (listNotProcessedFile.size() == 0) {
            log.info("KepatuhanService.procesImportDataBadan NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.BADAN);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("KepatuhanService.procesImportDataBadan process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.SEMUA:
                    badanFactory.importBadanAll(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    badanFactory.importBadanStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataBendahara() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.KEPATUHAN, ImportConstant.SubyekData.BENDAHARA);
        if (listNotProcessedFile.size() == 0) {
            log.info("KepatuhanService.procesImportDataBendahara NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.BENDAHARA);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("KepatuhanService.procesImportDataBendahara process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.SEMUA:
                    bendaharaFactory.importBendaharaAll(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    bendaharaFactory.importBendaharaStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void postProccessFile(TImportFile tImportFile) {
        log.info("KepatuhanService.postProccessFile update t_import_file, id: {}", tImportFile.getId());
        tImportFile.setProcess(true);
        tImportFile.setUpdatedBy("SYSTEM");
        importFileRepo.save(tImportFile);
        // backup already processed file
        fileUtil.backupFile(tImportFile.getDirectory(), tImportFile.getDirectory() + ImportConstant.PROCESSEDPATH,
                tImportFile.getFilename());
    }

    private String getDirWithFileName(String directory, String filename) {
        log.info("KepatuhanService.getDirWithFileName path: {}", directory + ImportConstant.PATH_PREFIX + filename);
        return directory + ImportConstant.PATH_PREFIX + filename;
    }

    private List<TImportFile> findFileNotProcess(String dataset, String subyekData) {
        log.info("KepatuhanService.findFileNotProcess dataset: {}, subyekData: {}", dataset, subyekData);
        return importFileRepo.findByDatasetNotProcess(dataset, subyekData);
    }

    @Override
    public BigDecimal getTotalKepatuhan() {
        log.info("KepatuhanService.getTotalKepatuhan");
        if (badanFactory.isAvailableKepatuhanBadanAll()) {
            kepatuhanBadanAllCount = mathUtil.calcCombination(headerColumnService.badanAllSize(),
                    ImportConstant.COMBINATION);
        }

        if (badanFactory.isAvailableKepatuhanBadanStatus()) {
            kepatuhanBadanStatusCount = mathUtil.calcCombination(headerColumnService.badanStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (bendaharaFactory.isAvailableKepatuhanBendaharaAll()) {
            kepatuhanBendaharaAllCount = mathUtil.calcCombination(headerColumnService.bendaharaAllSize(),
                    ImportConstant.COMBINATION);
        }

        if (bendaharaFactory.isAvailableKepatuhanBendaharaStatus()) {
            kepatuhanBendaharaStatusCount = mathUtil.calcCombination(headerColumnService.bendaharaStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableKepatuhanOpWilayah()) {
            kepatuhanOpWilayahCount = mathUtil.calcCombination(headerColumnService.opWilayahSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableKepatuhanOpWilayahKlu()) {
            kepatuhanOpWilayahKluCount = mathUtil.calcCombination(headerColumnService.opWilayahKluSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableKepatuhanOpKlu()) {
            kepatuhanOpKluCount = mathUtil.calcCombination(headerColumnService.opKluSize(), ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableKepatuhanOpStatus()) {
            kepatuhanOpStatusCount = mathUtil.calcCombination(headerColumnService.opStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (umumFactory.isAvailableKepatuhanUmumStatus()) {
            kepatuhanUmumStatusCount = mathUtil.calcCombination(headerColumnService.umumStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (umumFactory.isAvailableKepatuhanUmumWilayahKlu()) {
            kepatuhanUmumWilayahKluCount = mathUtil.calcCombination(headerColumnService.umumWilayahKluSize(),
                    ImportConstant.COMBINATION);
        }

        return kepatuhanBadanAllCount.add(kepatuhanBadanStatusCount)
                .add(kepatuhanBendaharaAllCount).add(kepatuhanBendaharaStatusCount)
                .add(kepatuhanOpWilayahCount).add(kepatuhanOpWilayahKluCount).add(kepatuhanOpKluCount).add(kepatuhanOpStatusCount)
                .add(kepatuhanUmumStatusCount).add(kepatuhanUmumWilayahKluCount);
    }

    @Override
    public List<?> getAllData(String subyekData, String datasource, List<String> tahun) {
        switch (subyekData) {
        case ImportConstant.SubyekData.OP:
            switch (datasource) {
            case ImportConstant.SourceData.WILAYAH:
                return opFactory.kepatuhanOpWilayahList(tahun);
            case ImportConstant.SourceData.WILAYAHKLU:
                return opFactory.kepatuhanOpWilayahKluList(tahun);
            case ImportConstant.SourceData.KLU:
                return opFactory.kepatuhanOpKluList(tahun);
            case ImportConstant.SourceData.STATUS:
                return opFactory.kepatuhanOpStatusList(tahun);
            }
            break;

        case ImportConstant.SubyekData.BADAN:
            switch (datasource) {
            case ImportConstant.SourceData.SEMUA:
                return badanFactory.kepatuhanBadanAll(tahun);
            case ImportConstant.SourceData.STATUS:
                return badanFactory.kepatuhanBadanStatus(tahun);
            }
            break;
        case ImportConstant.SubyekData.BENDAHARA:
            switch (datasource) {
            case ImportConstant.SourceData.SEMUA:
                return bendaharaFactory.kepatuhanBendaharaAll(tahun);
            case ImportConstant.SourceData.STATUS:
                return bendaharaFactory.kepatuhanBendaharaStatus(tahun);
            }
            break;
        case ImportConstant.SubyekData.UMUM:
            switch (datasource) {
            case ImportConstant.SourceData.WILAYAHKLU:
                return umumFactory.kepatuhanUmumWilayahKluList(tahun);
            case ImportConstant.SourceData.STATUS:
                return umumFactory.kepatuhanUmumStatusList(tahun);
            }
            break;
        }
        return null;
    }

    @Override
    public void processImportDataUmum() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.KEPATUHAN, ImportConstant.SubyekData.UMUM);
        if (listNotProcessedFile.size() == 0) {
            log.info("KepatuhanService.processImportDataUmum NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.UMUM);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("KepatuhanService.processImportDataUmum process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.WILAYAHKLU:
                    umumFactory.importUmumWilayahKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    umumFactory.importUmumStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

}