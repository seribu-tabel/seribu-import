package id.seribu.seribuimport.service.impl;

import id.seribu.seribuimport.constant.HeaderConstant;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.dto.ObjectMappingDto;
import id.seribu.seribuimport.service.SubyekDataService;

public class BendaharaServiceImpl implements SubyekDataService {

    @Override
    public ObjectMappingDto getArrayIndex(String datasource, String rowParam, String colParam, String sumParam) {
        int rowIndex = -1, colIndex = -1, summaryIndex = -1, size = 0;
        switch (datasource) {
        case ImportConstant.SourceData.SEMUA:
            for (int i = 0; i < HeaderConstant.BENDAHARAALL.length; i++) {
                if (HeaderConstant.BENDAHARAALL[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.BENDAHARAALL[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.BENDAHARAALL[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.BENDAHARAALL.length;
            break;

        case ImportConstant.SourceData.STATUS:
            for (int i = 0; i < HeaderConstant.BENDAHARASTATUS.length; i++) {
                if (HeaderConstant.BENDAHARASTATUS[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.BENDAHARASTATUS[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.BENDAHARASTATUS[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.BENDAHARAALL.length;
            break;
        }

        return new ObjectMappingDto(rowIndex, colIndex, summaryIndex, size);
    }

}