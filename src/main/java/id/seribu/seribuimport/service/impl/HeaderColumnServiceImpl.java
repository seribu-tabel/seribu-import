package id.seribu.seribuimport.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.BadanAllHeaderRepo;
import id.seribu.seribuimport.repository.BadanStatusHeaderRepo;
import id.seribu.seribuimport.repository.BendaharaAllHeaderRepo;
import id.seribu.seribuimport.repository.BendaharaStatusHeaderRepo;
import id.seribu.seribuimport.repository.OpKluHeaderRepo;
import id.seribu.seribuimport.repository.OpStatusHeaderRepo;
import id.seribu.seribuimport.repository.OpWilayahHeaderRepo;
import id.seribu.seribuimport.repository.OpWilayahKluHeaderRepo;
import id.seribu.seribuimport.repository.UmumStatusHeaderRepo;
import id.seribu.seribuimport.repository.UmumWilayahKluHeaderRepo;
import id.seribu.seribuimport.service.HeaderColumnService;
import id.seribu.seribumodel.header.BadanAllHeader;
import id.seribu.seribumodel.header.BadanStatusHeader;
import id.seribu.seribumodel.header.BendaharaAllHeader;
import id.seribu.seribumodel.header.BendaharaStatusHeader;
import id.seribu.seribumodel.header.OpKluHeader;
import id.seribu.seribumodel.header.OpStatusHeader;
import id.seribu.seribumodel.header.OpWilayahHeader;
import id.seribu.seribumodel.header.OpWilayahKluHeader;
import id.seribu.seribumodel.header.UmumStatusHeader;
import id.seribu.seribumodel.header.UmumWilayahKluHeader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HeaderColumnServiceImpl implements HeaderColumnService {

    @Autowired
    BadanAllHeaderRepo badanAllHeaderRepo;

    @Autowired
    BadanStatusHeaderRepo badanStatusHeaderRepo;

    @Autowired
    BendaharaAllHeaderRepo bendaharaAllHeaderRepo;

    @Autowired
    BendaharaStatusHeaderRepo bendaharaStatusHeaderRepo;

    @Autowired
    OpWilayahHeaderRepo opWilayahHeaderRepo;

    @Autowired
    OpWilayahKluHeaderRepo opWilayahKluHeaderRepo;

    @Autowired
    OpKluHeaderRepo opKluHeaderRepo;

    @Autowired
    OpStatusHeaderRepo opStatusHeaderRepo;

    @Autowired
    UmumStatusHeaderRepo umumStatusHeaderRepo;

    @Autowired
    UmumWilayahKluHeaderRepo umumWilayahKluHeaderRepo;

    private List<Lov> lovHeader;

    @Override
    public List<Lov> getHeaderColumn(String subyekData, String sourceData) {
        log.info("HeaderColumnService.getHeaderColumn subyekData: {}, sourceData: {}", subyekData, sourceData);
        switch (subyekData) {
            case ImportConstant.SubyekData.OP:
                if(ImportConstant.SourceData.WILAYAH.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (OpWilayahHeader hdr : opWilayahHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                } else if (ImportConstant.SourceData.WILAYAHKLU.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (OpWilayahKluHeader hdr : opWilayahKluHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                } else if (ImportConstant.SourceData.KLU.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (OpKluHeader hdr : opKluHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                } else if (ImportConstant.SourceData.STATUS.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (OpStatusHeader hdr : opStatusHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                }
                break;
            case ImportConstant.SubyekData.BADAN:
                if(ImportConstant.SourceData.SEMUA.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (BadanAllHeader hdr : badanAllHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                } else if (ImportConstant.SourceData.STATUS.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (BadanStatusHeader hdr : badanStatusHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                } 
                break;
            case ImportConstant.SubyekData.BENDAHARA:
                if(ImportConstant.SourceData.SEMUA.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (BendaharaAllHeader hdr : bendaharaAllHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                } else if (ImportConstant.SourceData.STATUS.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (BendaharaStatusHeader hdr : bendaharaStatusHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                }
                break;
            case ImportConstant.SubyekData.UMUM:
                if(ImportConstant.SourceData.WILAYAHKLU.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (UmumWilayahKluHeader hdr : umumWilayahKluHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                } else if (ImportConstant.SourceData.STATUS.equalsIgnoreCase(sourceData)) {
                    lovHeader = new ArrayList<>();
                    for (UmumStatusHeader hdr : umumStatusHeaderRepo.findAll()) {
                        lovHeader.add(new Lov(hdr.getValue(), hdr.getText()));
                    }
                }
                break;
        }

        return lovHeader;
    }

    @Override
    public int badanAllSize() {
        log.info("HeaderColumnService.badanAllSize");
        return ((Collection<?>) badanAllHeaderRepo.findAll()).size();
    }

    @Override
    public int badanStatusSize() {
        log.info("HeaderColumnService.badanStatusSize");
        return ((Collection<?>) badanStatusHeaderRepo.findAll()).size();
    }

    @Override
    public int bendaharaStatusSize() {
        log.info("HeaderColumnService.bendaharaStatusSize");
        return ((Collection<?>) bendaharaStatusHeaderRepo.findAll()).size();
    }

    @Override
    public int bendaharaAllSize() {
        log.info("HeaderColumnService.bendaharaAllSize");
        return ((Collection<?>) bendaharaAllHeaderRepo.findAll()).size();
    }

    @Override
    public int opWilayahSize() {
        log.info("HeaderColumnService.opWilayahSize");
        return ((Collection<?>) opWilayahHeaderRepo.findAll()).size();
    }

    @Override
    public int opWilayahKluSize() {
        log.info("HeaderColumnService.opWilayahKluSize");
        return ((Collection<?>) opWilayahKluHeaderRepo.findAll()).size();
    }

    @Override
    public int opKluSize() {
        log.info("HeaderColumnService.opKlu");
        return ((Collection<?>) opKluHeaderRepo.findAll()).size();
    }

    @Override
    public int opStatusSize() {
        log.info("HeaderColumnService.opStatusSize");
        return ((Collection<?>) opStatusHeaderRepo.findAll()).size();
    }

    @Override
    public int umumWilayahKluSize() {
        log.info("HeaderColumnService.umumWilayahKluSize");
        return ((Collection<?>) umumWilayahKluHeaderRepo.findAll()).size();
    }

    @Override
    public int umumStatusSize() {
        log.info("HeaderColumnService.umumStatusSize");
        return ((Collection<?>) umumStatusHeaderRepo.findAll()).size();
    }

}