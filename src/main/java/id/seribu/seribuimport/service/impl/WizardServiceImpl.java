package id.seribu.seribuimport.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.WizardRequestDto;
import id.seribu.seribuimport.builder.ExcelBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.service.DemografiService;
import id.seribu.seribuimport.service.KepatuhanService;
import id.seribu.seribuimport.service.PenerimaanService;
import id.seribu.seribuimport.service.SubyekDataService;
import id.seribu.seribuimport.service.WizardService;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.FileUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class WizardServiceImpl implements WizardService {

    ExcelBuilder builder;
    XSSFWorkbook workbook;

    ExcelUtil excelUtil = new ExcelUtil();

    SubyekDataService subyekDataService;

    @Autowired
    FileUtil fileUtil;

    @Autowired
    DemografiService demografiService;

    @Autowired
    KepatuhanService kepatuhanService;

    @Autowired
    PenerimaanService penerimaanService;

    @Override
    public DownloadResponseDto getExcelFile(WizardRequestDto wizardRequestDto) {
        log.info("WizardService.getExcelFile wizardRequest: {}", wizardRequestDto.toString());
        List<?> listModel = new ArrayList<>();
        switch (wizardRequestDto.getDataset()) {
        case ImportConstant.DEMOGRAFI:
            listModel = demografiService.getAllData(wizardRequestDto.getSubyekData(), wizardRequestDto.getSourceData(),
                    wizardRequestDto.getFilterByYear() != null && wizardRequestDto.getFilterByYear().length != 0
                            ? Arrays.asList(wizardRequestDto.getFilterByYear())
                            : null);
            break;

        case ImportConstant.KEPATUHAN:
            listModel = kepatuhanService.getAllData(wizardRequestDto.getSubyekData(), wizardRequestDto.getSourceData(),
                    wizardRequestDto.getFilterByYear() != null && wizardRequestDto.getFilterByYear().length != 0
                            ? Arrays.asList(wizardRequestDto.getFilterByYear())
                            : null);
            break;

        case ImportConstant.PENERIMAAN:
            listModel = penerimaanService.getAllData(wizardRequestDto.getSubyekData(), wizardRequestDto.getSourceData(),
                    wizardRequestDto.getFilterByYear() != null && wizardRequestDto.getFilterByYear().length != 0
                            ? Arrays.asList(wizardRequestDto.getFilterByYear())
                            : null);
            break;
        }

        workbook = new XSSFWorkbook();
        builder = new ExcelBuilder(workbook, new Date());

        builder.buildTitle(excelUtil.excelTitle(wizardRequestDto.getDataset(), wizardRequestDto.getSubyekData(), 
            wizardRequestDto.getColumnPivot()), excelUtil.formatDate(new Date(), ImportConstant.EXCEL_FORMAT_DATE))
            .buildPivotTabel(listModel, wizardRequestDto.getRowPivot(), wizardRequestDto.getColumnPivot())
            .sort();

        Resource resource = fileUtil.getFilResource(excelUtil.getExcelByte(workbook), this.setFilename(
                wizardRequestDto.getDataset(), wizardRequestDto.getSubyekData(), wizardRequestDto.getSourceData()));

        return new DownloadResponseDto(ImportConstant.XLSXTYPE, resource);
    }

    private String setFilename(String dataset, String subyekData, String sourceData) {
        return dataset + "_" + subyekData + "_" + sourceData + ".xlsx";
    }

}