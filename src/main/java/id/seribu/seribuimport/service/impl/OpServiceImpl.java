package id.seribu.seribuimport.service.impl;

import id.seribu.seribuimport.constant.HeaderConstant;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.dto.ObjectMappingDto;
import id.seribu.seribuimport.service.SubyekDataService;

public class OpServiceImpl implements SubyekDataService {

    @Override
    public ObjectMappingDto getArrayIndex(String datasource, String rowParam, String colParam, String sumParam) {
        int rowIndex = -1, colIndex = -1, summaryIndex = -1, size = 0;
        switch (datasource) {
        case ImportConstant.SourceData.WILAYAH:
            for (int i = 0; i < HeaderConstant.OPWILAYAH.length; i++) {
                if (HeaderConstant.OPWILAYAH[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.OPWILAYAH[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.OPWILAYAH[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.OPWILAYAH.length;
            break;

        case ImportConstant.SourceData.WILAYAHKLU:
            for (int i = 0; i < HeaderConstant.OPWILAYAHKLU.length; i++) {
                if (HeaderConstant.OPWILAYAHKLU[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.OPWILAYAHKLU[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.OPWILAYAHKLU[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.OPWILAYAHKLU.length;
            break;

        case ImportConstant.SourceData.KLU:
            for (int i = 0; i < HeaderConstant.OPKLU.length; i++) {
                if (HeaderConstant.OPKLU[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.OPKLU[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.OPKLU[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.OPKLU.length;
            break;

        case ImportConstant.SourceData.STATUS:
            for (int i = 0; i < HeaderConstant.OPSTATUS.length; i++) {
                if (HeaderConstant.OPSTATUS[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.OPSTATUS[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.OPSTATUS[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.OPSTATUS.length;
            break;
        }

        return new ObjectMappingDto(rowIndex, colIndex, summaryIndex, size);
    }

}