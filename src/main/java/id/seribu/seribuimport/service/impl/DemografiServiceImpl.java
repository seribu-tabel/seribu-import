package id.seribu.seribuimport.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.factory.demografi.BadanFactory;
import id.seribu.seribuimport.factory.demografi.BendaharaFactory;
import id.seribu.seribuimport.factory.demografi.OpFactory;
import id.seribu.seribuimport.factory.demografi.UmumFactory;
import id.seribu.seribuimport.repository.ImportFileRepo;
import id.seribu.seribuimport.service.DemografiService;
import id.seribu.seribuimport.service.HeaderColumnService;
import id.seribu.seribuimport.util.FileUtil;
import id.seribu.seribuimport.util.MathUtil;
import id.seribu.seribumodel.transaction.TImportFile;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DemografiServiceImpl implements DemografiService {

    @Autowired
    ImportFileRepo importFileRepo;

    @Autowired
    @Qualifier(value = "demografiOpFactory")
    OpFactory opFactory;

    @Autowired
    @Qualifier(value = "demografiBadanFactory")
    BadanFactory badanFactory;

    @Autowired
    @Qualifier(value = "demografiBendaharaFactory")
    BendaharaFactory bendaharaFactory;

    @Autowired
    @Qualifier(value = "demografiUmumFactory")
    UmumFactory umumFactory;

    @Autowired
    HeaderColumnService headerColumnService;

    @Autowired
    FileUtil fileUtil;

    @Autowired
    MathUtil mathUtil;

    private BigDecimal demoBadanAllCount = BigDecimal.ZERO;
    private BigDecimal demoBadanStatusCount = BigDecimal.ZERO;
    private BigDecimal demoBendaharaAllCount = BigDecimal.ZERO;
    private BigDecimal demoBendaharaStatusCount = BigDecimal.ZERO;
    private BigDecimal demoOpWilayahCount = BigDecimal.ZERO;
    private BigDecimal demoOpWilayahKluCount = BigDecimal.ZERO;
    private BigDecimal demoOpKluCount = BigDecimal.ZERO;
    private BigDecimal demoOpStatusCount = BigDecimal.ZERO;
    private BigDecimal demoUmumWilayahKluCount = BigDecimal.ZERO;
    private BigDecimal demoUmumStatusCount = BigDecimal.ZERO;

    @Override
    public List<?> getAllData(String subyekData, String datasource, List<String> tahun) {
        switch (subyekData) {
        case ImportConstant.SubyekData.OP:
            switch (datasource) {
            case ImportConstant.SourceData.WILAYAH:
                return opFactory.demoOpWilayahList(tahun);
            case ImportConstant.SourceData.WILAYAHKLU:
                return opFactory.demoOpWilayahKluList(tahun);
            case ImportConstant.SourceData.KLU:
                return opFactory.demoOpKluList(tahun);
            case ImportConstant.SourceData.STATUS:
                return opFactory.demoOpStatusList(tahun);
            }
            break;

        case ImportConstant.SubyekData.BADAN:
            switch (datasource) {
            case ImportConstant.SourceData.SEMUA:
                return badanFactory.demoBadanAll(tahun);
            case ImportConstant.SourceData.STATUS:
                return badanFactory.demoBadanStatus(tahun);
            }
            break;
        case ImportConstant.SubyekData.BENDAHARA:
            switch (datasource) {
            case ImportConstant.SourceData.SEMUA:
                return bendaharaFactory.demoBendaharaAll(tahun);
            case ImportConstant.SourceData.STATUS:
                return bendaharaFactory.demoBendaharaStatus(tahun);
            }
            break;
        case ImportConstant.SubyekData.UMUM:
            switch (datasource) {
            case ImportConstant.SourceData.WILAYAHKLU:
                return umumFactory.demoUmumWilayahKluList(tahun);
            case ImportConstant.SourceData.STATUS:
                return umumFactory.demoUmumStatusList(tahun);
            }
            break;
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataOp() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.DEMOGRAFI, ImportConstant.SubyekData.OP);
        if (listNotProcessedFile.size() == 0) {
            log.info("DemografiService.procesImportDataOp NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.OP);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("DemografiService.procesImportDataOp process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.WILAYAH:
                    opFactory.importOpWilayah(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.WILAYAHKLU:
                    opFactory.importOpWilayahKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.KLU:
                    opFactory.importOpKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    opFactory.importOpStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataBadan() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.DEMOGRAFI, ImportConstant.SubyekData.BADAN);
        if (listNotProcessedFile.size() == 0) {
            log.info("DemografiService.procesImportDataBadan NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.BADAN);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("DemografiService.procesImportDataBadan process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.SEMUA:
                    badanFactory.importBadanAll(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    badanFactory.importBadanStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataBendahara() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.DEMOGRAFI, ImportConstant.SubyekData.BENDAHARA);
        if (listNotProcessedFile.size() == 0) {
            log.info("DemografiService.procesImportDataBendahara NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.BENDAHARA);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("DemografiService.procesImportDataBendahara process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.SEMUA:
                    bendaharaFactory.importBendaharaAll(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    bendaharaFactory.importBendaharaStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void postProccessFile(TImportFile tImportFile) {
        log.info("DemografiService.postProccessFile update t_import_file, id: {}", tImportFile.getId());
        tImportFile.setProcess(true);
        tImportFile.setUpdatedBy("SYSTEM");
        importFileRepo.save(tImportFile);
        // backup already processed file
        fileUtil.backupFile(tImportFile.getDirectory(), tImportFile.getDirectory() + ImportConstant.PROCESSEDPATH,
                tImportFile.getFilename());
    }

    private String getDirWithFileName(String directory, String filename) {
        log.info("DemografiService.getDirWithFileName path: {}", directory + ImportConstant.PATH_PREFIX + filename);
        return directory + ImportConstant.PATH_PREFIX + filename;
    }

    private List<TImportFile> findFileNotProcess(String dataset, String subyekData) {
        log.info("DemografiService.findFileNotProcess dataset: {}, subyekData: {}", dataset, subyekData);
        return importFileRepo.findByDatasetNotProcess(dataset, subyekData);
    }

    @Override
    public BigDecimal getTotalDemografi() {
        log.info("DemografiService.getTotalDemografi");
        if (badanFactory.isAvailableDemoBadanAll()) {
            demoBadanAllCount = mathUtil.calcCombination(headerColumnService.badanAllSize(),
                    ImportConstant.COMBINATION);
        }

        if (badanFactory.isAvailableDemoBadanStatus()) {
            demoBadanStatusCount = mathUtil.calcCombination(headerColumnService.badanStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (bendaharaFactory.isAvailableDemoBendaharaAll()) {
            demoBendaharaAllCount = mathUtil.calcCombination(headerColumnService.bendaharaAllSize(),
                    ImportConstant.COMBINATION);
        }

        if (bendaharaFactory.isAvailableDemoBendaharaStatus()) {
            demoBendaharaStatusCount = mathUtil.calcCombination(headerColumnService.bendaharaStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableDemoOpWilayah()) {
            demoOpWilayahCount = mathUtil.calcCombination(headerColumnService.opWilayahSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableDemoOpWilayahKlu()) {
            demoOpWilayahKluCount = mathUtil.calcCombination(headerColumnService.opWilayahKluSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableDemoOpKlu()) {
            demoOpKluCount = mathUtil.calcCombination(headerColumnService.opKluSize(), ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailableDemoOpStatus()) {
            demoOpStatusCount = mathUtil.calcCombination(headerColumnService.opStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (umumFactory.isAvailableDemoUmumStatus()) {
            demoUmumStatusCount = mathUtil.calcCombination(headerColumnService.umumStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (umumFactory.isAvailableDemoUmumWilayahKlu()) {
            demoUmumWilayahKluCount = mathUtil.calcCombination(headerColumnService.umumWilayahKluSize(),
                    ImportConstant.COMBINATION);
        }

        return demoBadanAllCount.add(demoBadanStatusCount) 
                .add(demoBendaharaAllCount) 
                .add(demoBendaharaStatusCount) 
                .add(demoOpWilayahCount)
                .add(demoOpWilayahKluCount)
                .add(demoOpKluCount)
                .add(demoOpStatusCount)
                .add(demoUmumStatusCount)
                .add(demoUmumWilayahKluCount);
    }

    @Transactional
    @Override
    public void processImportDataUmum() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.DEMOGRAFI, ImportConstant.SubyekData.UMUM);
        if (listNotProcessedFile.size() == 0) {
            log.info("DemografiService.processImportDataUmum NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.UMUM);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("DemografiService.processImportDataUmum process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.WILAYAHKLU:
                    umumFactory.importUmumWilayahKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    umumFactory.importUmumStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

}