package id.seribu.seribuimport.service.impl;

import id.seribu.seribuimport.constant.HeaderConstant;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.dto.ObjectMappingDto;
import id.seribu.seribuimport.service.SubyekDataService;

public class BadanServiceImpl implements SubyekDataService {

    @Override
    public ObjectMappingDto getArrayIndex(String datasource, String rowParam, String colParam, String sumParam) {
        int rowIndex = -1, colIndex = -1, summaryIndex = -1, size = 0;
        switch (datasource) {
        case ImportConstant.SourceData.SEMUA:
            for (int i = 0; i < HeaderConstant.BADANALL.length; i++) {
                if (HeaderConstant.BADANALL[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.BADANALL[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.BADANALL[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.BADANALL.length;
            break;

        case ImportConstant.SourceData.STATUS:
            for (int i = 0; i < HeaderConstant.BADANSTATUS.length; i++) {
                if (HeaderConstant.BADANSTATUS[i].equals(rowParam)) {
                    rowIndex = i;
                }
                if (HeaderConstant.BADANSTATUS[i].equals(colParam)) {
                    colIndex = i;
                }
                if (HeaderConstant.BADANSTATUS[i].equals(sumParam)) {
                    summaryIndex = i;
                }
            }
            size = HeaderConstant.BADANSTATUS.length;
            break;
        }

        return new ObjectMappingDto(rowIndex, colIndex, summaryIndex, size);
    }

}