package id.seribu.seribuimport.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.ImportDto;
import id.seribu.seribudto.transaction.ImportResponseDto;
import id.seribu.seribuimport.config.FileUploadConfig;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.ImportFileRepo;
import id.seribu.seribuimport.service.AbstractProcessFile;
import id.seribu.seribuimport.service.UploadService;
import id.seribu.seribuimport.util.ModelUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UploadServiceImpl extends AbstractProcessFile implements UploadService {

    @Autowired
    FileUploadConfig fileUploadConfig;

    @Autowired
    ImportFileRepo importFileRepo;

    @Autowired
    ModelUtil modelUtil;

    @Transactional
    @Override
    public ApiResponse<ImportResponseDto> processFile(MultipartFile file, String dataset, String subyekData,
            String dataSource, String uploadedBy) {
        log.info("UploadService.processFile save data filename: {}", file.getOriginalFilename());
        if(importFileRepo.findByFilename(file.getOriginalFilename()).size() > 0) {
            importFileRepo.deleteByFileName(file.getOriginalFilename());
        }
        importFileRepo.save(modelUtil.createImportFile(this.createImportDto(dataset, subyekData, dataSource,
                this.generateDir(dataset, subyekData, dataSource), file.getOriginalFilename(), uploadedBy)));
        ImportResponseDto response = this.upload(file, this.createImportDto(dataset, subyekData, dataSource,
                this.generateDir(dataset, subyekData, dataSource), file.getOriginalFilename(), uploadedBy));
        return new ApiResponse<>(HttpStatus.OK.value(), "File berhasil diupload", response);
    }

    private ImportDto createImportDto(String dataset, String subyekData, String sourceData, String directory,
            String filename, String uploadedBy) {
        return new ImportDto(dataset, subyekData, sourceData, directory, filename, uploadedBy, new Date());
    }

    private String generateDir(String dataset, String subyekData, String sourceData) {
        log.info("UploadService.generateDir dataset: {}, subyekData: {}, soureData: {}", dataset, subyekData,
                sourceData);
        String parentDir = "";
        switch (dataset) {
        case ImportConstant.DEMOGRAFI:
            parentDir = ImportConstant.DataSetPath.DEMOGRAFI;
            break;
        case ImportConstant.KEPATUHAN:
            parentDir = ImportConstant.DataSetPath.KEPATUHAN;
            break;
        case ImportConstant.PENERIMAAN:
            parentDir = ImportConstant.DataSetPath.PENERIMAAN;
            break;
        }

        String path = fileUploadConfig.getUploadDir() + parentDir + ImportConstant.PATH_PREFIX + subyekData
                + ImportConstant.PATH_PREFIX + sourceData;
        return path;
    }
}