package id.seribu.seribuimport.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.factory.penerimaan.BadanFactory;
import id.seribu.seribuimport.factory.penerimaan.BendaharaFactory;
import id.seribu.seribuimport.factory.penerimaan.OpFactory;
import id.seribu.seribuimport.factory.penerimaan.UmumFactory;
import id.seribu.seribuimport.repository.ImportFileRepo;
import id.seribu.seribuimport.service.HeaderColumnService;
import id.seribu.seribuimport.service.PenerimaanService;
import id.seribu.seribuimport.util.FileUtil;
import id.seribu.seribuimport.util.MathUtil;
import id.seribu.seribumodel.transaction.TImportFile;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class PenerimaanServiceImpl implements PenerimaanService {

    @Autowired
    ImportFileRepo importFileRepo;

    @Autowired
    @Qualifier(value = "penerimaanOpFactory")
    OpFactory opFactory;

    @Autowired
    @Qualifier(value = "penerimaanBadanFactory")
    BadanFactory badanFactory;

    @Autowired
    @Qualifier(value = "penerimaanBendaharaFactory")
    BendaharaFactory bendaharaFactory;

    @Autowired
    @Qualifier(value = "penerimaanUmumFactory")
    UmumFactory umumFactory;

    @Autowired
    FileUtil fileUtil;

    @Autowired
    MathUtil mathUtil;

    @Autowired
    HeaderColumnService headerColumnService;

    private BigDecimal penerimaanBadanAllCount = BigDecimal.ZERO;
    private BigDecimal penerimaanBadanStatusCount = BigDecimal.ZERO;
    private BigDecimal penerimaanBendaharaAllCount = BigDecimal.ZERO;
    private BigDecimal penerimaanBendaharaStatusCount = BigDecimal.ZERO;
    private BigDecimal penerimaanOpWilayahCount = BigDecimal.ZERO;
    private BigDecimal penerimaanOpWilayahKluCount = BigDecimal.ZERO;
    private BigDecimal penerimaanOpKluCount = BigDecimal.ZERO;
    private BigDecimal penerimaanOpStatusCount = BigDecimal.ZERO;
    private BigDecimal penerimaanUmumStatusCount = BigDecimal.ZERO;
    private BigDecimal penerimaanUmumWilayahKluCount = BigDecimal.ZERO;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataOp() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.PENERIMAAN, ImportConstant.SubyekData.OP);
        if (listNotProcessedFile.size() == 0) {
            log.info("PenerimaanService.procesImportDataOp NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.OP);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("PenerimaanService.procesImportDataOp process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.WILAYAH:
                    opFactory.importOpWilayah(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.WILAYAHKLU:
                    opFactory.importOpWilayahKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.KLU:
                    opFactory.importOpKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    opFactory.importOpStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataBadan() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.PENERIMAAN, ImportConstant.SubyekData.BADAN);
        if (listNotProcessedFile.size() == 0) {
            log.info("PenerimaanService.procesImportDataBadan NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.BADAN);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("PenerimaanService.procesImportDataBadan process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.SEMUA:
                    badanFactory.importBadanAll(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    badanFactory.importBadanStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void procesImportDataBendahara() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.PENERIMAAN, ImportConstant.SubyekData.BENDAHARA);
        if (listNotProcessedFile.size() == 0) {
            log.info("PenerimaanService.procesImportDataBendahara NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.BENDAHARA);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("PenerimaanService.procesImportDataBendahara process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.SEMUA:
                    bendaharaFactory.importBendaharaAll(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    bendaharaFactory.importBendaharaStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void postProccessFile(TImportFile tImportFile) {
        log.info("PenerimaanService.postProccessFile update t_import_file, id: {}", tImportFile.getId());
        tImportFile.setProcess(true);
        tImportFile.setUpdatedBy("SYSTEM");
        importFileRepo.save(tImportFile);
        // backup already processed file
        fileUtil.backupFile(tImportFile.getDirectory(), tImportFile.getDirectory() + ImportConstant.PROCESSEDPATH,
                tImportFile.getFilename());
    }

    private String getDirWithFileName(String directory, String filename) {
        log.info("PenerimaanService.getDirWithFileName path: {}", directory + ImportConstant.PATH_PREFIX + filename);
        return directory + ImportConstant.PATH_PREFIX + filename;
    }

    private List<TImportFile> findFileNotProcess(String dataset, String subyekData) {
        log.info("PenerimaanService.findFileNotProcess dataset: {}, subyekData: {}", dataset, subyekData);
        return importFileRepo.findByDatasetNotProcess(dataset, subyekData);
    }

    @Override
    public BigDecimal getTotalPenerimaan() {
        log.info("PenerimaanService.getTotalPenerimaan");
        if (badanFactory.isAvailablePenerimaanBadanAll()) {
            penerimaanBadanAllCount = mathUtil.calcCombination(headerColumnService.badanAllSize(),
                    ImportConstant.COMBINATION);
        }

        if (badanFactory.isAvailablePenerimaanBadanStatus()) {
            penerimaanBadanStatusCount = mathUtil.calcCombination(headerColumnService.badanStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (bendaharaFactory.isAvailablePenerimaanBendaharaAll()) {
            penerimaanBendaharaAllCount = mathUtil.calcCombination(headerColumnService.bendaharaAllSize(),
                    ImportConstant.COMBINATION);
        }

        if (bendaharaFactory.isAvailablePenerimaanBendaharaStatus()) {
            penerimaanBendaharaStatusCount = mathUtil.calcCombination(headerColumnService.bendaharaStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailablePenerimaanOpWilayah()) {
            penerimaanOpWilayahCount = mathUtil.calcCombination(headerColumnService.opWilayahSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailablePenerimaanOpWilayahKlu()) {
            penerimaanOpWilayahKluCount = mathUtil.calcCombination(headerColumnService.opWilayahKluSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailablePenerimaanOpKlu()) {
            penerimaanOpKluCount = mathUtil.calcCombination(headerColumnService.opKluSize(),
                    ImportConstant.COMBINATION);
        }

        if (opFactory.isAvailablePenerimaanOpStatus()) {
            penerimaanOpStatusCount = mathUtil.calcCombination(headerColumnService.opStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (umumFactory.isAvailablePenerimaanUmumStatus()) {
            penerimaanUmumStatusCount = mathUtil.calcCombination(headerColumnService.umumStatusSize(),
                    ImportConstant.COMBINATION);
        }

        if (umumFactory.isAvailablePenerimaanUmumWilayahKlu()) {
            penerimaanUmumWilayahKluCount = mathUtil.calcCombination(headerColumnService.umumWilayahKluSize(),
                    ImportConstant.COMBINATION);
        }

        return penerimaanBadanAllCount.add(penerimaanBadanStatusCount)
                .add(penerimaanBendaharaAllCount).add(penerimaanBendaharaStatusCount)
                .add(penerimaanOpWilayahCount).add(penerimaanOpWilayahKluCount).add(penerimaanOpKluCount).add(penerimaanOpStatusCount)
                .add(penerimaanUmumStatusCount).add(penerimaanUmumWilayahKluCount);
    }

    @Override
    public List<?> getAllData(String subyekData, String datasource, List<String> tahun) {
        switch (subyekData) {
        case ImportConstant.SubyekData.OP:
            switch (datasource) {
            case ImportConstant.SourceData.WILAYAH:
                return opFactory.penerimaanOpWilayahList(tahun);
            case ImportConstant.SourceData.WILAYAHKLU:
                return opFactory.penerimaanOpWilayahKluList(tahun);
            case ImportConstant.SourceData.KLU:
                return opFactory.penerimaanOpKluList(tahun);
            case ImportConstant.SourceData.STATUS:
                return opFactory.penerimaanOpStatusList(tahun);
            }
            break;

        case ImportConstant.SubyekData.BADAN:
            switch (datasource) {
            case ImportConstant.SourceData.SEMUA:
                return badanFactory.penerimaanBadanAll(tahun);
            case ImportConstant.SourceData.STATUS:
                return badanFactory.penerimaanBadanStatus(tahun);
            }
            break;
        case ImportConstant.SubyekData.BENDAHARA:
            switch (datasource) {
            case ImportConstant.SourceData.SEMUA:
                return bendaharaFactory.penerimaanBendaharaAll(tahun);
            case ImportConstant.SourceData.STATUS:
                return bendaharaFactory.penerimaanBendaharaStatus(tahun);
            }
            break;
        case ImportConstant.SubyekData.UMUM:
            switch (datasource) {
            case ImportConstant.SourceData.WILAYAHKLU:
                return umumFactory.penerimaanUmumWilayahKluList(tahun);
            case ImportConstant.SourceData.STATUS:
                return umumFactory.penerimaanUmumStatusList(tahun);
            }
            break;
        }
        return null;
    }

    @Override
    public void processImportDataUmum() {
        List<TImportFile> listNotProcessedFile = new ArrayList<>();
        listNotProcessedFile = this.findFileNotProcess(ImportConstant.PENERIMAAN, ImportConstant.SubyekData.UMUM);
        if (listNotProcessedFile.size() == 0) {
            log.info("PenerimaanService.processImportDataUmum NO DATA AVAILABLE. subyekData: {}",
                    ImportConstant.SubyekData.UMUM);
        } else {
            for (TImportFile tImportFile : listNotProcessedFile) {
                log.info("PenerimaanService.processImportDataUmum process data size: {}, source: {}",
                        listNotProcessedFile.size(), tImportFile.getSourceData());
                switch (tImportFile.getSourceData()) {
                case ImportConstant.SourceData.WILAYAHKLU:
                    umumFactory.importUmumWilayahKlu(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                case ImportConstant.SourceData.STATUS:
                    umumFactory.importUmumStatus(
                            this.getDirWithFileName(tImportFile.getDirectory(), tImportFile.getFilename()),
                            tImportFile.getUpdatedBy());
                    break;
                }
                this.postProccessFile(tImportFile);
            }
        }

    }

}