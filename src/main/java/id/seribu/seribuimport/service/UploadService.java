package id.seribu.seribuimport.service;

import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.ImportResponseDto;

public interface UploadService {

    ApiResponse<ImportResponseDto> processFile(MultipartFile file, String dataset, String subyekData, String dataSource,
            String uploadedBy);
}