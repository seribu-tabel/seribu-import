package id.seribu.seribuimport.service;

import java.util.List;

import id.seribu.seribudto.shared.Lov;

public interface HeaderColumnService {

    List<Lov> getHeaderColumn(String subyekData, String sourceData);

    int badanAllSize();
    int badanStatusSize();
    int bendaharaStatusSize();
    int bendaharaAllSize();
    int opWilayahSize();
    int opWilayahKluSize();
    int opKluSize();
    int opStatusSize();
    int umumWilayahKluSize();
    int umumStatusSize();
}