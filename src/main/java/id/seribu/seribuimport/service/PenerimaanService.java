package id.seribu.seribuimport.service;

import java.math.BigDecimal;
import java.util.List;

public interface PenerimaanService {

    List<?> getAllData(String subyekData, String datasource, List<String> tahun);
    void procesImportDataOp();
    void procesImportDataBadan();
    void procesImportDataBendahara();
    void processImportDataUmum();
    BigDecimal getTotalPenerimaan();
}