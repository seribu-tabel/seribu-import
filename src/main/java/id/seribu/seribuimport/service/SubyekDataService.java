package id.seribu.seribuimport.service;

import id.seribu.seribuimport.dto.ObjectMappingDto;

public interface SubyekDataService {

    ObjectMappingDto getArrayIndex(String datasource, String rowParam, String colParam, String sumParam);

}