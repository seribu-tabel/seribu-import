package id.seribu.seribuimport.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import id.seribu.seribuimport.service.DemografiService;
import id.seribu.seribuimport.service.KepatuhanService;
import id.seribu.seribuimport.service.PenerimaanService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BatchProcessExcel {

    @Autowired
    DemografiService demografiService;

    @Autowired
    PenerimaanService penerimaanService;

    @Autowired
    KepatuhanService kepatuhanService;

    @Scheduled(cron = "${application.import.scheduler.demografi}")
    public void processDataDemografi() {
        try {
            log.info("BatchProcessExcel.processDataDemografi data bendahara start...");
            demografiService.procesImportDataBendahara();
            log.info("BatchProcessExcel.processDataDemografi data bendahara finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataDemografi data bendahara failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataPenerimaan data Badan start...");
            demografiService.procesImportDataBadan();
            log.info("BatchProcessExcel.processDataDemografi data Badan finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataDemografi data Badan failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataPenerimaan data op start...");
            demografiService.procesImportDataOp();
            log.info("BatchProcessExcel.processDataDemografi data op finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataDemografi data op failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataPenerimaan data umum start...");
            demografiService.processImportDataUmum();
            log.info("BatchProcessExcel.processDataDemografi data umum finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataDemografi data umum failed", e);
        }

    }

    @Scheduled(cron = "${application.import.scheduler.penerimaan}")
    public void processDataPenerimaan() {
        try {
            log.info("BatchProcessExcel.processDataPenerimaan data bendahara start...");
            penerimaanService.procesImportDataBendahara();
            log.info("BatchProcessExcel.processDataPenerimaan data bendahara finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataPenerimaan data bendahara failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataPenerimaan data badan start...");
            penerimaanService.procesImportDataBadan();
            log.info("BatchProcessExcel.processDataPenerimaan data badan finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataPenerimaan data badan failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataPenerimaan data op start...");
            penerimaanService.procesImportDataOp();
            log.info("BatchProcessExcel.processDataPenerimaan data op finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataPenerimaan data op failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataPenerimaan data umum start...");
            penerimaanService.processImportDataUmum();
            log.info("BatchProcessExcel.processDataPenerimaan data umum finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataPenerimaan data umum failed", e);
        }

    }

    @Scheduled(cron = "${application.import.scheduler.kepatuhan}")
    public void processDataKepatuhan() {
        try {
            log.info("BatchProcessExcel.processDataKepatuhan data bendahara start...");
            kepatuhanService.procesImportDataBendahara();
            log.info("BatchProcessExcel.processDataKepatuhan data bendahara finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataKepatuhan data bendahara failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataKepatuhan data badan start...");
            kepatuhanService.procesImportDataBadan();
            log.info("BatchProcessExcel.processDataKepatuhan data badan finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataKepatuhan data badan failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataKepatuhan data op start...");
            kepatuhanService.procesImportDataOp();
            log.info("BatchProcessExcel.processDataKepatuhan data op finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataKepatuhan data op failed", e);
        }

        try {
            log.info("BatchProcessExcel.processDataKepatuhan data umum start...");
            kepatuhanService.processImportDataUmum();
            log.info("BatchProcessExcel.processDataKepatuhan data umum finish...");
        } catch (Exception e) {
            log.error("BatchProcessExcel.processDataKepatuhan data umum failed", e);
        }
    }

}