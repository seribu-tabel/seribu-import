package id.seribu.seribuimport.factory;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import id.seribu.seribumodel.transaction.TDemoBadanAll;
import id.seribu.seribumodel.transaction.TDemoBadanStatus;
import id.seribu.seribumodel.transaction.TDemoBendaharaAll;
import id.seribu.seribumodel.transaction.TDemoBendaharaStatus;
import id.seribu.seribumodel.transaction.TDemoOpKlu;
import id.seribu.seribumodel.transaction.TDemoOpStatus;
import id.seribu.seribumodel.transaction.TDemoOpWilayah;
import id.seribu.seribumodel.transaction.TDemoOpWilayahKlu;
import id.seribu.seribumodel.transaction.TKepatuhanBadanAll;
import id.seribu.seribumodel.transaction.TKepatuhanBadanStatus;
import id.seribu.seribumodel.transaction.TKepatuhanBendaharaAll;
import id.seribu.seribumodel.transaction.TKepatuhanBendaharaStatus;
import id.seribu.seribumodel.transaction.TKepatuhanOpKlu;
import id.seribu.seribumodel.transaction.TKepatuhanOpStatus;
import id.seribu.seribumodel.transaction.TKepatuhanOpWilayah;
import id.seribu.seribumodel.transaction.TKepatuhanOpWilayahKlu;
import id.seribu.seribumodel.transaction.TPenerimaanBadanAll;
import id.seribu.seribumodel.transaction.TPenerimaanBadanStatus;
import id.seribu.seribumodel.transaction.TPenerimaanBendaharaAll;
import id.seribu.seribumodel.transaction.TPenerimaanBendaharaStatus;
import id.seribu.seribumodel.transaction.TPenerimaanOpKlu;
import id.seribu.seribumodel.transaction.TPenerimaanOpStatus;
import id.seribu.seribumodel.transaction.TPenerimaanOpWilayah;
import id.seribu.seribumodel.transaction.TPenerimaanOpWilayahKlu;

@Component
public class ExcelFactory {

    public CellStyle createExcelCellStyle(final XSSFWorkbook workbook, final Short backgroundColor,
            final BorderStyle borderTop, final BorderStyle borderBottom, final HorizontalAlignment alignment,
            final Short dataFormat, final XSSFFont font, boolean wrap) {
        final CellStyle cellStyle = workbook.createCellStyle();
        if (backgroundColor != null) {
            cellStyle.setFillBackgroundColor(backgroundColor);
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        if (borderTop != null) {
            cellStyle.setBorderTop(borderTop);
        }
        if (borderBottom != null) {
            cellStyle.setBorderTop(borderBottom);
        }
        if (alignment != null) {
            cellStyle.setAlignment(alignment);
        }
        if (dataFormat != null) {
            cellStyle.setDataFormat(dataFormat);
        }
        if (font != null) {
            cellStyle.setFont(font);
        }

        if (wrap) {
            cellStyle.setWrapText(true);
        }
        return cellStyle;
    }

    public short createExcelDataFormat(final XSSFWorkbook workbook, final String format) {
        final XSSFDataFormat df = workbook.createDataFormat();
        return df.getFormat(format);
    }

    public XSSFFont createExcelFont(final XSSFWorkbook workbook, final Boolean bold, final Integer fontHeight) {
        final XSSFFont font = workbook.createFont();
        if (bold) {
            font.setBold(bold);
        }
        if (fontHeight != null) {
            font.setFontHeight(fontHeight);
        }
        return font;
    }

    public <T> Object[] modelToObjectArray(T t) {
        if (t instanceof TDemoOpWilayah) {
            return new Object[] { ((TDemoOpWilayah) t).getId(), ((TDemoOpWilayah) t).getTahun(),
                    ((TDemoOpWilayah) t).getKodeKpp(), ((TDemoOpWilayah) t).getNamaKpp(),
                    ((TDemoOpWilayah) t).getKanwil(), ((TDemoOpWilayah) t).getProvinsi(),
                    ((TDemoOpWilayah) t).getPulau(), ((TDemoOpWilayah) t).getStatusPerkawinan(),
                    ((TDemoOpWilayah) t).getJumlahTanggungan(), ((TDemoOpWilayah) t).getJenisOp(),
                    ((TDemoOpWilayah) t).getRangeUsia(), ((TDemoOpWilayah) t).getJenisKelamin(),
                    ((TDemoOpWilayah) t).getWargaNegara(), ((TDemoOpWilayah) t).getJumlah(),
                    ((TDemoOpWilayah) t).getCreatedBy(), ((TDemoOpWilayah) t).getCreatedDate() };
        } else if (t instanceof TDemoOpWilayahKlu) {
            return new Object[] { ((TDemoOpWilayahKlu) t).getId(), ((TDemoOpWilayahKlu) t).getTahun(),
                    ((TDemoOpWilayahKlu) t).getKodeKpp(), ((TDemoOpWilayahKlu) t).getNamaKpp(),
                    ((TDemoOpWilayahKlu) t).getKanwil(), ((TDemoOpWilayahKlu) t).getProvinsi(),
                    ((TDemoOpWilayahKlu) t).getPulau(), ((TDemoOpWilayahKlu) t).getKodeKategori(),
                    ((TDemoOpWilayahKlu) t).getNamaKategori(), ((TDemoOpWilayahKlu) t).getJumlah(),
                    ((TDemoOpWilayahKlu) t).getCreatedBy(), ((TDemoOpWilayahKlu) t).getCreatedDate()
            };
        } else if(t instanceof TDemoOpKlu) {
            return new Object[] { ((TDemoOpKlu) t).getId(), ((TDemoOpKlu) t).getTahun(),
                ((TDemoOpKlu) t).getKodeKategori(), ((TDemoOpKlu) t).getNamaKategori(),
                ((TDemoOpKlu) t).getStatusPerkawinan(), ((TDemoOpKlu) t).getJumlahTanggungan(),
                ((TDemoOpKlu) t).getJenisOp(), ((TDemoOpKlu) t).getRangeUsia(),
                ((TDemoOpKlu) t).getJenisKelamin(), ((TDemoOpKlu) t).getWargaNegara(), ((TDemoOpKlu) t).getJumlah(),
                ((TDemoOpKlu) t).getCreatedBy(), ((TDemoOpKlu) t).getCreatedDate()
            };
        } else if(t instanceof TDemoOpStatus) {
            return new Object[] { ((TDemoOpStatus) t).getId(), ((TDemoOpStatus) t).getTahun(),
                ((TDemoOpStatus) t).getKodeKpp(), ((TDemoOpStatus) t).getNamaKpp(),
                ((TDemoOpStatus) t).getKanwil(), ((TDemoOpStatus) t).getProvinsi(),
                ((TDemoOpStatus) t).getPulau(), ((TDemoOpStatus) t).getKodeKategori(), ((TDemoOpStatus) t).getNamaKategori(),
                ((TDemoOpStatus) t).getStatusNpwp(), ((TDemoOpStatus) t).getStatusPkp(),
                ((TDemoOpStatus) t).getMetodePembukuan(), ((TDemoOpStatus) t).getJumlah(),
                ((TDemoOpStatus) t).getCreatedBy(), ((TDemoOpStatus) t).getCreatedDate()
            };        
        } else if(t instanceof TDemoBadanAll) {
            return new Object[] { ((TDemoBadanAll) t).getId(), ((TDemoBadanAll) t).getTahun(),
                ((TDemoBadanAll) t).getKodeKpp(), ((TDemoBadanAll) t).getNamaKpp(),
                ((TDemoBadanAll) t).getKanwil(), ((TDemoBadanAll) t).getProvinsi(),
                ((TDemoBadanAll) t).getPulau(), ((TDemoBadanAll) t).getKodeKategori(), ((TDemoBadanAll) t).getNamaKategori(),
                ((TDemoBadanAll) t).getKategoriWp(), ((TDemoBadanAll) t).getModal(),
                ((TDemoBadanAll) t).getStatusPusat(), ((TDemoBadanAll) t).getPeriodePembukuan(),
                ((TDemoBadanAll) t).getTahunPendirian(), ((TDemoBadanAll) t).getBadanHukum(),
                ((TDemoBadanAll) t).getJumlah(),
                ((TDemoBadanAll) t).getCreatedBy(), ((TDemoBadanAll) t).getCreatedDate()
            };  
        } else if(t instanceof TDemoBadanStatus) {
            return new Object[] { ((TDemoBadanStatus) t).getId(), ((TDemoBadanStatus) t).getTahun(),
                ((TDemoBadanStatus) t).getKodeKpp(), ((TDemoBadanStatus) t).getNamaKpp(),
                ((TDemoBadanStatus) t).getKanwil(), ((TDemoBadanStatus) t).getProvinsi(),
                ((TDemoBadanStatus) t).getPulau(), ((TDemoBadanStatus) t).getKodeKategori(), ((TDemoBadanStatus) t).getNamaKategori(),
                ((TDemoBadanStatus) t).getKategoriWp(), ((TDemoBadanStatus) t).getModal(),
                ((TDemoBadanStatus) t).getStatusPusat(), ((TDemoBadanStatus) t).getPeriodePembukuan(), 
                ((TDemoBadanStatus) t).getTahunPendirian(), ((TDemoBadanStatus) t).getBadanHukum(), 
                ((TDemoBadanStatus) t).getStatusNpwp(), ((TDemoBadanStatus) t).getStatusPkp(),  ((TDemoBadanStatus) t).getJumlah(),
                ((TDemoBadanStatus) t).getCreatedBy(), ((TDemoBadanStatus) t).getCreatedDate()
            };  
        } else if(t instanceof TDemoBendaharaAll) {
            return new Object[] { ((TDemoBendaharaAll) t).getId(), ((TDemoBendaharaAll) t).getTahun(),
                ((TDemoBendaharaAll) t).getKodeKpp(), ((TDemoBendaharaAll) t).getNamaKpp(),
                ((TDemoBendaharaAll) t).getKanwil(), ((TDemoBendaharaAll) t).getProvinsi(),
                ((TDemoBendaharaAll) t).getPulau(), ((TDemoBendaharaAll) t).getJenis(), ((TDemoBendaharaAll) t).getJumlah(),
                ((TDemoBendaharaAll) t).getCreatedBy(), ((TDemoBendaharaAll) t).getCreatedDate()
            };  
        } else if(t instanceof TDemoBendaharaStatus) {
            return new Object[] { ((TDemoBendaharaStatus) t).getId(), ((TDemoBendaharaStatus) t).getTahun(),
                ((TDemoBendaharaStatus) t).getKodeKpp(), ((TDemoBendaharaStatus) t).getNamaKpp(),
                ((TDemoBendaharaStatus) t).getKanwil(), ((TDemoBendaharaStatus) t).getProvinsi(),
                ((TDemoBendaharaStatus) t).getPulau(), ((TDemoBendaharaStatus) t).getJenis(), 
                ((TDemoBendaharaStatus) t).getStatusNpwp(), ((TDemoBendaharaStatus) t).getStatusPkp(), ((TDemoBendaharaStatus) t).getJumlah(),
                ((TDemoBendaharaStatus) t).getCreatedBy(), ((TDemoBendaharaStatus) t).getCreatedDate()
            };    
        } else if (t instanceof TKepatuhanOpWilayah) {
            return new Object[] { ((TKepatuhanOpWilayah) t).getId(), ((TKepatuhanOpWilayah) t).getTahun(),
                    ((TKepatuhanOpWilayah) t).getKodeKpp(), ((TKepatuhanOpWilayah) t).getNamaKpp(),
                    ((TKepatuhanOpWilayah) t).getKanwil(), ((TKepatuhanOpWilayah) t).getProvinsi(),
                    ((TKepatuhanOpWilayah) t).getPulau(), ((TKepatuhanOpWilayah) t).getStatusPerkawinan(),
                    ((TKepatuhanOpWilayah) t).getJumlahTanggungan(), ((TKepatuhanOpWilayah) t).getJenisOp(),
                    ((TKepatuhanOpWilayah) t).getRangeUsia(), ((TKepatuhanOpWilayah) t).getJenisKelamin(),
                    ((TKepatuhanOpWilayah) t).getWargaNegara(), ((TKepatuhanOpWilayah) t).getJumlah(),
                    ((TKepatuhanOpWilayah) t).getCreatedBy(), ((TKepatuhanOpWilayah) t).getCreatedDate() };
        } else if (t instanceof TKepatuhanOpWilayahKlu) {
            return new Object[] { ((TKepatuhanOpWilayahKlu) t).getId(), ((TKepatuhanOpWilayahKlu) t).getTahun(),
                    ((TKepatuhanOpWilayahKlu) t).getKodeKpp(), ((TKepatuhanOpWilayahKlu) t).getNamaKpp(),
                    ((TKepatuhanOpWilayahKlu) t).getKanwil(), ((TKepatuhanOpWilayahKlu) t).getProvinsi(),
                    ((TKepatuhanOpWilayahKlu) t).getPulau(), ((TKepatuhanOpWilayahKlu) t).getKodeKategori(),
                    ((TKepatuhanOpWilayahKlu) t).getNamaKategori(), ((TKepatuhanOpWilayahKlu) t).getJumlah(),
                    ((TKepatuhanOpWilayahKlu) t).getCreatedBy(), ((TKepatuhanOpWilayahKlu) t).getCreatedDate()
            };
        } else if(t instanceof TKepatuhanOpKlu) {
            return new Object[] { ((TKepatuhanOpKlu) t).getId(), ((TKepatuhanOpKlu) t).getTahun(),
                ((TKepatuhanOpKlu) t).getKodeKategori(), ((TKepatuhanOpKlu) t).getNamaKategori(),
                ((TKepatuhanOpKlu) t).getStatusPerkawinan(), ((TKepatuhanOpKlu) t).getJumlahTanggungan(),
                ((TKepatuhanOpKlu) t).getJenisOp(), ((TKepatuhanOpKlu) t).getRangeUsia(),
                ((TKepatuhanOpKlu) t).getJenisKelamin(), ((TKepatuhanOpKlu) t).getWargaNegara(), ((TKepatuhanOpKlu) t).getJumlah(),
                ((TKepatuhanOpKlu) t).getCreatedBy(), ((TKepatuhanOpKlu) t).getCreatedDate()
            };
        } else if(t instanceof TKepatuhanOpStatus) {
            return new Object[] { ((TKepatuhanOpStatus) t).getId(), ((TKepatuhanOpStatus) t).getTahun(),
                ((TKepatuhanOpStatus) t).getKodeKpp(), ((TKepatuhanOpStatus) t).getNamaKpp(),
                ((TKepatuhanOpStatus) t).getKanwil(), ((TKepatuhanOpStatus) t).getProvinsi(),
                ((TKepatuhanOpStatus) t).getPulau(), ((TKepatuhanOpStatus) t).getKodeKategori(), ((TKepatuhanOpStatus) t).getNamaKategori(),
                ((TKepatuhanOpStatus) t).getStatusNpwp(), ((TKepatuhanOpStatus) t).getStatusPkp(),
                ((TKepatuhanOpStatus) t).getMetodePembukuan(), ((TKepatuhanOpStatus) t).getJumlah(),
                ((TKepatuhanOpStatus) t).getCreatedBy(), ((TKepatuhanOpStatus) t).getCreatedDate()
            };        
        } else if(t instanceof TKepatuhanBadanAll) {
            return new Object[] { ((TKepatuhanBadanAll) t).getId(), ((TKepatuhanBadanAll) t).getTahun(),
                ((TKepatuhanBadanAll) t).getKodeKpp(), ((TKepatuhanBadanAll) t).getNamaKpp(),
                ((TKepatuhanBadanAll) t).getKanwil(), ((TKepatuhanBadanAll) t).getProvinsi(),
                ((TKepatuhanBadanAll) t).getPulau(), ((TKepatuhanBadanAll) t).getKodeKategori(), ((TKepatuhanBadanAll) t).getNamaKategori(),
                ((TKepatuhanBadanAll) t).getKategoriWp(), ((TKepatuhanBadanAll) t).getModal(),
                ((TKepatuhanBadanAll) t).getStatusPusat(), ((TKepatuhanBadanAll) t).getPeriodePembukuan(),
                ((TKepatuhanBadanAll) t).getTahunPendirian(), ((TKepatuhanBadanAll) t).getBadanHukum(),
                ((TKepatuhanBadanAll) t).getJumlah(),
                ((TKepatuhanBadanAll) t).getCreatedBy(), ((TKepatuhanBadanAll) t).getCreatedDate()
            };  
        } else if(t instanceof TKepatuhanBadanStatus) {
            return new Object[] { ((TKepatuhanBadanStatus) t).getId(), ((TKepatuhanBadanStatus) t).getTahun(),
                ((TKepatuhanBadanStatus) t).getKodeKpp(), ((TKepatuhanBadanStatus) t).getNamaKpp(),
                ((TKepatuhanBadanStatus) t).getKanwil(), ((TKepatuhanBadanStatus) t).getProvinsi(),
                ((TKepatuhanBadanStatus) t).getPulau(), ((TKepatuhanBadanStatus) t).getKodeKategori(), ((TKepatuhanBadanStatus) t).getNamaKategori(),
                ((TKepatuhanBadanStatus) t).getKategoriWp(), ((TKepatuhanBadanStatus) t).getModal(),
                ((TKepatuhanBadanStatus) t).getStatusPusat(), ((TKepatuhanBadanStatus) t).getPeriodePembukuan(), 
                ((TKepatuhanBadanStatus) t).getTahunPendirian(), ((TKepatuhanBadanStatus) t).getBadanHukum(), 
                ((TKepatuhanBadanStatus) t).getStatusNpwp(), ((TKepatuhanBadanStatus) t).getStatusPkp(),  ((TKepatuhanBadanStatus) t).getJumlah(),
                ((TKepatuhanBadanStatus) t).getCreatedBy(), ((TKepatuhanBadanStatus) t).getCreatedDate()
            };  
        } else if(t instanceof TKepatuhanBendaharaAll) {
            return new Object[] { ((TKepatuhanBendaharaAll) t).getId(), ((TKepatuhanBendaharaAll) t).getTahun(),
                ((TKepatuhanBendaharaAll) t).getKodeKpp(), ((TKepatuhanBendaharaAll) t).getNamaKpp(),
                ((TKepatuhanBendaharaAll) t).getKanwil(), ((TKepatuhanBendaharaAll) t).getProvinsi(),
                ((TKepatuhanBendaharaAll) t).getPulau(), ((TKepatuhanBendaharaAll) t).getJenis(), ((TKepatuhanBendaharaAll) t).getJumlah(),
                ((TKepatuhanBendaharaAll) t).getCreatedBy(), ((TKepatuhanBendaharaAll) t).getCreatedDate()
            };  
        } else if(t instanceof TKepatuhanBendaharaStatus) {
            return new Object[] { ((TKepatuhanBendaharaStatus) t).getId(), ((TKepatuhanBendaharaStatus) t).getTahun(),
                ((TKepatuhanBendaharaStatus) t).getKodeKpp(), ((TKepatuhanBendaharaStatus) t).getNamaKpp(),
                ((TKepatuhanBendaharaStatus) t).getKanwil(), ((TKepatuhanBendaharaStatus) t).getProvinsi(),
                ((TKepatuhanBendaharaStatus) t).getPulau(), ((TKepatuhanBendaharaStatus) t).getJenis(), 
                ((TKepatuhanBendaharaStatus) t).getStatusNpwp(), ((TKepatuhanBendaharaStatus) t).getStatusPkp(), ((TKepatuhanBendaharaStatus) t).getJumlah(),
                ((TKepatuhanBendaharaStatus) t).getCreatedBy(), ((TKepatuhanBendaharaStatus) t).getCreatedDate()
            };    
        } else if (t instanceof TPenerimaanOpWilayah) {
            return new Object[] { ((TPenerimaanOpWilayah) t).getId(), ((TPenerimaanOpWilayah) t).getTahun(),
                    ((TPenerimaanOpWilayah) t).getKodeKpp(), ((TPenerimaanOpWilayah) t).getNamaKpp(),
                    ((TPenerimaanOpWilayah) t).getKanwil(), ((TPenerimaanOpWilayah) t).getProvinsi(),
                    ((TPenerimaanOpWilayah) t).getPulau(), ((TPenerimaanOpWilayah) t).getStatusPerkawinan(),
                    ((TPenerimaanOpWilayah) t).getJumlahTanggungan(), ((TPenerimaanOpWilayah) t).getJenisOp(),
                    ((TPenerimaanOpWilayah) t).getRangeUsia(), ((TPenerimaanOpWilayah) t).getJenisKelamin(),
                    ((TPenerimaanOpWilayah) t).getWargaNegara(), ((TPenerimaanOpWilayah) t).getJumlah(),
                    ((TPenerimaanOpWilayah) t).getCreatedBy(), ((TPenerimaanOpWilayah) t).getCreatedDate() };
        } else if (t instanceof TPenerimaanOpWilayahKlu) {
            return new Object[] { ((TPenerimaanOpWilayahKlu) t).getId(), ((TPenerimaanOpWilayahKlu) t).getTahun(),
                    ((TPenerimaanOpWilayahKlu) t).getKodeKpp(), ((TPenerimaanOpWilayahKlu) t).getNamaKpp(),
                    ((TPenerimaanOpWilayahKlu) t).getKanwil(), ((TPenerimaanOpWilayahKlu) t).getProvinsi(),
                    ((TPenerimaanOpWilayahKlu) t).getPulau(), ((TPenerimaanOpWilayahKlu) t).getKodeKategori(),
                    ((TPenerimaanOpWilayahKlu) t).getNamaKategori(), ((TPenerimaanOpWilayahKlu) t).getJumlah(),
                    ((TPenerimaanOpWilayahKlu) t).getCreatedBy(), ((TPenerimaanOpWilayahKlu) t).getCreatedDate()
            };
        } else if(t instanceof TPenerimaanOpKlu) {
            return new Object[] { ((TPenerimaanOpKlu) t).getId(), ((TPenerimaanOpKlu) t).getTahun(),
                ((TPenerimaanOpKlu) t).getKodeKategori(), ((TPenerimaanOpKlu) t).getNamaKategori(),
                ((TPenerimaanOpKlu) t).getStatusPerkawinan(), ((TPenerimaanOpKlu) t).getJumlahTanggungan(),
                ((TPenerimaanOpKlu) t).getJenisOp(), ((TPenerimaanOpKlu) t).getRangeUsia(),
                ((TPenerimaanOpKlu) t).getJenisKelamin(), ((TPenerimaanOpKlu) t).getWargaNegara(), ((TPenerimaanOpKlu) t).getJumlah(),
                ((TPenerimaanOpKlu) t).getCreatedBy(), ((TPenerimaanOpKlu) t).getCreatedDate()
            };
        } else if(t instanceof TPenerimaanOpStatus) {
            return new Object[] { ((TPenerimaanOpStatus) t).getId(), ((TPenerimaanOpStatus) t).getTahun(),
                ((TPenerimaanOpStatus) t).getKodeKpp(), ((TPenerimaanOpStatus) t).getNamaKpp(),
                ((TPenerimaanOpStatus) t).getKanwil(), ((TPenerimaanOpStatus) t).getProvinsi(),
                ((TPenerimaanOpStatus) t).getPulau(), ((TPenerimaanOpStatus) t).getKodeKategori(), ((TPenerimaanOpStatus) t).getNamaKategori(),
                ((TPenerimaanOpStatus) t).getStatusNpwp(), ((TPenerimaanOpStatus) t).getStatusPkp(),
                ((TPenerimaanOpStatus) t).getMetodePembukuan(), ((TPenerimaanOpStatus) t).getJumlah(),
                ((TPenerimaanOpStatus) t).getCreatedBy(), ((TPenerimaanOpStatus) t).getCreatedDate()
            };        
        } else if(t instanceof TPenerimaanBadanAll) {
            return new Object[] { ((TPenerimaanBadanAll) t).getId(), ((TPenerimaanBadanAll) t).getTahun(),
                ((TPenerimaanBadanAll) t).getKodeKpp(), ((TPenerimaanBadanAll) t).getNamaKpp(),
                ((TPenerimaanBadanAll) t).getKanwil(), ((TPenerimaanBadanAll) t).getProvinsi(),
                ((TPenerimaanBadanAll) t).getPulau(), ((TPenerimaanBadanAll) t).getKodeKategori(), ((TPenerimaanBadanAll) t).getNamaKategori(),
                ((TPenerimaanBadanAll) t).getKategoriWp(), ((TPenerimaanBadanAll) t).getModal(),
                ((TPenerimaanBadanAll) t).getStatusPusat(), ((TPenerimaanBadanAll) t).getPeriodePembukuan(),
                ((TPenerimaanBadanAll) t).getTahunPendirian(), ((TPenerimaanBadanAll) t).getBadanHukum(),
                ((TPenerimaanBadanAll) t).getJumlah(),
                ((TPenerimaanBadanAll) t).getCreatedBy(), ((TPenerimaanBadanAll) t).getCreatedDate()
            };  
        } else if(t instanceof TPenerimaanBadanStatus) {
            return new Object[] { ((TPenerimaanBadanStatus) t).getId(), ((TPenerimaanBadanStatus) t).getTahun(),
                ((TPenerimaanBadanStatus) t).getKodeKpp(), ((TPenerimaanBadanStatus) t).getNamaKpp(),
                ((TPenerimaanBadanStatus) t).getKanwil(), ((TPenerimaanBadanStatus) t).getProvinsi(),
                ((TPenerimaanBadanStatus) t).getPulau(), ((TPenerimaanBadanStatus) t).getKodeKategori(), ((TPenerimaanBadanStatus) t).getNamaKategori(),
                ((TPenerimaanBadanStatus) t).getKategoriWp(), ((TPenerimaanBadanStatus) t).getModal(),
                ((TPenerimaanBadanStatus) t).getStatusPusat(), ((TPenerimaanBadanStatus) t).getPeriodePembukuan(), 
                ((TPenerimaanBadanStatus) t).getTahunPendirian(), ((TPenerimaanBadanStatus) t).getBadanHukum(), 
                ((TPenerimaanBadanStatus) t).getStatusNpwp(), ((TPenerimaanBadanStatus) t).getStatusPkp(),  ((TPenerimaanBadanStatus) t).getJumlah(),
                ((TPenerimaanBadanStatus) t).getCreatedBy(), ((TPenerimaanBadanStatus) t).getCreatedDate()
            };  
        } else if(t instanceof TPenerimaanBendaharaAll) {
            return new Object[] { ((TPenerimaanBendaharaAll) t).getId(), ((TPenerimaanBendaharaAll) t).getTahun(),
                ((TPenerimaanBendaharaAll) t).getKodeKpp(), ((TPenerimaanBendaharaAll) t).getNamaKpp(),
                ((TPenerimaanBendaharaAll) t).getKanwil(), ((TPenerimaanBendaharaAll) t).getProvinsi(),
                ((TPenerimaanBendaharaAll) t).getPulau(), ((TPenerimaanBendaharaAll) t).getJenis(), ((TPenerimaanBendaharaAll) t).getJumlah(),
                ((TPenerimaanBendaharaAll) t).getCreatedBy(), ((TPenerimaanBendaharaAll) t).getCreatedDate()
            };  
        } else if(t instanceof TPenerimaanBendaharaStatus) {
            return new Object[] { ((TPenerimaanBendaharaStatus) t).getId(), ((TPenerimaanBendaharaStatus) t).getTahun(),
                ((TPenerimaanBendaharaStatus) t).getKodeKpp(), ((TPenerimaanBendaharaStatus) t).getNamaKpp(),
                ((TPenerimaanBendaharaStatus) t).getKanwil(), ((TPenerimaanBendaharaStatus) t).getProvinsi(),
                ((TPenerimaanBendaharaStatus) t).getPulau(), ((TPenerimaanBendaharaStatus) t).getJenis(), 
                ((TPenerimaanBendaharaStatus) t).getStatusNpwp(), ((TPenerimaanBendaharaStatus) t).getStatusPkp(), ((TPenerimaanBendaharaStatus) t).getJumlah(),
                ((TPenerimaanBendaharaStatus) t).getCreatedBy(), ((TPenerimaanBendaharaStatus) t).getCreatedDate()
            };    
        } else {
            return null;
        }
    }
}