package id.seribu.seribuimport.factory.demografi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.demografi.DemografiDto;
import id.seribu.seribudto.demografi.badan.BadanAllDtoBuilder;
import id.seribu.seribudto.demografi.badan.BadanStatusDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.DemoBadanAllRepo;
import id.seribu.seribuimport.repository.DemoBadanStatusRepo;
import id.seribu.seribuimport.util.DemografiUtil;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribumodel.transaction.TDemoBadanAll;
import id.seribu.seribumodel.transaction.TDemoBadanStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "demografiBadanFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BadanFactory {

    ExcelUtil excelUtil;

    @Autowired
    DemografiUtil demografiUtil;

    @Autowired
    DemoBadanAllRepo demoBadanAllRepo;

    @Autowired
    DemoBadanStatusRepo demoBadanStatusRepo;

    public List<TDemoBadanAll> demoBadanAll(List<String> tahun) {
        List<TDemoBadanAll> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoBadanAllRepo.findByTahunIn(tahun);
        } else {
            demoBadanAllRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TDemoBadanStatus> demoBadanStatus(List<String> tahun) {
        List<TDemoBadanStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoBadanStatusRepo.findByTahunIn(tahun);
        } else {
            demoBadanStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBadanAll(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.BadanFactory.importBadanAll, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BadanAllDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .kategoriWp(dto.get(ImportConstant.Header.KATEGORIWP)).modal(dto.get(ImportConstant.Header.MODAL))
                    .statusPusat(dto.get(ImportConstant.Header.STATUSPUSAT))
                    .periodePembukuan(dto.get(ImportConstant.Header.PERIODEPEMBUKUAN))
                    .tahunPendirian(dto.get(ImportConstant.Header.TAHUNPENDIRIAN))
                    .badanHukum(dto.get(ImportConstant.Header.BADANHUKUM))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoBadanAllRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: Badan, source data: All");
            demoBadanAllRepo.saveAll(demografiUtil.createDemoBadanAllList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.BadanFactory.importBadanAll error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBadanStatus(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.BadanFactory.importBadanStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BadanStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .kategoriWp(dto.get(ImportConstant.Header.KATEGORIWP)).modal(dto.get(ImportConstant.Header.MODAL))
                    .statusPusat(dto.get(ImportConstant.Header.STATUSPUSAT))
                    .periodePembukuan(dto.get(ImportConstant.Header.PERIODEPEMBUKUAN))
                    .tahunPendirian(dto.get(ImportConstant.Header.TAHUNPENDIRIAN))
                    .badanHukum(dto.get(ImportConstant.Header.BADANHUKUM))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoBadanStatusRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: Badan, source data: Status");
            demoBadanStatusRepo.saveAll(demografiUtil.createDemoBadanStatusList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.BadanFactory.importBadanStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableDemoBadanAll() {
        log.info("Demografi.BadanFactory.isAvailableDemoBadanAll");
        if (demoBadanAllRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableDemoBadanStatus() {
        log.info("Demografi.BadanFactory.isAvailableDemoBadanStatus");
        if (demoBadanStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}