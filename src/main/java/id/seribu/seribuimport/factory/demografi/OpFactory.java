package id.seribu.seribuimport.factory.demografi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.demografi.DemografiDto;
import id.seribu.seribudto.demografi.op.OpKluDtoBuilder;
import id.seribu.seribudto.demografi.op.OpStatusDtoBuilder;
import id.seribu.seribudto.demografi.op.OpWilayahDtoBuilder;
import id.seribu.seribudto.demografi.op.OpWilayahKluDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.DemoOpKluRepo;
import id.seribu.seribuimport.repository.DemoOpStatusRepo;
import id.seribu.seribuimport.repository.DemoOpWilayahKluRepo;
import id.seribu.seribuimport.repository.DemoOpWilayahRepo;
import id.seribu.seribuimport.util.DemografiUtil;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribumodel.transaction.TDemoOpKlu;
import id.seribu.seribumodel.transaction.TDemoOpStatus;
import id.seribu.seribumodel.transaction.TDemoOpWilayah;
import id.seribu.seribumodel.transaction.TDemoOpWilayahKlu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "demografiOpFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class OpFactory {

    ExcelUtil excelUtil;

    @Autowired
    DemografiUtil demografiUtil;

    @Autowired
    DemoOpWilayahRepo demoOpWilayahRepo;

    @Autowired
    DemoOpWilayahKluRepo demoOpWilayahKluRepo;

    @Autowired
    DemoOpKluRepo demoOpKluRepo;

    @Autowired
    DemoOpStatusRepo demoOpStatusRepo;

    public List<TDemoOpWilayah> demoOpWilayahList(List<String> tahun) {
        List<TDemoOpWilayah> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoOpWilayahRepo.findByTahunIn(tahun);
        } else {
            demoOpWilayahRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TDemoOpWilayahKlu> demoOpWilayahKluList(List<String> tahun) {
        List<TDemoOpWilayahKlu> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoOpWilayahKluRepo.findByTahunIn(tahun);
        } else {
            demoOpWilayahKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TDemoOpKlu> demoOpKluList(List<String> tahun) {
        List<TDemoOpKlu> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoOpKluRepo.findByTahunIn(tahun);
        } else {
            demoOpKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TDemoOpStatus> demoOpStatusList(List<String> tahun) {
        List<TDemoOpStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoOpStatusRepo.findByTahunIn(tahun);
        } else {
            demoOpStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpWilayah(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.OpFactory.importOpWilayah, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpWilayahDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .statusPerkawinan(dto.get(ImportConstant.Header.STATUSPERKAWINAN))
                    .jumlahTanggungan(dto.get(ImportConstant.Header.JUMLAHTANGGUNGAN))
                    .jenisOp(dto.get(ImportConstant.Header.JENISOP)).rangeUsia(dto.get(ImportConstant.Header.RANGEUSIA))
                    .jenisKelamin(dto.get(ImportConstant.Header.JENISKELAMIN))
                    .wargaNegara(dto.get(ImportConstant.Header.WARGANEGARA))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoOpWilayahRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: OP, source data: Wilayah");
            demoOpWilayahRepo.saveAll(demografiUtil.createDemoOpWilayahList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.OpFactory.importOpWilayah error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpWilayahKlu(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.OpFactory.importOpWilayahKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpWilayahKluDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoOpWilayahKluRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: OP, source data: Wilayah KLU");
            demoOpWilayahKluRepo.saveAll(demografiUtil.createDemoOpWilayahKluList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.OpFactory.importOpWilayahKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpKlu(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.OpFactory.importOpKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream()
                    .map(dto -> new OpKluDtoBuilder().id().tahun(dto.get(ImportConstant.Header.TAHUN))
                            .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                            .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                            .statusPerkawinan(dto.get(ImportConstant.Header.STATUSPERKAWINAN))
                            .jumlahTanggungan(dto.get(ImportConstant.Header.JUMLAHTANGGUNGAN))
                            .jenisOp(dto.get(ImportConstant.Header.JENISOP))
                            .rangeUsia(dto.get(ImportConstant.Header.RANGEUSIA))
                            .jenisKelamin(dto.get(ImportConstant.Header.JENISKELAMIN))
                            .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy)
                            .build())
                    .collect(Collectors.toList());
            demoOpKluRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: OP, source data: KLU");
            demoOpKluRepo.saveAll(demografiUtil.createDemoOpKluList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.OpFactory.importOpKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpStatus(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.OpFactory.importOpStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .metodePembukuan(dto.get(ImportConstant.Header.METODEPEMBUKUAN))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoOpStatusRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: OP, source data: Status");
            demoOpStatusRepo.saveAll(demografiUtil.createDemoOpStatusList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.OpFactory.importOpStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableDemoOpWilayah() {
        log.info("Demografi.OpFactory.isAvailableDemoOpWilayah");
        if (demoOpWilayahRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableDemoOpWilayahKlu() {
        log.info("Demografi.OpFactory.isAvailableDemoOpWilayahKlu");
        if (demoOpWilayahKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableDemoOpKlu() {
        log.info("Demografi.OpFactory.isAvailableDemoOpKlu");
        if (demoOpKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableDemoOpStatus() {
        log.info("Demografi.OpFactory.isAvailableDemoOpStatus");
        if (demoOpStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

}