package id.seribu.seribuimport.factory.demografi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.demografi.DemografiDto;
import id.seribu.seribudto.demografi.umum.UmumStatusDtoBuilder;
import id.seribu.seribudto.demografi.umum.UmumWilayahKluDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.DemoUmumStatusRepo;
import id.seribu.seribuimport.repository.DemoUmumWilayahKluRepo;
import id.seribu.seribuimport.util.DemografiUtil;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribumodel.transaction.TDemoUmumStatus;
import id.seribu.seribumodel.transaction.TDemoUmumWilayahKlu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "demografiUmumFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UmumFactory {

    ExcelUtil excelUtil;

    @Autowired
    DemografiUtil demografiUtil;

    @Autowired
    DemoUmumWilayahKluRepo demoUmumWilayahKluRepo;

    @Autowired
    DemoUmumStatusRepo demoUmumStatusRepo;

    public List<TDemoUmumWilayahKlu> demoUmumWilayahKluList(final List<String> tahun) {
        List<TDemoUmumWilayahKlu> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoUmumWilayahKluRepo.findByTahunIn(tahun);
        } else {
            demoUmumWilayahKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TDemoUmumStatus> demoUmumStatusList(final List<String> tahun) {
        List<TDemoUmumStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoUmumStatusRepo.findByTahunIn(tahun);
        } else {
            demoUmumStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional
    public void importUmumWilayahKlu(final String pathWithFile, final String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.UmumFactory.importUmumWilayahKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new UmumWilayahKluDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .jenisWp(dto.get(ImportConstant.Header.JENISWP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoUmumWilayahKluRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: Umum, source data: Wilayah KLU");
            demoUmumWilayahKluRepo.saveAll(demografiUtil.createDemoUmumWilayahKluList(lDtos));
        } catch (final Exception e) {
            log.error("Demografi.UmumFactory.importUmumWilayahKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional
    public void importUmumStatus(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.UmumFactory.importUmumStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new UmumStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN))
                    .kodeKpp(dto.get(ImportConstant.Header.KODEKPP)).namaKpp(dto.get(ImportConstant.Header.NAMAKPP))
                    .kanwil(dto.get(ImportConstant.Header.KANWIL)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP)).jenisWp(dto.get(ImportConstant.Header.JENISWP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoUmumStatusRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: Umum, source data: Status");
            demoUmumStatusRepo.saveAll(demografiUtil.createDemoUmumStatusList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.UmumFactory.importUmumStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableDemoUmumWilayahKlu() {
        log.info("Demografi.UmumFactory.isAvailableDemoUmumWilayahKlu");
        if (demoUmumWilayahKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableDemoUmumStatus() {
        log.info("Demografi.UmumFactory.isAvailableDemoUmumStatus");
        if (demoUmumStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}