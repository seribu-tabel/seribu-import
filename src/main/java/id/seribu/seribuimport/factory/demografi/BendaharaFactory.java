package id.seribu.seribuimport.factory.demografi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.demografi.DemografiDto;
import id.seribu.seribudto.demografi.bendahara.BendaharaAllDtoBuilder;
import id.seribu.seribudto.demografi.bendahara.BendaharaStatusDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.DemoBendaharaAllRepo;
import id.seribu.seribuimport.repository.DemoBendaharaStatusRepo;
import id.seribu.seribuimport.util.DemografiUtil;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribumodel.transaction.TDemoBendaharaAll;
import id.seribu.seribumodel.transaction.TDemoBendaharaStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "demografiBendaharaFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BendaharaFactory {

    ExcelUtil excelUtil;

    @Autowired
    DemografiUtil demografiUtil;

    @Autowired
    DemoBendaharaAllRepo demoBendaharaAllRepo;

    @Autowired
    DemoBendaharaStatusRepo demoBendaharaStatusRepo;

    public List<TDemoBendaharaAll> demoBendaharaAll(List<String> tahun) {
        List<TDemoBendaharaAll> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoBendaharaAllRepo.findByTahunIn(tahun);
        } else {
            demoBendaharaAllRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TDemoBendaharaStatus> demoBendaharaStatus(List<String> tahun) {
        List<TDemoBendaharaStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = demoBendaharaStatusRepo.findByTahunIn(tahun);
        } else {
            demoBendaharaStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBendaharaAll(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.BendaharaFactory.importBendaharaAll, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BendaharaAllDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .jenis(dto.get(ImportConstant.Header.JENIS))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoBendaharaAllRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: Bendahara, source data: All");
            demoBendaharaAllRepo.saveAll(demografiUtil.createDemoBendaharaAllList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.BendaharaFactory.importBendaharaAll error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBendaharaStatus(String pathWithFile, String uploadedBy) {
        List<DemografiDto> lDtos = new ArrayList<>();
        try {
            log.info("Demografi.BendaharaFactory.importBendaharaStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BendaharaStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .jenis(dto.get(ImportConstant.Header.JENIS)).statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            demoBendaharaStatusRepo.truncateData();
            log.info("save dataset: Demografi, subyek data: Bendahara, source data: Status");
            demoBendaharaStatusRepo.saveAll(demografiUtil.createDemoBendaharaStatusList(lDtos));
        } catch (Exception e) {
            log.error("Demografi.BendaharaFactory.importBendaharaStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableDemoBendaharaAll() {
        log.info("Demografi.BendaharaFactory.isAvailableDemoBendaharaAll");
        if (demoBendaharaAllRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableDemoBendaharaStatus() {
        log.info("Demografi.BendaharaFactory.isAvailableDemoBendaharaStatus");
        if (demoBendaharaStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}