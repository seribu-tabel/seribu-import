package id.seribu.seribuimport.factory.penerimaan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import id.seribu.seribudto.penerimaan.badan.BadanAllDtoBuilder;
import id.seribu.seribudto.penerimaan.badan.BadanStatusDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.PenerimaanBadanAllRepo;
import id.seribu.seribuimport.repository.PenerimaanBadanStatusRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.PenerimaanUtil;
import id.seribu.seribumodel.transaction.TPenerimaanBadanAll;
import id.seribu.seribumodel.transaction.TPenerimaanBadanStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "penerimaanBadanFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BadanFactory {

    ExcelUtil excelUtil;

    @Autowired
    PenerimaanUtil penerimaanUtil;

    @Autowired
    PenerimaanBadanAllRepo penerimaanBadanAllRepo;

    @Autowired
    PenerimaanBadanStatusRepo penerimaanBadanStatusRepo;

    public List<TPenerimaanBadanAll> penerimaanBadanAll(List<String> tahun) {
        List<TPenerimaanBadanAll> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanBadanAllRepo.findByTahunIn(tahun);
        } else {
            penerimaanBadanAllRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TPenerimaanBadanStatus> penerimaanBadanStatus(List<String> tahun) {
        List<TPenerimaanBadanStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanBadanStatusRepo.findByTahunIn(tahun);
        } else {
            penerimaanBadanStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBadanAll(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.BadanFactory.importBadanAll, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BadanAllDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .kategoriWp(dto.get(ImportConstant.Header.KATEGORIWP)).modal(dto.get(ImportConstant.Header.MODAL))
                    .statusPusat(dto.get(ImportConstant.Header.STATUSPUSAT))
                    .periodePembukuan(dto.get(ImportConstant.Header.PERIODEPEMBUKUAN))
                    .tahunPendirian(dto.get(ImportConstant.Header.TAHUNPENDIRIAN))
                    .badanHukum(dto.get(ImportConstant.Header.BADANHUKUM))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanBadanAllRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: Badan, source data: All");
            penerimaanBadanAllRepo.saveAll(penerimaanUtil.createPenerimaanBadanAllList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.BadanFactory.importBadanAll error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBadanStatus(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.BadanFactory.importBadanStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BadanStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .kategoriWp(dto.get(ImportConstant.Header.KATEGORIWP)).modal(dto.get(ImportConstant.Header.MODAL))
                    .statusPusat(dto.get(ImportConstant.Header.STATUSPUSAT))
                    .periodePembukuan(dto.get(ImportConstant.Header.PERIODEPEMBUKUAN))
                    .tahunPendirian(dto.get(ImportConstant.Header.TAHUNPENDIRIAN))
                    .badanHukum(dto.get(ImportConstant.Header.BADANHUKUM))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanBadanStatusRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: Badan, source data: Status");
            penerimaanBadanStatusRepo.saveAll(penerimaanUtil.createPenerimaanBadanStatusList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.BadanFactory.importBadanStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailablePenerimaanBadanAll() {
        if (penerimaanBadanAllRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailablePenerimaanBadanStatus() {
        if (penerimaanBadanStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}