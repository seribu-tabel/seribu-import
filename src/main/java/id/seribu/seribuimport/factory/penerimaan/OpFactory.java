package id.seribu.seribuimport.factory.penerimaan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import id.seribu.seribudto.penerimaan.op.OpKluDtoBuilder;
import id.seribu.seribudto.penerimaan.op.OpStatusDtoBuilder;
import id.seribu.seribudto.penerimaan.op.OpWilayahDtoBuilder;
import id.seribu.seribudto.penerimaan.op.OpWilayahKluDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.PenerimaanOpKluRepo;
import id.seribu.seribuimport.repository.PenerimaanOpStatusRepo;
import id.seribu.seribuimport.repository.PenerimaanOpWilayahKluRepo;
import id.seribu.seribuimport.repository.PenerimaanOpWilayahRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.PenerimaanUtil;
import id.seribu.seribumodel.transaction.TPenerimaanOpKlu;
import id.seribu.seribumodel.transaction.TPenerimaanOpStatus;
import id.seribu.seribumodel.transaction.TPenerimaanOpWilayah;
import id.seribu.seribumodel.transaction.TPenerimaanOpWilayahKlu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "penerimaanOpFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class OpFactory {

    ExcelUtil excelUtil;

    @Autowired
    PenerimaanUtil penerimaanUtil;

    @Autowired
    PenerimaanOpWilayahRepo penerimaanOpWilayahRepo;

    @Autowired
    PenerimaanOpWilayahKluRepo penerimaanOpWilayahKluRepo;

    @Autowired
    PenerimaanOpKluRepo penerimaanOpKluRepo;

    @Autowired
    PenerimaanOpStatusRepo penerimaanOpStatusRepo;

    public List<TPenerimaanOpWilayah> penerimaanOpWilayahList(List<String> tahun) {
        List<TPenerimaanOpWilayah> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanOpWilayahRepo.findByTahunIn(tahun);
        } else {
            penerimaanOpWilayahRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TPenerimaanOpWilayahKlu> penerimaanOpWilayahKluList(List<String> tahun) {
        List<TPenerimaanOpWilayahKlu> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanOpWilayahKluRepo.findByTahunIn(tahun);
        } else {
            penerimaanOpWilayahKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TPenerimaanOpKlu> penerimaanOpKluList(List<String> tahun) {
        List<TPenerimaanOpKlu> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanOpKluRepo.findByTahunIn(tahun);
        } else {
            penerimaanOpKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TPenerimaanOpStatus> penerimaanOpStatusList(List<String> tahun) {
        List<TPenerimaanOpStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanOpStatusRepo.findByTahunIn(tahun);
        } else {
            penerimaanOpStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpWilayah(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.OpFactory.importOpWilayah, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpWilayahDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .statusPerkawinan(dto.get(ImportConstant.Header.STATUSPERKAWINAN))
                    .jumlahTanggungan(dto.get(ImportConstant.Header.JUMLAHTANGGUNGAN))
                    .jenisOp(dto.get(ImportConstant.Header.JENISOP)).rangeUsia(dto.get(ImportConstant.Header.RANGEUSIA))
                    .jenisKelamin(dto.get(ImportConstant.Header.JENISKELAMIN))
                    .wargaNegara(dto.get(ImportConstant.Header.WARGANEGARA))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanOpWilayahRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: OP, source data: Wilayah");
            penerimaanOpWilayahRepo.saveAll(penerimaanUtil.createPenerimaanOpWilayahList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.OpFactory.importOpWilayah error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpWilayahKlu(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.OpFactory.importOpWilayahKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpWilayahKluDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanOpWilayahKluRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: OP, source data: Wilayah KLU");
            penerimaanOpWilayahKluRepo.saveAll(penerimaanUtil.createPenerimaanOpWilayahKluList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.OpFactory.importOpWilayahKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpKlu(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.OpFactory.importOpKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream()
                    .map(dto -> new OpKluDtoBuilder().id().tahun(dto.get(ImportConstant.Header.TAHUN))
                            .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                            .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                            .statusPerkawinan(dto.get(ImportConstant.Header.STATUSPERKAWINAN))
                            .jumlahTanggungan(dto.get(ImportConstant.Header.JUMLAHTANGGUNGAN))
                            .jenisOp(dto.get(ImportConstant.Header.JENISOP))
                            .rangeUsia(dto.get(ImportConstant.Header.RANGEUSIA))
                            .jenisKelamin(dto.get(ImportConstant.Header.JENISKELAMIN))
                            .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy)
                            .build())
                    .collect(Collectors.toList());
            penerimaanOpKluRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: OP, source data: KLU");
            penerimaanOpKluRepo.saveAll(penerimaanUtil.createPenerimaanOpKluList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.OpFactory.importOpKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpStatus(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.OpFactory.importOpStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .metodePembukuan(dto.get(ImportConstant.Header.METODEPEMBUKUAN))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanOpStatusRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: OP, source data: Status");
            penerimaanOpStatusRepo.saveAll(penerimaanUtil.createPenerimaanOpStatusList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.OpFactory.importOpStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailablePenerimaanOpWilayah() {
        if (penerimaanOpWilayahRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailablePenerimaanOpWilayahKlu() {
        if (penerimaanOpWilayahKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailablePenerimaanOpKlu() {
        if (penerimaanOpKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailablePenerimaanOpStatus() {
        if (penerimaanOpStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}