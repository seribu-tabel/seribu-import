package id.seribu.seribuimport.factory.penerimaan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import id.seribu.seribudto.penerimaan.umum.UmumStatusDtoBuilder;
import id.seribu.seribudto.penerimaan.umum.UmumWilayahKluDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.PenerimaanUmumStatusRepo;
import id.seribu.seribuimport.repository.PenerimaanUmumWilayahKluRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.PenerimaanUtil;
import id.seribu.seribumodel.transaction.TPenerimaanUmumStatus;
import id.seribu.seribumodel.transaction.TPenerimaanUmumWilayahKlu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "penerimaanUmumFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UmumFactory {

    ExcelUtil excelUtil;

    @Autowired
    PenerimaanUtil penerimaanUtil;

    @Autowired
    PenerimaanUmumWilayahKluRepo penerimaanUmumWilayahKluRepo;

    @Autowired
    PenerimaanUmumStatusRepo penerimaanUmumStatusRepo;

    public List<TPenerimaanUmumWilayahKlu> penerimaanUmumWilayahKluList(final List<String> tahun) {
        List<TPenerimaanUmumWilayahKlu> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanUmumWilayahKluRepo.findByTahunIn(tahun);
        } else {
            penerimaanUmumWilayahKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TPenerimaanUmumStatus> penerimaanUmumStatusList(final List<String> tahun) {
        List<TPenerimaanUmumStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanUmumStatusRepo.findByTahunIn(tahun);
        } else {
            penerimaanUmumStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importUmumWilayahKlu(final String pathWithFile, final String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.UmumFactory.importUmumWilayahKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new UmumWilayahKluDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .jenisWp(dto.get(ImportConstant.Header.JENISWP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanUmumWilayahKluRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: Umum, source data: Wilayah KLU");
            penerimaanUmumWilayahKluRepo.saveAll(penerimaanUtil.createPenerimaanUmumWilayahKluList(lDtos));
        } catch (final Exception e) {
            log.error("Penerimaan.UmumFactory.importUmumWilayahKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importUmumStatus(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.UmumFactory.importUmumStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new UmumStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP)).jenisWp(dto.get(ImportConstant.Header.JENISWP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanUmumStatusRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: Umum, source data: Status");
            penerimaanUmumStatusRepo.saveAll(penerimaanUtil.createPenerimaanUmumStatusList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.UmumFactory.importUmumStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailablePenerimaanUmumWilayahKlu() {
        log.info("Penerimaan.UmumFactory.isAvailablePenerimaanUmumWilayahKlu");
        if (penerimaanUmumWilayahKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailablePenerimaanUmumStatus() {
        log.info("Penerimaan.UmumFactory.isAvailablePenerimaanUmumStatus");
        if (penerimaanUmumStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}