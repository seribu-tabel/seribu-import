package id.seribu.seribuimport.factory.penerimaan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import id.seribu.seribudto.penerimaan.bendahara.BendaharaAllDtoBuilder;
import id.seribu.seribudto.penerimaan.bendahara.BendaharaStatusDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.PenerimaanBendaharaAllRepo;
import id.seribu.seribuimport.repository.PenerimaanBendaharaStatusRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.PenerimaanUtil;
import id.seribu.seribumodel.transaction.TPenerimaanBendaharaAll;
import id.seribu.seribumodel.transaction.TPenerimaanBendaharaStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "penerimaanBendaharaFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BendaharaFactory {

    ExcelUtil excelUtil;

    @Autowired
    PenerimaanUtil penerimaanUtil;

    @Autowired
    PenerimaanBendaharaAllRepo penerimaanBendaharaAllRepo;

    @Autowired
    PenerimaanBendaharaStatusRepo penerimaanBendaharaStatusRepo;

    public List<TPenerimaanBendaharaAll> penerimaanBendaharaAll(List<String> tahun) {
        List<TPenerimaanBendaharaAll> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanBendaharaAllRepo.findByTahunIn(tahun);
        } else {
            penerimaanBendaharaAllRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TPenerimaanBendaharaStatus> penerimaanBendaharaStatus(List<String> tahun) {
        List<TPenerimaanBendaharaStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = penerimaanBendaharaStatusRepo.findByTahunIn(tahun);
        } else {
            penerimaanBendaharaStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBendaharaAll(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.BendaharaFactory.importBendaharaAll, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BendaharaAllDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .jenis(dto.get(ImportConstant.Header.JENIS))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanBendaharaAllRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: Bendahara, source data: All");
            penerimaanBendaharaAllRepo.saveAll(penerimaanUtil.createPenerimaanBendaharaAllList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.BendaharaFactory.importBendaharaAll error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBendaharaStatus(String pathWithFile, String uploadedBy) {
        List<PenerimaanDto> lDtos = new ArrayList<>();
        try {
            log.info("Penerimaan.BendaharaFactory.importBendaharaStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BendaharaStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .jenis(dto.get(ImportConstant.Header.JENIS)).statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            penerimaanBendaharaStatusRepo.truncateData();
            log.info("save dataset: Penerimaan, subyek data: Bendahara, source data: Status");
            penerimaanBendaharaStatusRepo.saveAll(penerimaanUtil.createPenerimaanBendaharaStatusList(lDtos));
        } catch (Exception e) {
            log.error("Penerimaan.BendaharaFactory.importBendaharaStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailablePenerimaanBendaharaAll() {
        if (penerimaanBendaharaAllRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailablePenerimaanBendaharaStatus() {
        if (penerimaanBendaharaStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}