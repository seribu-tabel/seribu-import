package id.seribu.seribuimport.factory.kepatuhan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.kepatuhan.KepatuhanDto;
import id.seribu.seribudto.kepatuhan.umum.UmumStatusDtoBuilder;
import id.seribu.seribudto.kepatuhan.umum.UmumWilayahKluDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.KepatuhanUmumStatusRepo;
import id.seribu.seribuimport.repository.KepatuhanUmumWilayahKluRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.KepatuhanUtil;
import id.seribu.seribumodel.transaction.TKepatuhanUmumStatus;
import id.seribu.seribumodel.transaction.TKepatuhanUmumWilayahKlu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "kepatuhanUmumFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UmumFactory {

    ExcelUtil excelUtil;

    @Autowired
    KepatuhanUtil kepatuhanUtil;

    @Autowired
    KepatuhanUmumWilayahKluRepo kepatuhanUmumWilayahKluRepo;

    @Autowired
    KepatuhanUmumStatusRepo kepatuhanUmumStatusRepo;

    public List<TKepatuhanUmumWilayahKlu> kepatuhanUmumWilayahKluList(final List<String> tahun) {
        List<TKepatuhanUmumWilayahKlu> listT = new ArrayList<>();
        if (tahun != null) {
            listT = kepatuhanUmumWilayahKluRepo.findByTahunIn(tahun);
        } else {
            kepatuhanUmumWilayahKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TKepatuhanUmumStatus> kepatuhanUmumStatusList(final List<String> tahun) {
        List<TKepatuhanUmumStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = kepatuhanUmumStatusRepo.findByTahunIn(tahun);
        } else {
            kepatuhanUmumStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importUmumWilayahKlu(final String pathWithFile, final String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.UmumFactory.importUmumWilayahKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new UmumWilayahKluDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .jenisWp(dto.get(ImportConstant.Header.JENISWP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            kepatuhanUmumWilayahKluRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: Umum, source data: Wilayah KLU");
            kepatuhanUmumWilayahKluRepo.saveAll(kepatuhanUtil.createKepatuhanUmumWilayahKluList(lDtos));
        } catch (final Exception e) {
            log.error("Kepatuhan.UmumFactory.importUmumWilayahKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importUmumStatus(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.UmumFactory.importUmumStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new UmumStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP)).jenisWp(dto.get(ImportConstant.Header.JENISWP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            kepatuhanUmumStatusRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: Umum, source data: Status");
            kepatuhanUmumStatusRepo.saveAll(kepatuhanUtil.createKepatuhanUmumStatusList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.UmumFactory.importUmumStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableKepatuhanUmumWilayahKlu() {
        log.info("Kepatuhan.UmumFactory.isAvailableKepatuhanUmumWilayahKlu");
        if (kepatuhanUmumWilayahKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableKepatuhanUmumStatus() {
        log.info("Kepatuhan.UmumFactory.isAvailableKepatuhanUmumStatus");
        if (kepatuhanUmumStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}