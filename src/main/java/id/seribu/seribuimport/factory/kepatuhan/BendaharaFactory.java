package id.seribu.seribuimport.factory.kepatuhan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.kepatuhan.KepatuhanDto;
import id.seribu.seribudto.kepatuhan.bendahara.BendaharaAllDtoBuilder;
import id.seribu.seribudto.kepatuhan.bendahara.BendaharaStatusDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.KepatuhanBendaharaAllRepo;
import id.seribu.seribuimport.repository.KepatuhanBendaharaStatusRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.KepatuhanUtil;
import id.seribu.seribumodel.transaction.TKepatuhanBendaharaAll;
import id.seribu.seribumodel.transaction.TKepatuhanBendaharaStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "kepatuhanBendaharaFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BendaharaFactory {

    ExcelUtil excelUtil;

    @Autowired
    KepatuhanUtil kepatuhanUtil;

    @Autowired
    KepatuhanBendaharaAllRepo kepatuhanBendaharaAllRepo;

    @Autowired
    KepatuhanBendaharaStatusRepo kepatuhanBendaharaStatusRepo;

    public List<TKepatuhanBendaharaAll> kepatuhanBendaharaAll(List<String> tahun) {
        List<TKepatuhanBendaharaAll> listT = new ArrayList<>();
        if (tahun != null) {
            listT = kepatuhanBendaharaAllRepo.findByTahunIn(tahun);
        } else {
            kepatuhanBendaharaAllRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TKepatuhanBendaharaStatus> kepatuhanBendaharaStatus(List<String> tahun) {
        List<TKepatuhanBendaharaStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = kepatuhanBendaharaStatusRepo.findByTahunIn(tahun);
        } else {
            kepatuhanBendaharaStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBendaharaAll(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.BendaharaFactory.importBendaharaAll, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BendaharaAllDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .jenis(dto.get(ImportConstant.Header.JENIS))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            kepatuhanBendaharaAllRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: Bendahara, source data: All");
            kepatuhanBendaharaAllRepo.saveAll(kepatuhanUtil.createKepatuhanBendaharaAllList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.BendaharaFactory.importBendaharaAll error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBendaharaStatus(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.BendaharaFactory.importBendaharaStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BendaharaStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .jenis(dto.get(ImportConstant.Header.JENIS)).statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            kepatuhanBendaharaStatusRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: Bendahara, source data: Status");
            kepatuhanBendaharaStatusRepo.saveAll(kepatuhanUtil.createKepatuhanBendaharaStatusList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.BendaharaFactory.importBendaharaStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableKepatuhanBendaharaAll() {
        if (kepatuhanBendaharaAllRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableKepatuhanBendaharaStatus() {
        if (kepatuhanBendaharaStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}