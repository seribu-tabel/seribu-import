package id.seribu.seribuimport.factory.kepatuhan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.kepatuhan.KepatuhanDto;
import id.seribu.seribudto.kepatuhan.badan.BadanAllDtoBuilder;
import id.seribu.seribudto.kepatuhan.badan.BadanStatusDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.KepatuhanBadanAllRepo;
import id.seribu.seribuimport.repository.KepatuhanBadanStatusRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.KepatuhanUtil;
import id.seribu.seribumodel.transaction.TKepatuhanBadanAll;
import id.seribu.seribumodel.transaction.TKepatuhanBadanStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "kepatuhanBadanFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BadanFactory {

    ExcelUtil excelUtil;

    @Autowired
    KepatuhanUtil kepatuhanUtil;

    @Autowired
    KepatuhanBadanAllRepo kepatuhanBadanAllRepo;

    @Autowired
    KepatuhanBadanStatusRepo kepatuhanBadanStatusRepo;

    public List<TKepatuhanBadanAll> kepatuhanBadanAll(List<String> tahun) {
        List<TKepatuhanBadanAll> listT = new ArrayList<>();
        if (tahun != null) {
            listT = kepatuhanBadanAllRepo.findByTahunIn(tahun);
        } else {
            kepatuhanBadanAllRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TKepatuhanBadanStatus> kepatuhanBadanStatus(List<String> tahun) {
        List<TKepatuhanBadanStatus> listT = new ArrayList<>();
        if (tahun != null) {
            listT = kepatuhanBadanStatusRepo.findByTahunIn(tahun);
        } else {
            kepatuhanBadanStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBadanAll(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.BadanFactory.importBadanAll, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BadanAllDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .kategoriWp(dto.get(ImportConstant.Header.KATEGORIWP)).modal(dto.get(ImportConstant.Header.MODAL))
                    .statusPusat(dto.get(ImportConstant.Header.STATUSPUSAT))
                    .periodePembukuan(dto.get(ImportConstant.Header.PERIODEPEMBUKUAN))
                    .tahunPendirian(dto.get(ImportConstant.Header.TAHUNPENDIRIAN))
                    .badanHukum(dto.get(ImportConstant.Header.BADANHUKUM))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            kepatuhanBadanAllRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: Badan, source data: All");
            kepatuhanBadanAllRepo.saveAll(kepatuhanUtil.createKepatuhanBadanAllList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.BadanFactory.importBadanAll error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importBadanStatus(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.BadanFactory.importBadanStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new BadanStatusDtoBuilder().id()
                    .tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI)).pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .kategoriWp(dto.get(ImportConstant.Header.KATEGORIWP)).modal(dto.get(ImportConstant.Header.MODAL))
                    .statusPusat(dto.get(ImportConstant.Header.STATUSPUSAT))
                    .periodePembukuan(dto.get(ImportConstant.Header.PERIODEPEMBUKUAN))
                    .tahunPendirian(dto.get(ImportConstant.Header.TAHUNPENDIRIAN))
                    .badanHukum(dto.get(ImportConstant.Header.BADANHUKUM))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH))).createdBy(uploadedBy).build())
                    .collect(Collectors.toList());
            kepatuhanBadanStatusRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: Badan, source data: Status");
            kepatuhanBadanStatusRepo.saveAll(kepatuhanUtil.createKepatuhanBadanStatusList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.BadanFactory.importBadanStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableKepatuhanBadanAll() {
        if (kepatuhanBadanAllRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableKepatuhanBadanStatus() {
        if (kepatuhanBadanStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

}