package id.seribu.seribuimport.factory.kepatuhan;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.kepatuhan.KepatuhanDto;
import id.seribu.seribudto.kepatuhan.op.OpKluDtoBuilder;
import id.seribu.seribudto.kepatuhan.op.OpStatusDtoBuilder;
import id.seribu.seribudto.kepatuhan.op.OpWilayahDtoBuilder;
import id.seribu.seribudto.kepatuhan.op.OpWilayahKluDtoBuilder;
import id.seribu.seribuimport.constant.ImportConstant;
import id.seribu.seribuimport.repository.KepatuhanOpKluRepo;
import id.seribu.seribuimport.repository.KepatuhanOpStatusRepo;
import id.seribu.seribuimport.repository.KepatuhanOpWilayahKluRepo;
import id.seribu.seribuimport.repository.KepatuhanOpWilayahRepo;
import id.seribu.seribuimport.util.ExcelUtil;
import id.seribu.seribuimport.util.KepatuhanUtil;
import id.seribu.seribumodel.transaction.TKepatuhanOpKlu;
import id.seribu.seribumodel.transaction.TKepatuhanOpStatus;
import id.seribu.seribumodel.transaction.TKepatuhanOpWilayah;
import id.seribu.seribumodel.transaction.TKepatuhanOpWilayahKlu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component(value = "kepatuhanOpFactory")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class OpFactory {

    ExcelUtil excelUtil;

    @Autowired
    KepatuhanUtil kepatuhanUtil;

    @Autowired
    KepatuhanOpWilayahRepo kepatuhanOpWilayahRepo;

    @Autowired
    KepatuhanOpWilayahKluRepo kepatuhanOpWilayahKluRepo;

    @Autowired
    KepatuhanOpKluRepo kepatuhanOpKluRepo;

    @Autowired
    KepatuhanOpStatusRepo kepatuhanOpStatusRepo;

    public List<TKepatuhanOpWilayah> kepatuhanOpWilayahList(List<String> tahun) {
        List<TKepatuhanOpWilayah> listT = new ArrayList<>();
        if(tahun != null) {
            listT = kepatuhanOpWilayahRepo.findByTahunIn(tahun);
        } else {
            kepatuhanOpWilayahRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TKepatuhanOpWilayahKlu> kepatuhanOpWilayahKluList(List<String> tahun) {
        List<TKepatuhanOpWilayahKlu> listT = new ArrayList<>();
        if(tahun != null) {
            listT = kepatuhanOpWilayahKluRepo.findByTahunIn(tahun);
        } else {
            kepatuhanOpWilayahKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TKepatuhanOpKlu> kepatuhanOpKluList(List<String> tahun) {
        List<TKepatuhanOpKlu> listT = new ArrayList<>();
        if(tahun != null) {
            listT = kepatuhanOpKluRepo.findByTahunIn(tahun);
        } else {
            kepatuhanOpKluRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    public List<TKepatuhanOpStatus> kepatuhanOpStatusList(List<String> tahun) {
        List<TKepatuhanOpStatus> listT = new ArrayList<>();
        if(tahun != null) {
            listT = kepatuhanOpStatusRepo.findByTahunIn(tahun);
        } else {
            kepatuhanOpStatusRepo.findAll().iterator().forEachRemaining(listT::add);
        }
        return listT;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpWilayah(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.OpFactory.importOpWilayah, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpWilayahDtoBuilder()
                    .id().tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .statusPerkawinan(dto.get(ImportConstant.Header.STATUSPERKAWINAN))
                    .jumlahTanggungan(dto.get(ImportConstant.Header.JUMLAHTANGGUNGAN))
                    .jenisOp(dto.get(ImportConstant.Header.JENISOP)).rangeUsia(dto.get(ImportConstant.Header.RANGEUSIA))
                    .jenisKelamin(dto.get(ImportConstant.Header.JENISKELAMIN))
                    .wargaNegara(dto.get(ImportConstant.Header.WARGANEGARA))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH)))
                    .createdBy(uploadedBy)
                    .build())
                    .collect(Collectors.toList());
            kepatuhanOpWilayahRepo.truncateData();     
            log.info("save dataset: Kepatuhan, subyek data: OP, source data: Wilayah");     
            kepatuhanOpWilayahRepo.saveAll(kepatuhanUtil.createKepatuhanOpWilayahList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.OpFactory.importOpWilayah error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpWilayahKlu(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.OpFactory.importOpWilayahKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpWilayahKluDtoBuilder()
                    .id().tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP)).provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU)).kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH)))
                    .createdBy(uploadedBy)
                    .build())
                    .collect(Collectors.toList());
            kepatuhanOpWilayahKluRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: OP, source data: Wilayah KLU");
            kepatuhanOpWilayahKluRepo.saveAll(kepatuhanUtil.createKepatuhanOpWilayahKluList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.OpFactory.importOpWilayahKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpKlu(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.OpFactory.importOpKlu, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpKluDtoBuilder()
                    .id().tahun(dto.get(ImportConstant.Header.TAHUN)).kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .statusPerkawinan(dto.get(ImportConstant.Header.STATUSPERKAWINAN))
                    .jumlahTanggungan(dto.get(ImportConstant.Header.JUMLAHTANGGUNGAN))
                    .jenisOp(dto.get(ImportConstant.Header.JENISOP))
                    .rangeUsia(dto.get(ImportConstant.Header.RANGEUSIA))
                    .jenisKelamin(dto.get(ImportConstant.Header.JENISKELAMIN))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH)))
                    .createdBy(uploadedBy)
                    .build())
                    .collect(Collectors.toList());
            kepatuhanOpKluRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: OP, source data: KLU");
            kepatuhanOpKluRepo.saveAll(kepatuhanUtil.createKepatuhanOpKluList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.OpFactory.importOpKlu error filename: {}", pathWithFile, e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void importOpStatus(String pathWithFile, String uploadedBy) {
        List<KepatuhanDto> lDtos = new ArrayList<>();
        try {
            log.info("Kepatuhan.OpFactory.importOpStatus, path: {}", pathWithFile);
            excelUtil = new ExcelUtil(pathWithFile);
            lDtos = excelUtil.readRows().stream().map(dto -> new OpStatusDtoBuilder()
                    .id().tahun(dto.get(ImportConstant.Header.TAHUN))
                    .kodeKpp(dto.get(ImportConstant.Header.KODEKPP))
                    .namaKpp(dto.get(ImportConstant.Header.NAMAKPP))
                    .kanwil(dto.get(ImportConstant.Header.KANWIL))
                    .provinsi(dto.get(ImportConstant.Header.PROVINSI))
                    .pulau(dto.get(ImportConstant.Header.PULAU))
                    .kodeKategori(dto.get(ImportConstant.Header.KODEKATEGORI))
                    .namaKategori(dto.get(ImportConstant.Header.NAMAKATEGORI))
                    .statusNpwp(dto.get(ImportConstant.Header.STATUSNPWP))
                    .statusPkp(dto.get(ImportConstant.Header.STATUSPKP))
                    .metodePembukuan(dto.get(ImportConstant.Header.METODEPEMBUKUAN))
                    .jumlah(Integer.parseInt(dto.get(ImportConstant.Header.JUMLAH)))
                    .createdBy(uploadedBy)
                    .build())
                    .collect(Collectors.toList());
            kepatuhanOpStatusRepo.truncateData();
            log.info("save dataset: Kepatuhan, subyek data: OP, source data: Status");
            kepatuhanOpStatusRepo.saveAll(kepatuhanUtil.createKepatuhanOpStatusList(lDtos));
        } catch (Exception e) {
            log.error("Kepatuhan.OpFactory.importOpStatus error filename: {}", pathWithFile, e);
        }
    }

    public boolean isAvailableKepatuhanOpWilayah() {
        if (kepatuhanOpWilayahRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableKepatuhanOpWilayahKlu() {
        if (kepatuhanOpWilayahKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableKepatuhanOpKlu() {
        if (kepatuhanOpKluRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }

    public boolean isAvailableKepatuhanOpStatus() {
        if (kepatuhanOpStatusRepo.validateAvailable().size() > 0) {
            return true;
        }
        return false;
    }
}