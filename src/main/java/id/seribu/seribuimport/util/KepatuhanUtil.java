package id.seribu.seribuimport.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import id.seribu.seribudto.kepatuhan.KepatuhanDto;
import id.seribu.seribumodel.transaction.TKepatuhanBadanAll;
import id.seribu.seribumodel.transaction.TKepatuhanBadanStatus;
import id.seribu.seribumodel.transaction.TKepatuhanBendaharaAll;
import id.seribu.seribumodel.transaction.TKepatuhanBendaharaStatus;
import id.seribu.seribumodel.transaction.TKepatuhanOpKlu;
import id.seribu.seribumodel.transaction.TKepatuhanOpStatus;
import id.seribu.seribumodel.transaction.TKepatuhanOpWilayah;
import id.seribu.seribumodel.transaction.TKepatuhanOpWilayahKlu;
import id.seribu.seribumodel.transaction.TKepatuhanUmumStatus;
import id.seribu.seribumodel.transaction.TKepatuhanUmumWilayahKlu;

@Component
public class KepatuhanUtil {

    public List<TKepatuhanOpWilayah> createKepatuhanOpWilayahList (List<KepatuhanDto> dto) {
        List<TKepatuhanOpWilayah> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanOpWilayah tKepatuhanOpWilayah = new TKepatuhanOpWilayah();
            tKepatuhanOpWilayah.setId(dt.getId());
            tKepatuhanOpWilayah.setTahun(dt.getTahun());
            tKepatuhanOpWilayah.setKodeKpp(dt.getKodeKpp());
            tKepatuhanOpWilayah.setNamaKpp(dt.getNamaKpp());
            tKepatuhanOpWilayah.setKanwil(dt.getKanwil());
            tKepatuhanOpWilayah.setProvinsi(dt.getProvinsi());
            tKepatuhanOpWilayah.setPulau(dt.getPulau());
            tKepatuhanOpWilayah.setStatusPerkawinan(dt.getStatusPerkawinan());
            tKepatuhanOpWilayah.setJumlahTanggungan(dt.getJumlahTanggungan());
            tKepatuhanOpWilayah.setJenisOp(dt.getJenisOp());
            tKepatuhanOpWilayah.setRangeUsia(dt.getRangeUsia());
            tKepatuhanOpWilayah.setJenisKelamin(dt.getJenisKelamin());
            tKepatuhanOpWilayah.setWargaNegara(dt.getWargaNegara());
            tKepatuhanOpWilayah.setJumlah(dt.getJumlah());
            tKepatuhanOpWilayah.setCreatedBy(dt.getCreatedBy());
            tKepatuhanOpWilayah.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanOpWilayah;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanOpWilayahKlu> createKepatuhanOpWilayahKluList(List<KepatuhanDto> dto) {
        List<TKepatuhanOpWilayahKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanOpWilayahKlu tKepatuhanOpWilayahKlu = new TKepatuhanOpWilayahKlu();
            tKepatuhanOpWilayahKlu.setId(dt.getId());
            tKepatuhanOpWilayahKlu.setTahun(dt.getTahun());
            tKepatuhanOpWilayahKlu.setKodeKpp(dt.getKodeKpp());
            tKepatuhanOpWilayahKlu.setNamaKpp(dt.getNamaKpp());
            tKepatuhanOpWilayahKlu.setKanwil(dt.getKanwil());
            tKepatuhanOpWilayahKlu.setProvinsi(dt.getProvinsi());
            tKepatuhanOpWilayahKlu.setPulau(dt.getPulau());
            tKepatuhanOpWilayahKlu.setKodeKategori(dt.getKodeKategori());
            tKepatuhanOpWilayahKlu.setNamaKategori(dt.getNamaKategori());
            tKepatuhanOpWilayahKlu.setJumlah(dt.getJumlah());
            tKepatuhanOpWilayahKlu.setCreatedBy(dt.getCreatedBy());
            tKepatuhanOpWilayahKlu.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanOpWilayahKlu;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanOpKlu> createKepatuhanOpKluList(List<KepatuhanDto> dto) {
        List<TKepatuhanOpKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanOpKlu tKepatuhanOpKlu = new TKepatuhanOpKlu();
            tKepatuhanOpKlu.setId(dt.getId());
            tKepatuhanOpKlu.setTahun(dt.getTahun());
            tKepatuhanOpKlu.setKodeKategori(dt.getKodeKategori());
            tKepatuhanOpKlu.setNamaKategori(dt.getNamaKategori());
            tKepatuhanOpKlu.setStatusPerkawinan(dt.getStatusPerkawinan());
            tKepatuhanOpKlu.setJumlahTanggungan(dt.getJumlahTanggungan());
            tKepatuhanOpKlu.setJenisOp(dt.getJenisOp());
            tKepatuhanOpKlu.setRangeUsia(dt.getRangeUsia());
            tKepatuhanOpKlu.setJenisKelamin(dt.getJenisKelamin());
            tKepatuhanOpKlu.setWargaNegara(dt.getWargaNegara());
            tKepatuhanOpKlu.setJumlah(dt.getJumlah());
            tKepatuhanOpKlu.setCreatedBy(dt.getCreatedBy());
            tKepatuhanOpKlu.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanOpKlu;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanOpStatus> createKepatuhanOpStatusList(List<KepatuhanDto> dto) {
        List<TKepatuhanOpStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanOpStatus tKepatuhanOpStatus = new TKepatuhanOpStatus();
            tKepatuhanOpStatus.setId(dt.getId());
            tKepatuhanOpStatus.setTahun(dt.getTahun());
            tKepatuhanOpStatus.setKodeKpp(dt.getKodeKpp());
            tKepatuhanOpStatus.setNamaKpp(dt.getNamaKpp());
            tKepatuhanOpStatus.setKanwil(dt.getKanwil());
            tKepatuhanOpStatus.setProvinsi(dt.getProvinsi());
            tKepatuhanOpStatus.setPulau(dt.getPulau());
            tKepatuhanOpStatus.setKodeKategori(dt.getKodeKategori());
            tKepatuhanOpStatus.setNamaKategori(dt.getNamaKategori());
            tKepatuhanOpStatus.setStatusNpwp(dt.getStatusNpwp());
            tKepatuhanOpStatus.setStatusPkp(dt.getStatusPkp());
            tKepatuhanOpStatus.setMetodePembukuan(dt.getMetodePembukuan());
            tKepatuhanOpStatus.setJumlah(dt.getJumlah());
            tKepatuhanOpStatus.setCreatedBy(dt.getCreatedBy());
            tKepatuhanOpStatus.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanOpStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanBadanStatus> createKepatuhanBadanStatusList(List<KepatuhanDto> dto) {
        List<TKepatuhanBadanStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanBadanStatus tKepatuhanBadanStatus = new TKepatuhanBadanStatus();
            tKepatuhanBadanStatus.setId(dt.getId());
            tKepatuhanBadanStatus.setTahun(dt.getTahun());
            tKepatuhanBadanStatus.setKodeKpp(dt.getKodeKpp());
            tKepatuhanBadanStatus.setNamaKpp(dt.getNamaKpp());
            tKepatuhanBadanStatus.setKanwil(dt.getKanwil());
            tKepatuhanBadanStatus.setProvinsi(dt.getProvinsi());
            tKepatuhanBadanStatus.setPulau(dt.getPulau());
            tKepatuhanBadanStatus.setKodeKategori(dt.getKodeKategori());
            tKepatuhanBadanStatus.setNamaKategori(dt.getNamaKategori());
            tKepatuhanBadanStatus.setKategoriWp(dt.getKategoriWp());
            tKepatuhanBadanStatus.setModal(dt.getModal());
            tKepatuhanBadanStatus.setStatusPusat(dt.getStatusPusat());
            tKepatuhanBadanStatus.setPeriodePembukuan(dt.getPeriodePembukuan());
            tKepatuhanBadanStatus.setTahunPendirian(dt.getTahunPendirian());
            tKepatuhanBadanStatus.setBadanHukum(dt.getBadanHukum());
            tKepatuhanBadanStatus.setStatusNpwp(dt.getStatusNpwp());
            tKepatuhanBadanStatus.setStatusPkp(dt.getStatusPkp());
            tKepatuhanBadanStatus.setJumlah(dt.getJumlah());
            tKepatuhanBadanStatus.setCreatedBy(dt.getCreatedBy());
            tKepatuhanBadanStatus.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanBadanStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanBadanAll> createKepatuhanBadanAllList(List<KepatuhanDto> dto) {
        List<TKepatuhanBadanAll> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanBadanAll tKepatuhanBadanAll = new TKepatuhanBadanAll();
            tKepatuhanBadanAll.setId(dt.getId());
            tKepatuhanBadanAll.setTahun(dt.getTahun());
            tKepatuhanBadanAll.setKodeKpp(dt.getKodeKpp());
            tKepatuhanBadanAll.setNamaKpp(dt.getNamaKpp());
            tKepatuhanBadanAll.setKanwil(dt.getKanwil());
            tKepatuhanBadanAll.setProvinsi(dt.getProvinsi());
            tKepatuhanBadanAll.setPulau(dt.getPulau());
            tKepatuhanBadanAll.setKodeKategori(dt.getKodeKategori());
            tKepatuhanBadanAll.setNamaKategori(dt.getNamaKategori());
            tKepatuhanBadanAll.setKategoriWp(dt.getKategoriWp());
            tKepatuhanBadanAll.setModal(dt.getModal());
            tKepatuhanBadanAll.setStatusPusat(dt.getStatusPusat());
            tKepatuhanBadanAll.setPeriodePembukuan(dt.getPeriodePembukuan());
            tKepatuhanBadanAll.setTahunPendirian(dt.getTahunPendirian());
            tKepatuhanBadanAll.setBadanHukum(dt.getBadanHukum());
            tKepatuhanBadanAll.setJumlah(dt.getJumlah());
            tKepatuhanBadanAll.setCreatedBy(dt.getCreatedBy());
            tKepatuhanBadanAll.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanBadanAll;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanBendaharaAll> createKepatuhanBendaharaAllList(List<KepatuhanDto> dto) {
        List<TKepatuhanBendaharaAll> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanBendaharaAll tKepatuhanBendaharaAll = new TKepatuhanBendaharaAll();
            tKepatuhanBendaharaAll.setId(dt.getId());
            tKepatuhanBendaharaAll.setTahun(dt.getTahun());
            tKepatuhanBendaharaAll.setKodeKpp(dt.getKodeKpp());
            tKepatuhanBendaharaAll.setNamaKpp(dt.getNamaKpp());
            tKepatuhanBendaharaAll.setKanwil(dt.getKanwil());
            tKepatuhanBendaharaAll.setProvinsi(dt.getProvinsi());
            tKepatuhanBendaharaAll.setPulau(dt.getPulau());
            tKepatuhanBendaharaAll.setJenis(dt.getJenis());
            tKepatuhanBendaharaAll.setJumlah(dt.getJumlah());
            tKepatuhanBendaharaAll.setCreatedBy(dt.getCreatedBy());
            tKepatuhanBendaharaAll.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanBendaharaAll;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanBendaharaStatus> createKepatuhanBendaharaStatusList(List<KepatuhanDto> dto) {
        List<TKepatuhanBendaharaStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanBendaharaStatus tKepatuhanBendaharaStatus = new TKepatuhanBendaharaStatus();
            tKepatuhanBendaharaStatus.setId(dt.getId());
            tKepatuhanBendaharaStatus.setTahun(dt.getTahun());
            tKepatuhanBendaharaStatus.setKodeKpp(dt.getKodeKpp());
            tKepatuhanBendaharaStatus.setNamaKpp(dt.getNamaKpp());
            tKepatuhanBendaharaStatus.setKanwil(dt.getKanwil());
            tKepatuhanBendaharaStatus.setProvinsi(dt.getProvinsi());
            tKepatuhanBendaharaStatus.setPulau(dt.getPulau());
            tKepatuhanBendaharaStatus.setJenis(dt.getJenis());
            tKepatuhanBendaharaStatus.setStatusNpwp(dt.getStatusNpwp());
            tKepatuhanBendaharaStatus.setStatusPkp(dt.getStatusPkp());
            tKepatuhanBendaharaStatus.setJumlah(dt.getJumlah());
            tKepatuhanBendaharaStatus.setCreatedBy(dt.getCreatedBy());
            tKepatuhanBendaharaStatus.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanBendaharaStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanUmumStatus> createKepatuhanUmumStatusList(List<KepatuhanDto> dto) {
        List<TKepatuhanUmumStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanUmumStatus tKepatuhanUmumStatus = new TKepatuhanUmumStatus();
            tKepatuhanUmumStatus.setId(dt.getId());
            tKepatuhanUmumStatus.setTahun(dt.getTahun());
            tKepatuhanUmumStatus.setKodeKpp(dt.getKodeKpp());
            tKepatuhanUmumStatus.setNamaKpp(dt.getNamaKpp());
            tKepatuhanUmumStatus.setKanwil(dt.getKanwil());
            tKepatuhanUmumStatus.setProvinsi(dt.getProvinsi());
            tKepatuhanUmumStatus.setPulau(dt.getPulau());
            tKepatuhanUmumStatus.setKodeKategori(dt.getKodeKategori());
            tKepatuhanUmumStatus.setNamaKategori(dt.getNamaKategori());
            tKepatuhanUmumStatus.setStatusNpwp(dt.getStatusNpwp());
            tKepatuhanUmumStatus.setStatusPkp(dt.getStatusPkp());
            tKepatuhanUmumStatus.setJenisWp(dt.getJenisWp());
            tKepatuhanUmumStatus.setJumlah(dt.getJumlah());
            tKepatuhanUmumStatus.setCreatedBy(dt.getCreatedBy());
            tKepatuhanUmumStatus.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanUmumStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TKepatuhanUmumWilayahKlu> createKepatuhanUmumWilayahKluList(List<KepatuhanDto> dto) {
        List<TKepatuhanUmumWilayahKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TKepatuhanUmumWilayahKlu tKepatuhanUmumWilayahKlu = new TKepatuhanUmumWilayahKlu();
            tKepatuhanUmumWilayahKlu.setId(dt.getId());
            tKepatuhanUmumWilayahKlu.setTahun(dt.getTahun());
            tKepatuhanUmumWilayahKlu.setKodeKpp(dt.getKodeKpp());
            tKepatuhanUmumWilayahKlu.setNamaKpp(dt.getNamaKpp());
            tKepatuhanUmumWilayahKlu.setKanwil(dt.getKanwil());
            tKepatuhanUmumWilayahKlu.setProvinsi(dt.getProvinsi());
            tKepatuhanUmumWilayahKlu.setPulau(dt.getPulau());
            tKepatuhanUmumWilayahKlu.setKodeKategori(dt.getKodeKategori());
            tKepatuhanUmumWilayahKlu.setNamaKategori(dt.getNamaKategori());
            tKepatuhanUmumWilayahKlu.setJenisWp(dt.getJenisWp());
            tKepatuhanUmumWilayahKlu.setJumlah(dt.getJumlah());
            tKepatuhanUmumWilayahKlu.setCreatedBy(dt.getCreatedBy());
            tKepatuhanUmumWilayahKlu.setCreatedDate(dt.getCreatedDate());
            return tKepatuhanUmumWilayahKlu;
        }).collect(Collectors.toList());
        return listModel;
    }
}