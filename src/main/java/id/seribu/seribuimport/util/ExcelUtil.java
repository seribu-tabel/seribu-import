package id.seribu.seribuimport.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import id.seribu.seribuimport.constant.HeaderEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * XSSF and XML Stream Reader
 * 
 * If memory footprint is an issue, then for XSSF, you can get at the underlying
 * XML data, and process it yourself. This is intended for intermediate
 * developers who are willing to learn a little bit of low level structure of
 * .xlsx files, and who are happy processing XML in java. Its relatively simple
 * to use, but requires a basic understanding of the file structure. The
 * advantage provided is that you can read a XLSX file with a relatively small
 * memory footprint.
 * 
 * @author lchen
 * 
 */
@Slf4j
public class ExcelUtil implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int rowNum = 1; // one index
    private OPCPackage opcPkg;
    private ReadOnlySharedStringsTable stringsTable;
    private XMLStreamReader xmlReader;
    private Map<Integer, String> rowHeaderMap = new HashMap<>();

    public ExcelUtil() {
    }

    public ExcelUtil(String excelPath) throws Exception {
        try {
            log.info("ExcelUtil file: {}", excelPath);
            opcPkg = OPCPackage.open(excelPath, PackageAccess.READ);
            this.stringsTable = new ReadOnlySharedStringsTable(opcPkg);
            XSSFReader xssfReader = new XSSFReader(opcPkg);
            XMLInputFactory factory = XMLInputFactory.newInstance();
            InputStream inputStream = xssfReader.getSheetsData().next();
            xmlReader = factory.createXMLStreamReader(inputStream);

            while (xmlReader.hasNext()) {
                xmlReader.next();
                if (xmlReader.isStartElement()) {
                    if (xmlReader.getLocalName().equals("sheetData"))
                        break;
                }
            }
        } catch (Exception e) {
            log.warn("File is not found path: {}", excelPath);
        }
    }

    public int rowNum() {
        return rowNum;
    }

    public List<Map<String, String>> readRows() throws XMLStreamException {
        log.info("ExcelUtil.readRows start ...");
        String elementName = "row";
        List<Map<String, String>> dataRows = new ArrayList<>();
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals(elementName)) {
                    if (rowNum > 1) {
                        dataRows.add(getDataRow());
                    } else {
                        // only invoked to define the header column
                        getDataRow();
                    }
                    rowNum++;
                }
            }
        }
        log.info("ExcelUtil.readRows finish {} rows", rowNum);
        return dataRows;
    }

    private Map<String, String> getDataRow() throws XMLStreamException {
        int rowCell = 1;
        Map<String, String> rowValueMap = new HashMap<>();
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals("c")) {
                    String cellType = xmlReader.getAttributeValue(null, "t");
                    // set header
                    if (rowNum == 1) {
                        rowHeaderMap.put(rowCell, getCellValue(cellType).toUpperCase().replace(" ", "_"));
                    } else {
                        rowValueMap.put(rowHeaderMap.get(rowCell), getCellValue(cellType));
                    }
                    // rowValues.add(getCellValue(cellType));
                    rowCell++;
                }
            } else if (xmlReader.isEndElement() && xmlReader.getLocalName().equals("row")) {
                break;
            }
        }
        return rowValueMap;
    }

    private String getCellValue(String cellType) throws XMLStreamException {
        String value = ""; // by default
        while (xmlReader.hasNext()) {
            xmlReader.next();
            if (xmlReader.isStartElement()) {
                if (xmlReader.getLocalName().equals("v")) {
                    if (cellType != null && cellType.equals("s")) {
                        int idx = Integer.parseInt(xmlReader.getElementText());
                        return stringsTable.getItemAt(idx).toString();
                        // return new XSSFRichTextString(stringsTable.getEntryAt(idx)).toString();
                    } else {
                        return xmlReader.getElementText();
                    }
                }
            } else if (xmlReader.isEndElement() && xmlReader.getLocalName().equals("c")) {
                break;
            }
        }
        return value;
    }

    @Override
    protected void finalize() throws Throwable {
        if (opcPkg != null)
            opcPkg.close();
        super.finalize();
    }

    public String formatDate(final Date date, final String pattern) {
        if (date != null) {
            SimpleDateFormat sdt = new SimpleDateFormat(pattern);
            return sdt.format(date);
        }
        return null;
    }

    public byte[] getExcelByte(XSSFWorkbook workbook) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();
        } catch (IOException e) {
            log.error("ExcelUtil.getExcelByte failed ", e);
        }
        return baos.toByteArray();
    }

    public void writeExcelFile(XSSFWorkbook workbook) throws IOException {
        log.info("ExcelUtil.writeExcelFile");
        try {
            final OutputStream os = new FileOutputStream(new File("/home/arieki/workspace/sample_pivot_2.xlsx"));
            workbook.write(os);
            workbook.close();
        } catch (IOException e) {
            log.error("ExcelUtil.writeExcelFile failed ", e);
            workbook.close();
        }
    }

    public String excelTitle(String dataset, String subyekData, String pivotData) {
        return "DATA " + dataset.toUpperCase() + " PAJAK " + subyekData.toUpperCase() + " BERDASARKAN "
                + HeaderEnum.valueOf(pivotData).getText().toUpperCase();
    }
}