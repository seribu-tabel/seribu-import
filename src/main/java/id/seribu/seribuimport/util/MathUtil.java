package id.seribu.seribuimport.util;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class MathUtil {

    public BigDecimal calcCombination(int n, int r) {
        return factorial(n).divide((factorial(r).multiply(factorial(n-r))));
    }

    public int calcPermutation(int n, int r) {
        return (factorial(n).divide(factorial(n-r)).intValue());
    }

    private BigDecimal factorial(int n) {
        BigDecimal fact = new BigDecimal(1);
        int i = 1;
        while (i <= n) {
            fact = fact.multiply(new BigDecimal(i));
            i++;
        }
        return fact;
    }
}