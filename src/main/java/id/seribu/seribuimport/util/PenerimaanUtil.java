package id.seribu.seribuimport.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import id.seribu.seribumodel.transaction.TPenerimaanBadanAll;
import id.seribu.seribumodel.transaction.TPenerimaanBadanStatus;
import id.seribu.seribumodel.transaction.TPenerimaanBendaharaAll;
import id.seribu.seribumodel.transaction.TPenerimaanBendaharaStatus;
import id.seribu.seribumodel.transaction.TPenerimaanOpKlu;
import id.seribu.seribumodel.transaction.TPenerimaanOpStatus;
import id.seribu.seribumodel.transaction.TPenerimaanOpWilayah;
import id.seribu.seribumodel.transaction.TPenerimaanOpWilayahKlu;
import id.seribu.seribumodel.transaction.TPenerimaanUmumStatus;
import id.seribu.seribumodel.transaction.TPenerimaanUmumWilayahKlu;

@Component
public class PenerimaanUtil {

    public List<TPenerimaanOpWilayah> createPenerimaanOpWilayahList (List<PenerimaanDto> dto) {
        List<TPenerimaanOpWilayah> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanOpWilayah tPenerimaanOpWilayah = new TPenerimaanOpWilayah();
            tPenerimaanOpWilayah.setId(dt.getId());
            tPenerimaanOpWilayah.setTahun(dt.getTahun());
            tPenerimaanOpWilayah.setKodeKpp(dt.getKodeKpp());
            tPenerimaanOpWilayah.setNamaKpp(dt.getNamaKpp());
            tPenerimaanOpWilayah.setKanwil(dt.getKanwil());
            tPenerimaanOpWilayah.setProvinsi(dt.getProvinsi());
            tPenerimaanOpWilayah.setPulau(dt.getPulau());
            tPenerimaanOpWilayah.setStatusPerkawinan(dt.getStatusPerkawinan());
            tPenerimaanOpWilayah.setJumlahTanggungan(dt.getJumlahTanggungan());
            tPenerimaanOpWilayah.setJenisOp(dt.getJenisOp());
            tPenerimaanOpWilayah.setRangeUsia(dt.getRangeUsia());
            tPenerimaanOpWilayah.setJenisKelamin(dt.getJenisKelamin());
            tPenerimaanOpWilayah.setWargaNegara(dt.getWargaNegara());
            tPenerimaanOpWilayah.setJumlah(dt.getJumlah());
            tPenerimaanOpWilayah.setCreatedBy(dt.getCreatedBy());
            tPenerimaanOpWilayah.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanOpWilayah;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanOpWilayahKlu> createPenerimaanOpWilayahKluList(List<PenerimaanDto> dto) {
        List<TPenerimaanOpWilayahKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanOpWilayahKlu tPenerimaanOpWilayahKlu = new TPenerimaanOpWilayahKlu();
            tPenerimaanOpWilayahKlu.setId(dt.getId());
            tPenerimaanOpWilayahKlu.setTahun(dt.getTahun());
            tPenerimaanOpWilayahKlu.setKodeKpp(dt.getKodeKpp());
            tPenerimaanOpWilayahKlu.setNamaKpp(dt.getNamaKpp());
            tPenerimaanOpWilayahKlu.setKanwil(dt.getKanwil());
            tPenerimaanOpWilayahKlu.setProvinsi(dt.getProvinsi());
            tPenerimaanOpWilayahKlu.setPulau(dt.getPulau());
            tPenerimaanOpWilayahKlu.setKodeKategori(dt.getKodeKategori());
            tPenerimaanOpWilayahKlu.setNamaKategori(dt.getNamaKategori());
            tPenerimaanOpWilayahKlu.setJumlah(dt.getJumlah());
            tPenerimaanOpWilayahKlu.setCreatedBy(dt.getCreatedBy());
            tPenerimaanOpWilayahKlu.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanOpWilayahKlu;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanOpKlu> createPenerimaanOpKluList(List<PenerimaanDto> dto) {
        List<TPenerimaanOpKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanOpKlu tPenerimaanOpKlu = new TPenerimaanOpKlu();
            tPenerimaanOpKlu.setId(dt.getId());
            tPenerimaanOpKlu.setTahun(dt.getTahun());
            tPenerimaanOpKlu.setKodeKategori(dt.getKodeKategori());
            tPenerimaanOpKlu.setNamaKategori(dt.getNamaKategori());
            tPenerimaanOpKlu.setStatusPerkawinan(dt.getStatusPerkawinan());
            tPenerimaanOpKlu.setJumlahTanggungan(dt.getJumlahTanggungan());
            tPenerimaanOpKlu.setJenisOp(dt.getJenisOp());
            tPenerimaanOpKlu.setRangeUsia(dt.getRangeUsia());
            tPenerimaanOpKlu.setJenisKelamin(dt.getJenisKelamin());
            tPenerimaanOpKlu.setWargaNegara(dt.getWargaNegara());
            tPenerimaanOpKlu.setJumlah(dt.getJumlah());
            tPenerimaanOpKlu.setCreatedBy(dt.getCreatedBy());
            tPenerimaanOpKlu.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanOpKlu;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanOpStatus> createPenerimaanOpStatusList(List<PenerimaanDto> dto) {
        List<TPenerimaanOpStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanOpStatus tPenerimaanOpStatus = new TPenerimaanOpStatus();
            tPenerimaanOpStatus.setId(dt.getId());
            tPenerimaanOpStatus.setTahun(dt.getTahun());
            tPenerimaanOpStatus.setKodeKpp(dt.getKodeKpp());
            tPenerimaanOpStatus.setNamaKpp(dt.getNamaKpp());
            tPenerimaanOpStatus.setKanwil(dt.getKanwil());
            tPenerimaanOpStatus.setProvinsi(dt.getProvinsi());
            tPenerimaanOpStatus.setPulau(dt.getPulau());
            tPenerimaanOpStatus.setKodeKategori(dt.getKodeKategori());
            tPenerimaanOpStatus.setNamaKategori(dt.getNamaKategori());
            tPenerimaanOpStatus.setStatusNpwp(dt.getStatusNpwp());
            tPenerimaanOpStatus.setStatusPkp(dt.getStatusPkp());
            tPenerimaanOpStatus.setMetodePembukuan(dt.getMetodePembukuan());
            tPenerimaanOpStatus.setJumlah(dt.getJumlah());
            tPenerimaanOpStatus.setCreatedBy(dt.getCreatedBy());
            tPenerimaanOpStatus.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanOpStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanBadanStatus> createPenerimaanBadanStatusList(List<PenerimaanDto> dto) {
        List<TPenerimaanBadanStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanBadanStatus tPenerimaanBadanStatus = new TPenerimaanBadanStatus();
            tPenerimaanBadanStatus.setId(dt.getId());
            tPenerimaanBadanStatus.setTahun(dt.getTahun());
            tPenerimaanBadanStatus.setKodeKpp(dt.getKodeKpp());
            tPenerimaanBadanStatus.setNamaKpp(dt.getNamaKpp());
            tPenerimaanBadanStatus.setKanwil(dt.getKanwil());
            tPenerimaanBadanStatus.setProvinsi(dt.getProvinsi());
            tPenerimaanBadanStatus.setPulau(dt.getPulau());
            tPenerimaanBadanStatus.setKodeKategori(dt.getKodeKategori());
            tPenerimaanBadanStatus.setNamaKategori(dt.getNamaKategori());
            tPenerimaanBadanStatus.setKategoriWp(dt.getKategoriWp());
            tPenerimaanBadanStatus.setModal(dt.getModal());
            tPenerimaanBadanStatus.setStatusPusat(dt.getStatusPusat());
            tPenerimaanBadanStatus.setPeriodePembukuan(dt.getPeriodePembukuan());
            tPenerimaanBadanStatus.setTahunPendirian(dt.getTahunPendirian());
            tPenerimaanBadanStatus.setBadanHukum(dt.getBadanHukum());
            tPenerimaanBadanStatus.setStatusNpwp(dt.getStatusNpwp());
            tPenerimaanBadanStatus.setStatusPkp(dt.getStatusPkp());
            tPenerimaanBadanStatus.setJumlah(dt.getJumlah());
            tPenerimaanBadanStatus.setCreatedBy(dt.getCreatedBy());
            tPenerimaanBadanStatus.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanBadanStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanBadanAll> createPenerimaanBadanAllList(List<PenerimaanDto> dto) {
        List<TPenerimaanBadanAll> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanBadanAll tPenerimaanBadanAll = new TPenerimaanBadanAll();
            tPenerimaanBadanAll.setId(dt.getId());
            tPenerimaanBadanAll.setTahun(dt.getTahun());
            tPenerimaanBadanAll.setKodeKpp(dt.getKodeKpp());
            tPenerimaanBadanAll.setNamaKpp(dt.getNamaKpp());
            tPenerimaanBadanAll.setKanwil(dt.getKanwil());
            tPenerimaanBadanAll.setProvinsi(dt.getProvinsi());
            tPenerimaanBadanAll.setPulau(dt.getPulau());
            tPenerimaanBadanAll.setKodeKategori(dt.getKodeKategori());
            tPenerimaanBadanAll.setNamaKategori(dt.getNamaKategori());
            tPenerimaanBadanAll.setKategoriWp(dt.getKategoriWp());
            tPenerimaanBadanAll.setModal(dt.getModal());
            tPenerimaanBadanAll.setStatusPusat(dt.getStatusPusat());
            tPenerimaanBadanAll.setPeriodePembukuan(dt.getPeriodePembukuan());
            tPenerimaanBadanAll.setTahunPendirian(dt.getTahunPendirian());
            tPenerimaanBadanAll.setBadanHukum(dt.getBadanHukum());
            tPenerimaanBadanAll.setJumlah(dt.getJumlah());
            tPenerimaanBadanAll.setCreatedBy(dt.getCreatedBy());
            tPenerimaanBadanAll.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanBadanAll;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanBendaharaAll> createPenerimaanBendaharaAllList(List<PenerimaanDto> dto) {
        List<TPenerimaanBendaharaAll> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanBendaharaAll tPenerimaanBendaharaAll = new TPenerimaanBendaharaAll();
            tPenerimaanBendaharaAll.setId(dt.getId());
            tPenerimaanBendaharaAll.setTahun(dt.getTahun());
            tPenerimaanBendaharaAll.setKodeKpp(dt.getKodeKpp());
            tPenerimaanBendaharaAll.setNamaKpp(dt.getNamaKpp());
            tPenerimaanBendaharaAll.setKanwil(dt.getKanwil());
            tPenerimaanBendaharaAll.setProvinsi(dt.getProvinsi());
            tPenerimaanBendaharaAll.setPulau(dt.getPulau());
            tPenerimaanBendaharaAll.setJenis(dt.getJenis());
            tPenerimaanBendaharaAll.setJumlah(dt.getJumlah());
            tPenerimaanBendaharaAll.setCreatedBy(dt.getCreatedBy());
            tPenerimaanBendaharaAll.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanBendaharaAll;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanBendaharaStatus> createPenerimaanBendaharaStatusList(List<PenerimaanDto> dto) {
        List<TPenerimaanBendaharaStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanBendaharaStatus tPenerimaanBendaharaStatus = new TPenerimaanBendaharaStatus();
            tPenerimaanBendaharaStatus.setId(dt.getId());
            tPenerimaanBendaharaStatus.setTahun(dt.getTahun());
            tPenerimaanBendaharaStatus.setKodeKpp(dt.getKodeKpp());
            tPenerimaanBendaharaStatus.setNamaKpp(dt.getNamaKpp());
            tPenerimaanBendaharaStatus.setKanwil(dt.getKanwil());
            tPenerimaanBendaharaStatus.setProvinsi(dt.getProvinsi());
            tPenerimaanBendaharaStatus.setPulau(dt.getPulau());
            tPenerimaanBendaharaStatus.setJenis(dt.getJenis());
            tPenerimaanBendaharaStatus.setStatusNpwp(dt.getStatusNpwp());
            tPenerimaanBendaharaStatus.setStatusPkp(dt.getStatusPkp());
            tPenerimaanBendaharaStatus.setJumlah(dt.getJumlah());
            tPenerimaanBendaharaStatus.setCreatedBy(dt.getCreatedBy());
            tPenerimaanBendaharaStatus.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanBendaharaStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanUmumStatus> createPenerimaanUmumStatusList(List<PenerimaanDto> dto) {
        List<TPenerimaanUmumStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanUmumStatus tPenerimaanUmumStatus = new TPenerimaanUmumStatus();
            tPenerimaanUmumStatus.setId(dt.getId());
            tPenerimaanUmumStatus.setTahun(dt.getTahun());
            tPenerimaanUmumStatus.setKodeKpp(dt.getKodeKpp());
            tPenerimaanUmumStatus.setNamaKpp(dt.getNamaKpp());
            tPenerimaanUmumStatus.setKanwil(dt.getKanwil());
            tPenerimaanUmumStatus.setProvinsi(dt.getProvinsi());
            tPenerimaanUmumStatus.setPulau(dt.getPulau());
            tPenerimaanUmumStatus.setKodeKategori(dt.getKodeKategori());
            tPenerimaanUmumStatus.setNamaKategori(dt.getNamaKategori());
            tPenerimaanUmumStatus.setStatusNpwp(dt.getStatusNpwp());
            tPenerimaanUmumStatus.setStatusPkp(dt.getStatusPkp());
            tPenerimaanUmumStatus.setJenisWp(dt.getJenisWp());
            tPenerimaanUmumStatus.setJumlah(dt.getJumlah());
            tPenerimaanUmumStatus.setCreatedBy(dt.getCreatedBy());
            tPenerimaanUmumStatus.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanUmumStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TPenerimaanUmumWilayahKlu> createPenerimaanUmumWilayahKluList(List<PenerimaanDto> dto) {
        List<TPenerimaanUmumWilayahKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TPenerimaanUmumWilayahKlu tPenerimaanUmumWilayahKlu = new TPenerimaanUmumWilayahKlu();
            tPenerimaanUmumWilayahKlu.setId(dt.getId());
            tPenerimaanUmumWilayahKlu.setTahun(dt.getTahun());
            tPenerimaanUmumWilayahKlu.setKodeKpp(dt.getKodeKpp());
            tPenerimaanUmumWilayahKlu.setNamaKpp(dt.getNamaKpp());
            tPenerimaanUmumWilayahKlu.setKanwil(dt.getKanwil());
            tPenerimaanUmumWilayahKlu.setProvinsi(dt.getProvinsi());
            tPenerimaanUmumWilayahKlu.setPulau(dt.getPulau());
            tPenerimaanUmumWilayahKlu.setKodeKategori(dt.getKodeKategori());
            tPenerimaanUmumWilayahKlu.setNamaKategori(dt.getNamaKategori());
            tPenerimaanUmumWilayahKlu.setJenisWp(dt.getJenisWp());
            tPenerimaanUmumWilayahKlu.setJumlah(dt.getJumlah());
            tPenerimaanUmumWilayahKlu.setCreatedBy(dt.getCreatedBy());
            tPenerimaanUmumWilayahKlu.setCreatedDate(dt.getCreatedDate());
            return tPenerimaanUmumWilayahKlu;
        }).collect(Collectors.toList());
        return listModel;
    }
}