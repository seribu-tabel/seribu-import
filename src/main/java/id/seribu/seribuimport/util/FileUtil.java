package id.seribu.seribuimport.util;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.GZIPOutputStream;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import id.seribu.seribuimport.constant.ImportConstant;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class FileUtil {

    public byte[] downloadFileAsByte(final String path) {
        byte[] byteFile = null;
        try {
            byteFile = Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            log.error("FileUtil.readAllBytes error path: {}", path, e);
        }
        return byteFile;
    }

    public boolean backupFile(final String sourcePath, final String backupPath, String filename) {
        log.info("FileUtil.backupFile sourcePath: {}, filename: {}", sourcePath, filename);
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
                GZIPOutputStream gzOut = new GZIPOutputStream(baos)) {
            gzOut.write(this.downloadFileAsByte(sourcePath + ImportConstant.PATH_PREFIX + filename));
            gzOut.close();
            Files.createDirectories(Paths.get(backupPath));
            FileOutputStream fileStream = new FileOutputStream(
                    backupPath + ImportConstant.PATH_PREFIX + filename + ".gz");
            fileStream.write(baos.toByteArray());
            fileStream.close();
            Files.deleteIfExists(Paths.get(sourcePath + ImportConstant.PATH_PREFIX + filename));
            return true;
        } catch (IOException e) {
            log.error("FileUtil.backupFile error", e);
            return false;
        }
    }

    public Resource getFilResource(byte[] byteArray, final String filename) {
        Resource resource = null;
        try{
            resource = new ByteArrayResource(byteArray) {
                @Override
                public String getFilename() {
                    return filename;
                }
            };  
            return resource;
        } catch (Exception e) {
            log.error("FileUtil.getFilResource failed", e);
            return null;
        }
    }

}