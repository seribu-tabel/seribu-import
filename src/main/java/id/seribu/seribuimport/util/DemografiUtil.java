package id.seribu.seribuimport.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import id.seribu.seribudto.demografi.DemografiDto;
import id.seribu.seribumodel.transaction.TDemoBadanAll;
import id.seribu.seribumodel.transaction.TDemoBadanStatus;
import id.seribu.seribumodel.transaction.TDemoBendaharaAll;
import id.seribu.seribumodel.transaction.TDemoBendaharaStatus;
import id.seribu.seribumodel.transaction.TDemoOpKlu;
import id.seribu.seribumodel.transaction.TDemoOpStatus;
import id.seribu.seribumodel.transaction.TDemoOpWilayah;
import id.seribu.seribumodel.transaction.TDemoOpWilayahKlu;
import id.seribu.seribumodel.transaction.TDemoUmumStatus;
import id.seribu.seribumodel.transaction.TDemoUmumWilayahKlu;

@Component
public class DemografiUtil {

    public List<TDemoOpWilayah> createDemoOpWilayahList (List<DemografiDto> dto) {
        List<TDemoOpWilayah> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoOpWilayah tDemoOpWilayah = new TDemoOpWilayah();
            tDemoOpWilayah.setId(dt.getId());
            tDemoOpWilayah.setTahun(dt.getTahun());
            tDemoOpWilayah.setKodeKpp(dt.getKodeKpp());
            tDemoOpWilayah.setNamaKpp(dt.getNamaKpp());
            tDemoOpWilayah.setKanwil(dt.getKanwil());
            tDemoOpWilayah.setProvinsi(dt.getProvinsi());
            tDemoOpWilayah.setPulau(dt.getPulau());
            tDemoOpWilayah.setStatusPerkawinan(dt.getStatusPerkawinan());
            tDemoOpWilayah.setJumlahTanggungan(dt.getJumlahTanggungan());
            tDemoOpWilayah.setJenisOp(dt.getJenisOp());
            tDemoOpWilayah.setRangeUsia(dt.getRangeUsia());
            tDemoOpWilayah.setJenisKelamin(dt.getJenisKelamin());
            tDemoOpWilayah.setWargaNegara(dt.getWargaNegara());
            tDemoOpWilayah.setJumlah(dt.getJumlah());
            tDemoOpWilayah.setCreatedBy(dt.getCreatedBy());
            tDemoOpWilayah.setCreatedDate(dt.getCreatedDate());
            return tDemoOpWilayah;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoOpWilayahKlu> createDemoOpWilayahKluList(List<DemografiDto> dto) {
        List<TDemoOpWilayahKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoOpWilayahKlu tDemoOpWilayahKlu = new TDemoOpWilayahKlu();
            tDemoOpWilayahKlu.setId(dt.getId());
            tDemoOpWilayahKlu.setTahun(dt.getTahun());
            tDemoOpWilayahKlu.setKodeKpp(dt.getKodeKpp());
            tDemoOpWilayahKlu.setNamaKpp(dt.getNamaKpp());
            tDemoOpWilayahKlu.setKanwil(dt.getKanwil());
            tDemoOpWilayahKlu.setProvinsi(dt.getProvinsi());
            tDemoOpWilayahKlu.setPulau(dt.getPulau());
            tDemoOpWilayahKlu.setKodeKategori(dt.getKodeKategori());
            tDemoOpWilayahKlu.setNamaKategori(dt.getNamaKategori());
            tDemoOpWilayahKlu.setJumlah(dt.getJumlah());
            tDemoOpWilayahKlu.setCreatedBy(dt.getCreatedBy());
            tDemoOpWilayahKlu.setCreatedDate(dt.getCreatedDate());
            return tDemoOpWilayahKlu;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoOpKlu> createDemoOpKluList(List<DemografiDto> dto) {
        List<TDemoOpKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoOpKlu tDemoOpKlu = new TDemoOpKlu();
            tDemoOpKlu.setId(dt.getId());
            tDemoOpKlu.setTahun(dt.getTahun());
            tDemoOpKlu.setKodeKategori(dt.getKodeKategori());
            tDemoOpKlu.setNamaKategori(dt.getNamaKategori());
            tDemoOpKlu.setStatusPerkawinan(dt.getStatusPerkawinan());
            tDemoOpKlu.setJumlahTanggungan(dt.getJumlahTanggungan());
            tDemoOpKlu.setJenisOp(dt.getJenisOp());
            tDemoOpKlu.setRangeUsia(dt.getRangeUsia());
            tDemoOpKlu.setJenisKelamin(dt.getJenisKelamin());
            tDemoOpKlu.setWargaNegara(dt.getWargaNegara());
            tDemoOpKlu.setJumlah(dt.getJumlah());
            tDemoOpKlu.setCreatedBy(dt.getCreatedBy());
            tDemoOpKlu.setCreatedDate(dt.getCreatedDate());
            return tDemoOpKlu;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoOpStatus> createDemoOpStatusList(List<DemografiDto> dto) {
        List<TDemoOpStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoOpStatus tDemoOpStatus = new TDemoOpStatus();
            tDemoOpStatus.setId(dt.getId());
            tDemoOpStatus.setTahun(dt.getTahun());
            tDemoOpStatus.setKodeKpp(dt.getKodeKpp());
            tDemoOpStatus.setNamaKpp(dt.getNamaKpp());
            tDemoOpStatus.setKanwil(dt.getKanwil());
            tDemoOpStatus.setProvinsi(dt.getProvinsi());
            tDemoOpStatus.setPulau(dt.getPulau());
            tDemoOpStatus.setKodeKategori(dt.getKodeKategori());
            tDemoOpStatus.setNamaKategori(dt.getNamaKategori());
            tDemoOpStatus.setStatusNpwp(dt.getStatusNpwp());
            tDemoOpStatus.setStatusPkp(dt.getStatusPkp());
            tDemoOpStatus.setMetodePembukuan(dt.getMetodePembukuan());
            tDemoOpStatus.setJumlah(dt.getJumlah());
            tDemoOpStatus.setCreatedBy(dt.getCreatedBy());
            tDemoOpStatus.setCreatedDate(dt.getCreatedDate());
            return tDemoOpStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoBadanStatus> createDemoBadanStatusList(List<DemografiDto> dto) {
        List<TDemoBadanStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoBadanStatus tDemoBadanStatus = new TDemoBadanStatus();
            tDemoBadanStatus.setId(dt.getId());
            tDemoBadanStatus.setTahun(dt.getTahun());
            tDemoBadanStatus.setKodeKpp(dt.getKodeKpp());
            tDemoBadanStatus.setNamaKpp(dt.getNamaKpp());
            tDemoBadanStatus.setKanwil(dt.getKanwil());
            tDemoBadanStatus.setProvinsi(dt.getProvinsi());
            tDemoBadanStatus.setPulau(dt.getPulau());
            tDemoBadanStatus.setKodeKategori(dt.getKodeKategori());
            tDemoBadanStatus.setNamaKategori(dt.getNamaKategori());
            tDemoBadanStatus.setKategoriWp(dt.getKategoriWp());
            tDemoBadanStatus.setModal(dt.getModal());
            tDemoBadanStatus.setStatusPusat(dt.getStatusPusat());
            tDemoBadanStatus.setPeriodePembukuan(dt.getPeriodePembukuan());
            tDemoBadanStatus.setTahunPendirian(dt.getTahunPendirian());
            tDemoBadanStatus.setBadanHukum(dt.getBadanHukum());
            tDemoBadanStatus.setStatusNpwp(dt.getStatusNpwp());
            tDemoBadanStatus.setStatusPkp(dt.getStatusPkp());
            tDemoBadanStatus.setJumlah(dt.getJumlah());
            tDemoBadanStatus.setCreatedBy(dt.getCreatedBy());
            tDemoBadanStatus.setCreatedDate(dt.getCreatedDate());
            return tDemoBadanStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoBadanAll> createDemoBadanAllList(List<DemografiDto> dto) {
        List<TDemoBadanAll> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoBadanAll tDemoBadanAll = new TDemoBadanAll();
            tDemoBadanAll.setId(dt.getId());
            tDemoBadanAll.setTahun(dt.getTahun());
            tDemoBadanAll.setKodeKpp(dt.getKodeKpp());
            tDemoBadanAll.setNamaKpp(dt.getNamaKpp());
            tDemoBadanAll.setKanwil(dt.getKanwil());
            tDemoBadanAll.setProvinsi(dt.getProvinsi());
            tDemoBadanAll.setPulau(dt.getPulau());
            tDemoBadanAll.setKodeKategori(dt.getKodeKategori());
            tDemoBadanAll.setNamaKategori(dt.getNamaKategori());
            tDemoBadanAll.setKategoriWp(dt.getKategoriWp());
            tDemoBadanAll.setModal(dt.getModal());
            tDemoBadanAll.setStatusPusat(dt.getStatusPusat());
            tDemoBadanAll.setPeriodePembukuan(dt.getPeriodePembukuan());
            tDemoBadanAll.setTahunPendirian(dt.getTahunPendirian());
            tDemoBadanAll.setBadanHukum(dt.getBadanHukum());
            tDemoBadanAll.setJumlah(dt.getJumlah());
            tDemoBadanAll.setCreatedBy(dt.getCreatedBy());
            tDemoBadanAll.setCreatedDate(dt.getCreatedDate());
            return tDemoBadanAll;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoBendaharaAll> createDemoBendaharaAllList(List<DemografiDto> dto) {
        List<TDemoBendaharaAll> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoBendaharaAll tDemoBendaharaAll = new TDemoBendaharaAll();
            tDemoBendaharaAll.setId(dt.getId());
            tDemoBendaharaAll.setTahun(dt.getTahun());
            tDemoBendaharaAll.setKodeKpp(dt.getKodeKpp());
            tDemoBendaharaAll.setNamaKpp(dt.getNamaKpp());
            tDemoBendaharaAll.setKanwil(dt.getKanwil());
            tDemoBendaharaAll.setProvinsi(dt.getProvinsi());
            tDemoBendaharaAll.setPulau(dt.getPulau());
            tDemoBendaharaAll.setJenis(dt.getJenis());
            tDemoBendaharaAll.setJumlah(dt.getJumlah());
            tDemoBendaharaAll.setCreatedBy(dt.getCreatedBy());
            tDemoBendaharaAll.setCreatedDate(dt.getCreatedDate());
            return tDemoBendaharaAll;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoBendaharaStatus> createDemoBendaharaStatusList(List<DemografiDto> dto) {
        List<TDemoBendaharaStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoBendaharaStatus tDemoBendaharaStatus = new TDemoBendaharaStatus();
            tDemoBendaharaStatus.setId(dt.getId());
            tDemoBendaharaStatus.setTahun(dt.getTahun());
            tDemoBendaharaStatus.setKodeKpp(dt.getKodeKpp());
            tDemoBendaharaStatus.setNamaKpp(dt.getNamaKpp());
            tDemoBendaharaStatus.setKanwil(dt.getKanwil());
            tDemoBendaharaStatus.setProvinsi(dt.getProvinsi());
            tDemoBendaharaStatus.setPulau(dt.getPulau());
            tDemoBendaharaStatus.setJenis(dt.getJenis());
            tDemoBendaharaStatus.setStatusNpwp(dt.getStatusNpwp());
            tDemoBendaharaStatus.setStatusPkp(dt.getStatusPkp());
            tDemoBendaharaStatus.setJumlah(dt.getJumlah());
            tDemoBendaharaStatus.setCreatedBy(dt.getCreatedBy());
            tDemoBendaharaStatus.setCreatedDate(dt.getCreatedDate());
            return tDemoBendaharaStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoUmumStatus> createDemoUmumStatusList(List<DemografiDto> dto) {
        List<TDemoUmumStatus> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoUmumStatus tDemoUmumStatus = new TDemoUmumStatus();
            tDemoUmumStatus.setId(dt.getId());
            tDemoUmumStatus.setTahun(dt.getTahun());
            tDemoUmumStatus.setKodeKpp(dt.getKodeKpp());
            tDemoUmumStatus.setNamaKpp(dt.getNamaKpp());
            tDemoUmumStatus.setKanwil(dt.getKanwil());
            tDemoUmumStatus.setProvinsi(dt.getProvinsi());
            tDemoUmumStatus.setPulau(dt.getPulau());
            tDemoUmumStatus.setKodeKategori(dt.getKodeKategori());
            tDemoUmumStatus.setNamaKategori(dt.getNamaKategori());
            tDemoUmumStatus.setStatusNpwp(dt.getStatusNpwp());
            tDemoUmumStatus.setStatusPkp(dt.getStatusPkp());
            tDemoUmumStatus.setJenisWp(dt.getJenisWp());
            tDemoUmumStatus.setJumlah(dt.getJumlah());
            tDemoUmumStatus.setCreatedBy(dt.getCreatedBy());
            tDemoUmumStatus.setCreatedDate(dt.getCreatedDate());
            return tDemoUmumStatus;
        }).collect(Collectors.toList());
        return listModel;
    }

    public List<TDemoUmumWilayahKlu> createDemoUmumWilayahKluList(List<DemografiDto> dto) {
        List<TDemoUmumWilayahKlu> listModel = new ArrayList<>();
        listModel = dto.stream().map(dt -> {
            TDemoUmumWilayahKlu tDemoUmumWilayahKlu = new TDemoUmumWilayahKlu();
            tDemoUmumWilayahKlu.setId(dt.getId());
            tDemoUmumWilayahKlu.setTahun(dt.getTahun());
            tDemoUmumWilayahKlu.setKodeKpp(dt.getKodeKpp());
            tDemoUmumWilayahKlu.setNamaKpp(dt.getNamaKpp());
            tDemoUmumWilayahKlu.setKanwil(dt.getKanwil());
            tDemoUmumWilayahKlu.setProvinsi(dt.getProvinsi());
            tDemoUmumWilayahKlu.setPulau(dt.getPulau());
            tDemoUmumWilayahKlu.setKodeKategori(dt.getKodeKategori());
            tDemoUmumWilayahKlu.setNamaKategori(dt.getNamaKategori());
            tDemoUmumWilayahKlu.setJenisWp(dt.getJenisWp());
            tDemoUmumWilayahKlu.setJumlah(dt.getJumlah());
            tDemoUmumWilayahKlu.setCreatedBy(dt.getCreatedBy());
            tDemoUmumWilayahKlu.setCreatedDate(dt.getCreatedDate());
            return tDemoUmumWilayahKlu;
        }).collect(Collectors.toList());
        return listModel;
    }
}