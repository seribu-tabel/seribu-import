package id.seribu.seribuimport.util;

import java.util.UUID;

import org.springframework.stereotype.Component;

import id.seribu.seribudto.transaction.ImportDto;
import id.seribu.seribumodel.transaction.TImportFile;

@Component
public class ModelUtil {

    public TImportFile createImportFile(ImportDto importDto) {
        TImportFile importFile = new TImportFile();
        importFile.setId(UUID.randomUUID());
        importFile.setDataset(importDto.getDataset());
        importFile.setSubyekData(importDto.getSubyekData());
        importFile.setSourceData(importDto.getSourceData());
        importFile.setDirectory(importDto.getDirectory());
        importFile.setFilename(importDto.getFilename());
        importFile.setProcess(false);
        importFile.setUploadedBy(importDto.getUploadedBy());
        importFile.setUploadDate(importDto.getUploadedDate());
        return importFile;
    }
}