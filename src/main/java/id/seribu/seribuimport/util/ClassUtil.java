package id.seribu.seribuimport.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import id.seribu.seribudto.transaction.PivotDataDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClassUtil {
    private Set<String> getAllFields(final Class<?> type) {
        Set<String> fields = new HashSet<>();
        // loop the fields using Java Reflections
        for (Field field : type.getDeclaredFields()) {
            fields.add(field.getName());
        }

        // recursive call to getAllFields
        if (type.getSuperclass() != null) {
            fields.addAll(getAllFields(type.getSuperclass()));
        }
        return fields;
    }

    public <T> List<PivotDataDto> convertModelToExcelPivot(List<T> t) {
        log.info("ClassUtil.convertModelToExcelPivot");
        List<PivotDataDto> listExcelData = new ArrayList<>();
        listExcelData = t.stream().map(dt -> {
            PivotDataDto pivotDataTest = new PivotDataDto();
            try {
                Set<String> fields = new HashSet<>();
                fields = getAllFields(dt.getClass());
                Field fieldTahun = fields.contains("tahun") ? dt.getClass().getDeclaredField("tahun") : null;
                Field fieldKodeKpp = fields.contains("kodeKpp") ? dt.getClass().getDeclaredField("kodeKpp") : null;
                Field fieldNamaKpp = fields.contains("namaKpp") ? dt.getClass().getDeclaredField("namaKpp") : null;
                Field fieldKanwil = fields.contains("kanwil") ? dt.getClass().getDeclaredField("kanwil") : null;
                Field fieldProvinsi = fields.contains("provinsi") ? dt.getClass().getDeclaredField("provinsi") : null;
                Field fieldPulau = fields.contains("pulau") ? dt.getClass().getDeclaredField("pulau") : null;
                Field fieldKodeKategori = fields.contains("kodeKategori")
                        ? dt.getClass().getDeclaredField("kodeKategori")
                        : null;
                Field fieldNamaKategori = fields.contains("namaKategori")
                        ? dt.getClass().getDeclaredField("namaKategori")
                        : null;
                Field fieldStatusPerkawinan = fields.contains("statusPerkawinan")
                        ? dt.getClass().getDeclaredField("statusPerkawinan")
                        : null;
                Field fieldJumlahTanggungan = fields.contains("jumlahTanggungan")
                        ? dt.getClass().getDeclaredField("jumlahTanggungan")
                        : null;
                Field fieldJenisOp = fields.contains("jenisOp") ? dt.getClass().getDeclaredField("jenisOp") : null;
                Field fieldRangeUsia = fields.contains("rangeUsia") ? dt.getClass().getDeclaredField("rangeUsia")
                        : null;
                Field fieldJenisKelamin = fields.contains("jenisKelamin")
                        ? dt.getClass().getDeclaredField("jenisKelamin")
                        : null;
                Field fieldWargaNegara = fields.contains("wargaNegara") ? dt.getClass().getDeclaredField("wargaNegara")
                        : null;
                Field fieldKategoriWp = fields.contains("kategoriWp") ? dt.getClass().getDeclaredField("kategoriWp")
                        : null;
                Field fieldJenisWp = fields.contains("jenisWp") ? dt.getClass().getDeclaredField("jenisWp") : null;
                Field fieldModal = fields.contains("modal") ? dt.getClass().getDeclaredField("modal") : null;
                Field fieldStatusPusat = fields.contains("statusPusat") ? dt.getClass().getDeclaredField("statusPusat")
                        : null;
                Field fieldPeriodePembukuan = fields.contains("periodePembukuan")
                        ? dt.getClass().getDeclaredField("periodePembukuan")
                        : null;
                Field fieldTahunPendirian = fields.contains("tahunPendirian")
                        ? dt.getClass().getDeclaredField("tahunPendirian")
                        : null;
                Field fieldBadanHukum = fields.contains("badanHukum") ? dt.getClass().getDeclaredField("badanHukum")
                        : null;
                Field fieldJenis = fields.contains("jenis") ? dt.getClass().getDeclaredField("jenis") : null;
                Field fieldStatusNpwp = fields.contains("statusNpwp") ? dt.getClass().getDeclaredField("statusNpwp")
                        : null;
                Field fieldStatusPkp = fields.contains("statusPkp") ? dt.getClass().getDeclaredField("statusPkp")
                        : null;
                Field fieldMetodePembukuan = fields.contains("metodePembukuan")
                        ? dt.getClass().getDeclaredField("metodePembukuan")
                        : null;
                Field fieldJumlah = fields.contains("jumlah") ? dt.getClass().getDeclaredField("jumlah") : null;
                if (fieldTahun != null) {
                    fieldTahun.setAccessible(true);
                    pivotDataTest.setTahun(fieldTahun.get(dt) == null ? null : fieldTahun.get(dt).toString());
                }
                if (fieldKodeKpp != null) {
                    fieldKodeKpp.setAccessible(true);
                    pivotDataTest.setKodeKpp(fieldKodeKpp.get(dt) == null ? null : fieldKodeKpp.get(dt).toString());
                }
                if (fieldNamaKpp != null) {
                    fieldNamaKpp.setAccessible(true);
                    pivotDataTest.setNamaKpp(fieldNamaKpp.get(dt) == null ? null : fieldNamaKpp.get(dt).toString());
                }
                if (fieldKanwil != null) {
                    fieldKanwil.setAccessible(true);
                    pivotDataTest.setKanwil(fieldKanwil.get(dt) == null ? null : fieldKanwil.get(dt).toString());
                }
                if (fieldProvinsi != null) {
                    fieldProvinsi.setAccessible(true);
                    pivotDataTest.setProvinsi(fieldProvinsi.get(dt) == null ? null : fieldProvinsi.get(dt).toString());
                }
                if (fieldPulau != null) {
                    fieldPulau.setAccessible(true);
                    pivotDataTest.setPulau(fieldPulau.get(dt) == null ? null : fieldPulau.get(dt).toString());
                }
                if (fieldKodeKategori != null) {
                    fieldKodeKategori.setAccessible(true);
                    pivotDataTest.setKodeKategori(
                            fieldKodeKategori.get(dt) == null ? null : fieldKodeKategori.get(dt).toString());
                }
                if (fieldNamaKategori != null) {
                    fieldNamaKategori.setAccessible(true);
                    pivotDataTest.setNamaKategori(
                            fieldNamaKategori.get(dt) == null ? null : fieldNamaKategori.get(dt).toString());
                }
                if (fieldStatusPerkawinan != null) {
                    fieldStatusPerkawinan.setAccessible(true);
                    pivotDataTest.setStatusPerkawinan(
                            fieldStatusPerkawinan.get(dt) == null ? null : fieldStatusPerkawinan.get(dt).toString());
                }
                if (fieldJumlahTanggungan != null) {
                    fieldJumlahTanggungan.setAccessible(true);
                    pivotDataTest.setJumlahTanggungan(
                            fieldJumlahTanggungan.get(dt) == null ? null : fieldJumlahTanggungan.get(dt).toString());
                }
                if (fieldJenisOp != null) {
                    fieldJenisOp.setAccessible(true);
                    pivotDataTest.setJenisOp(fieldJenisOp.get(dt) == null ? null : fieldJenisOp.get(dt).toString());
                }
                if (fieldRangeUsia != null) {
                    fieldRangeUsia.setAccessible(true);
                    pivotDataTest
                            .setRangeUsia(fieldRangeUsia.get(dt) == null ? null : fieldRangeUsia.get(dt).toString());
                }
                if (fieldJenisKelamin != null) {
                    fieldJenisKelamin.setAccessible(true);
                    pivotDataTest.setJenisKelamin(
                            fieldJenisKelamin.get(dt) == null ? null : fieldJenisKelamin.get(dt).toString());
                }
                if (fieldWargaNegara != null) {
                    fieldWargaNegara.setAccessible(true);
                    pivotDataTest.setWargaNegara(
                            fieldWargaNegara.get(dt) == null ? null : fieldWargaNegara.get(dt).toString());
                }
                if (fieldKategoriWp != null) {
                    fieldKategoriWp.setAccessible(true);
                    pivotDataTest
                            .setKategoriWp(fieldKategoriWp.get(dt) == null ? null : fieldKategoriWp.get(dt).toString());
                }
                if (fieldJenisWp != null) {
                    fieldJenisWp.setAccessible(true);
                    pivotDataTest.setJenisWp(fieldJenisWp.get(dt) == null ? null : fieldJenisWp.get(dt).toString());
                }
                if (fieldModal != null) {
                    fieldModal.setAccessible(true);
                    pivotDataTest.setModal(fieldModal.get(dt) == null ? null : fieldModal.get(dt).toString());
                }
                if (fieldStatusPusat != null) {
                    fieldStatusPusat.setAccessible(true);
                    pivotDataTest.setStatusPusat(
                            fieldStatusPusat.get(dt) == null ? null : fieldStatusPusat.get(dt).toString());
                }
                if (fieldPeriodePembukuan != null) {
                    fieldPeriodePembukuan.setAccessible(true);
                    pivotDataTest.setPeriodePembukuan(
                            fieldPeriodePembukuan.get(dt) == null ? null : fieldPeriodePembukuan.get(dt).toString());
                }
                if (fieldTahunPendirian != null) {
                    fieldTahunPendirian.setAccessible(true);
                    pivotDataTest.setTahunPendirian(
                            fieldTahunPendirian.get(dt) == null ? null : fieldTahunPendirian.get(dt).toString());
                }
                if (fieldBadanHukum != null) {
                    fieldBadanHukum.setAccessible(true);
                    pivotDataTest
                            .setBadanHukum(fieldBadanHukum.get(dt) == null ? null : fieldBadanHukum.get(dt).toString());
                }
                if (fieldJenis != null) {
                    fieldJenis.setAccessible(true);
                    pivotDataTest.setJenis(fieldJenis.get(dt) == null ? null : fieldJenis.get(dt).toString());
                }
                if (fieldStatusNpwp != null) {
                    fieldStatusNpwp.setAccessible(true);
                    pivotDataTest
                            .setStatusNpwp(fieldStatusNpwp.get(dt) == null ? null : fieldStatusNpwp.get(dt).toString());
                }
                if (fieldStatusPkp != null) {
                    fieldStatusPkp.setAccessible(true);
                    pivotDataTest
                            .setStatusPkp(fieldStatusPkp.get(dt) == null ? null : fieldStatusPkp.get(dt).toString());
                }
                if (fieldMetodePembukuan != null) {
                    fieldMetodePembukuan.setAccessible(true);
                    pivotDataTest.setMetodePembukuan(
                            fieldMetodePembukuan.get(dt) == null ? null : fieldMetodePembukuan.get(dt).toString());
                }
                if (fieldJumlah != null) {
                    fieldJumlah.setAccessible(true);
                    pivotDataTest.setJumlah(fieldJumlah.getInt(dt));
                }
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                log.error("ClassUtil.convertModelToExcelPivot Error", e);
            }
            return pivotDataTest;
        }).collect(Collectors.toList());
        return listExcelData;
    }

}