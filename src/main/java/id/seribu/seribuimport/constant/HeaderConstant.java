package id.seribu.seribuimport.constant;

public class HeaderConstant {

    public static final String[] OPWILAYAH = new String[] {
        "id",
        "tahun",
        "kode_kpp",
        "nama_kpp",
        "kanwil",
        "provinsi",
        "pulau",
        "status_perkawinan",
        "jumlah_tanggungan",
        "jenis_op",
        "range_usia",
        "jenis_kelamin",
        "warga_negara",
        "jumlah",
        "created_by",
        "created_date"
    };

    public static final String[] OPWILAYAHKLU = new String[] {
        "id",
        "tahun",
        "kode_kpp",
        "nama_kpp",
        "kanwil",
        "provinsi",
        "pulau",
        "kode_kategori",
        "nama_kategori",
        "jumlah",
        "created_by",
        "created_date"
    };

    public static final String[] OPKLU = new String[] {
        "id",
        "tahun",
        "kode_kategori",
        "nama_kategori",
        "status_perkawinan",
        "jumlah_tanggungan",
        "jenis_op",
        "range_usia",
        "jenis_kelamin",
        "warga_negara",
        "jumlah",
        "created_by",
        "created_date"
    };

    public static final String[] OPSTATUS = new String[] {
        "id",
        "kode_kpp",
        "nama_kpp",
        "kanwil",
        "provinsi",
        "pulau",
        "kode_kategori",
        "nama_kategori",
        "status_npwp",
        "status_pkp",
        "metode_pembukuan",
        "jumlah",
        "created_by",
        "created_date"
    };

    public static final String[] BENDAHARAALL = new String[] {
        "id",
        "tahun",
        "kode_kpp",
        "nama_kpp",
        "kanwil",
        "provinsi",
        "pulau",
        "jenis",
        "jumlah",
        "created_by",
        "created_date"
    };

    public static final String[] BENDAHARASTATUS = new String[] {
        "id",
        "tahun",
        "kode_kpp",
        "nama_kpp",
        "kanwil",
        "provinsi",
        "pulau",
        "jenis",
        "status_npwp",
        "status_pkp",
        "jumlah",
        "created_by",
        "created_date"
    };

    public static final String[] BADANALL = new String[] {
        "id",
        "tahun",
        "kode_kpp",
        "nama_kpp",
        "kanwil",
        "provinsi",
        "pulau",
        "kode_kategori",
        "nama_kategori",
        "kategori_wp",
        "modal",
        "status_pusat",
        "periode_pembukuan",
        "tahun_pendirian",
        "badan_hukum",
        "jumlah",
        "created_by",
        "created_date"
    };

    public static final String[] BADANSTATUS = new String[] {
        "id",
        "tahun",
        "kode_kpp",
        "nama_kpp",
        "kanwil",
        "provinsi",
        "pulau",
        "kode_kategori",
        "nama_kategori",
        "kategori_wp",
        "modal",
        "status_pusat",
        "periode_pembukuan",
        "tahun_pendirian",
        "badan_hukum",
        "status_npwp",
        "status_pkp",
        "jumlah",
        "created_by",
        "created_date"
    };
}