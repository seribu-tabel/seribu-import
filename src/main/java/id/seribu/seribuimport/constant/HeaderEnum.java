package id.seribu.seribuimport.constant;

public enum HeaderEnum {
    tahun("Tahun"),
	kodeKpp("Kode KPP"),
	namaKpp("Nama KPP"),
	kanwil("Kanwil"),
	provinsi("Provinsi"),
	pulau("Pulau"),
	kodeKategori("Kode Kategori"),
	namaKategori("Nama Kategori"),
	statusPerkawinan("Status Perkawinan"),
	jumlahTanggungan("Jumlah Tanggungan"),
	jenisOp("Jenis OP"),
	rangeUsia("Range Usia"),
	jenisKelamin("Jenis Kelamin"),
	wargaNegara("Warga Negara"),
	kategoriWp("Kategori WP"),
	jenisWp("Jenis WP"),
	modal("Modal"),
	statusPusat("Status Pusat"),
	periodePembukuan("Periode Pembukuan"),
	tahunPendirian("Tahun Pendirian"),
	badanHukum("Badan Hukum"),
	jenis("Jenis"),
	statusNpwp("Status NPWP"),
	statusPkp("Status PKP"),
	metodePembukuan("Metode Pembukuan"),
    jumlah("Jumlah");
    
    private String text;

    private HeaderEnum(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}