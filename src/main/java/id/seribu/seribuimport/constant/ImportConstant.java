package id.seribu.seribuimport.constant;

public class ImportConstant {

    public static final String DEMOGRAFI = "demografi";
    public static final String KEPATUHAN = "kepatuhan";
    public static final String PENERIMAAN = "penerimaan";
    public static final String PATH_PREFIX = "/";
    public static final String SYSPARAM = "SYSPARAM";
    public static final String PROCESSEDPATH = "/processed";
    public static final int COMBINATION = 6;
    public static final String DATEFORMAT = "dd-MMM-yyyy";
    public static final String SHEETNAME = "Wizard Data";
    public static final String JUMLAH = "jumlah";
    public static final String TOTALFIELD = "TOTAL";
    public static final String XLSXTYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String EXCEL_FORMAT_DATE = "dd-MMM-yyyy";

    public class DataSetPath {
        public static final String DEMOGRAFI = "/demografi";
        public static final String KEPATUHAN = "/kepatuhan";
        public static final String PENERIMAAN = "/penerimaan";
    }

    public class SubyekData {
        public static final String OP = "op";
        public static final String BADAN = "badan";
        public static final String BENDAHARA = "bendahara";
        public static final String UMUM = "umum";
    }

    public class SourceData {
        public static final String WILAYAH = "wilayah";
        public static final String WILAYAHKLU = "wilayahKlu";
        public static final String KLU = "klu";
        public static final String STATUS = "status";
        public static final String SEMUA = "semua";
    }

    public class Header {
        public static final String TAHUN = "TAHUN";
	    public static final String KODEKPP = "KODE_KPP";
	    public static final String NAMAKPP = "NAMA_KPP";
	    public static final String KANWIL = "KANWIL";
	    public static final String PROVINSI = "PROVINSI";
	    public static final String PULAU = "PULAU";
	    public static final String KODEKATEGORI = "KODE_KATEGORI";
	    public static final String NAMAKATEGORI = "NAMA_KATEGORI";
	    public static final String STATUSPERKAWINAN = "STATUS_PERKAWINAN";
	    public static final String JUMLAHTANGGUNGAN = "JUMLAH_TANGGUNGAN";
	    public static final String JENISOP = "JENIS_OP";
	    public static final String RANGEUSIA = "RANGE_USIA";
	    public static final String JENISKELAMIN = "JENIS_KELAMIN";
	    public static final String WARGANEGARA = "WARGA_NEGARA";
	    public static final String STATUSNPWP = "STATUS_NPWP";
	    public static final String STATUSPKP = "STATUS_PKP";
        public static final String METODEPEMBUKUAN = "METODE_PEMBUKUAN";
        public static final String MODAL = "MODAL";
        public static final String STATUSPUSAT = "STATUS_PUSAT";
        public static final String PERIODEPEMBUKUAN = "PERIODE_PEMBUKUAN";
        public static final String TAHUNPENDIRIAN = "TAHUN_PENDIRIAN";
        public static final String BADANHUKUM = "BADAN_HUKUM";
        public static final String KATEGORIWP = "KATEGORI_WP";
        public static final String JENISWP = "JENIS_WP";
        public static final String JENIS = "JENIS";
	    public static final String JUMLAH = "JUMLAH";
    }
}