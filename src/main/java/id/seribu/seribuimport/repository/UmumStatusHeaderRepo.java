package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.UmumStatusHeader;

@Repository
public interface UmumStatusHeaderRepo extends CrudRepository<UmumStatusHeader, String> {

}