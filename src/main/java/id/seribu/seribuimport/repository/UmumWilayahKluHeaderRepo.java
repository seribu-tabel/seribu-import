package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.UmumWilayahKluHeader;

@Repository
public interface UmumWilayahKluHeaderRepo extends CrudRepository<UmumWilayahKluHeader, String> {

}