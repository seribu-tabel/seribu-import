package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TDemoUmumStatus;

@Repository
public interface DemoUmumStatusRepo extends CrudRepository<TDemoUmumStatus, UUID> {

    List<TDemoUmumStatus> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_demo_umum_status", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_demo_umum_status limit 1", nativeQuery = true)
    List<TDemoUmumStatus> validateAvailable();
}