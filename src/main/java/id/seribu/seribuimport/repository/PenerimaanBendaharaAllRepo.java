package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TPenerimaanBendaharaAll;

@Repository
public interface PenerimaanBendaharaAllRepo extends CrudRepository<TPenerimaanBendaharaAll, UUID> {
    
    List<TPenerimaanBendaharaAll> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_penerimaan_bendahara_all", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_penerimaan_bendahara_all limit 1", nativeQuery = true)
    List<TPenerimaanBendaharaAll> validateAvailable();
}