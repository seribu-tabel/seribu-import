package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.BendaharaAllHeader;

@Repository
public interface BendaharaAllHeaderRepo extends CrudRepository<BendaharaAllHeader, String> {

}