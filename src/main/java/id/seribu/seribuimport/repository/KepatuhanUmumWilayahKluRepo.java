package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanUmumWilayahKlu;


@Repository
public interface KepatuhanUmumWilayahKluRepo extends CrudRepository<TKepatuhanUmumWilayahKlu, UUID> {

    List<TKepatuhanUmumWilayahKlu> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_umum_wilayah_klu", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_umum_wilayah_klu limit 1", nativeQuery = true)
    List<TKepatuhanUmumWilayahKlu> validateAvailable();
}