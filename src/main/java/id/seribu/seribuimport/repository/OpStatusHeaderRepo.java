package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.OpStatusHeader;

@Repository
public interface OpStatusHeaderRepo extends CrudRepository<OpStatusHeader, String> {

}