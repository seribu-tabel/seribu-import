package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TDemoBadanStatus;

@Repository
public interface DemoBadanStatusRepo extends CrudRepository<TDemoBadanStatus, UUID> {

    List<TDemoBadanStatus> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_demo_badan_status", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_demo_badan_status limit 1", nativeQuery = true)
    List<TDemoBadanStatus> validateAvailable();
}