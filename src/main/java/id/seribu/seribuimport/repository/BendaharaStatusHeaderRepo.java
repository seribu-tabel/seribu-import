package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.BendaharaStatusHeader;

@Repository
public interface BendaharaStatusHeaderRepo extends CrudRepository<BendaharaStatusHeader, String> {

}