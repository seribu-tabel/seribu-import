package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.BadanStatusHeader;

@Repository
public interface BadanStatusHeaderRepo extends CrudRepository<BadanStatusHeader, String> {

}