package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.OpKluHeader;

@Repository
public interface OpKluHeaderRepo extends CrudRepository<OpKluHeader, String> {

}