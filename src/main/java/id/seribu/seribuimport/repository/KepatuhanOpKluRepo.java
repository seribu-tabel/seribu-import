package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanOpKlu;

@Repository
public interface KepatuhanOpKluRepo extends CrudRepository<TKepatuhanOpKlu, UUID> {
    
    List<TKepatuhanOpKlu> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_op_klu", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_op_klu limit 1", nativeQuery = true)
    List<TKepatuhanOpKlu> validateAvailable();
}