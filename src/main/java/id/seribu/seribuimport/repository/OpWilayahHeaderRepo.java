package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.OpWilayahHeader;

@Repository
public interface OpWilayahHeaderRepo extends CrudRepository<OpWilayahHeader, String> {

}