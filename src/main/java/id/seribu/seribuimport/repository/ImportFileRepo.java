package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TImportFile;

@Repository
public interface ImportFileRepo extends CrudRepository<TImportFile, UUID> {

    @Query(value = "select *from t_import_file "
        + "where process = false "
        + "and dataset = :dataset "
        + "and subyek_data = :subyekdata "
        + "order by upload_date desc", nativeQuery = true)
    List<TImportFile> findByDatasetNotProcess(@Param("dataset") String dataset, @Param("subyekdata") String subyekdata);

    @Query(value = "select *from t_import_file "
        + "where process = false and filename = :filename", nativeQuery = true)
    List<TImportFile> findByFilename(@Param("filename") String filename);

    @Modifying
    @Query(value = "delete from t_import_file where filename = :filename and process = false", nativeQuery = true)
    void deleteByFileName(@Param("filename") String filename);
}