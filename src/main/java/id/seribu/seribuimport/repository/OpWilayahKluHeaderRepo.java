package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.OpWilayahKluHeader;

@Repository
public interface OpWilayahKluHeaderRepo extends CrudRepository<OpWilayahKluHeader, String> {

}