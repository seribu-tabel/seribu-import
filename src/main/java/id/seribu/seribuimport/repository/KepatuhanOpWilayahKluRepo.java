package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanOpWilayahKlu;

@Repository
public interface KepatuhanOpWilayahKluRepo extends CrudRepository<TKepatuhanOpWilayahKlu, UUID> {

    List<TKepatuhanOpWilayahKlu> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_op_wilayah_klu", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_op_wilayah_klu limit 1", nativeQuery = true)
    List<TKepatuhanOpWilayahKlu> validateAvailable();
}