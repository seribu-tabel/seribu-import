package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TDemoOpWilayah;

@Repository
public interface DemoOpWilayahRepo extends CrudRepository<TDemoOpWilayah, UUID> {
    
    List<TDemoOpWilayah> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_demo_op_wilayah", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_demo_op_wilayah limit 1", nativeQuery = true)
    List<TDemoOpWilayah> validateAvailable();
}