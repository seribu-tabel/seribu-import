package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanOpWilayah;

@Repository
public interface KepatuhanOpWilayahRepo extends CrudRepository<TKepatuhanOpWilayah, UUID> {
    
    List<TKepatuhanOpWilayah> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_op_wilayah", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_op_wilayah limit 1", nativeQuery = true)
    List<TKepatuhanOpWilayah> validateAvailable();
}