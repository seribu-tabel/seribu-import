package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanBendaharaAll;

@Repository
public interface KepatuhanBendaharaAllRepo extends CrudRepository<TKepatuhanBendaharaAll, UUID> {
    
    List<TKepatuhanBendaharaAll> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_bendahara_all", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_bendahara_all limit 1", nativeQuery = true)
    List<TKepatuhanBendaharaAll> validateAvailable();

}