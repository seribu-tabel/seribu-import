package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanOpStatus;

@Repository
public interface KepatuhanOpStatusRepo extends CrudRepository<TKepatuhanOpStatus, UUID> {
    
    List<TKepatuhanOpStatus> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_op_status", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_op_status limit 1", nativeQuery = true)
    List<TKepatuhanOpStatus> validateAvailable();
}