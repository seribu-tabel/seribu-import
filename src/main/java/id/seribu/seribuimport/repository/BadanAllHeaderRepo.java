package id.seribu.seribuimport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.header.BadanAllHeader;

@Repository
public interface BadanAllHeaderRepo extends CrudRepository<BadanAllHeader, String> {

}