package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanUmumStatus;


@Repository
public interface KepatuhanUmumStatusRepo extends CrudRepository<TKepatuhanUmumStatus, UUID> {

    List<TKepatuhanUmumStatus> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_umum_status", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_umum_status limit 1", nativeQuery = true)
    List<TKepatuhanUmumStatus> validateAvailable();
}