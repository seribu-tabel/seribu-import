package id.seribu.seribuimport.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TKepatuhanBadanAll;

@Repository
public interface KepatuhanBadanAllRepo extends CrudRepository<TKepatuhanBadanAll, UUID> {
 
    List<TKepatuhanBadanAll> findByTahunIn(List<String> tahun);

    @Modifying
    @Query(value = "delete from t_kepatuhan_badan_all", nativeQuery = true)
    void truncateData();

    @Query(value = "select * from t_kepatuhan_badan_all limit 1", nativeQuery = true)
    List<TKepatuhanBadanAll> validateAvailable();
}