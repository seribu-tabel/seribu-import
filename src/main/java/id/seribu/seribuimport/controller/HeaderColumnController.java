package id.seribu.seribuimport.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribuimport.service.DemografiService;
import id.seribu.seribuimport.service.HeaderColumnService;
import id.seribu.seribuimport.service.KepatuhanService;
import id.seribu.seribuimport.service.PenerimaanService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class HeaderColumnController {

    @Autowired
    HeaderColumnService headerColumnService;

    @Autowired
    DemografiService demografiService;

    @Autowired 
    KepatuhanService kepatuhanService;

    @Autowired
    PenerimaanService penerimaanService;

    BigDecimal grandTotal = BigDecimal.ZERO;
    BigDecimal totalDemografi = BigDecimal.ZERO;
    BigDecimal totalKepatuhan = BigDecimal.ZERO;
    BigDecimal totalPenerimaan = BigDecimal.ZERO;

    @GetMapping(path = "${application.endpoints.import-service.get-header-column}")
    public ApiResponse<List<Lov>> getHeaderColumn(@PathVariable("subyekData") String subyekData,
            @PathVariable("sourceData") String sourceData) {
        log.info("HeaderColumnController.getHeaderColumn subyekData: {}, sourceData: ", subyekData, sourceData);
        return new ApiResponse<>(HttpStatus.OK.value(), "lov header column",
                headerColumnService.getHeaderColumn(subyekData, sourceData));
    }

    @GetMapping(path = "${application.endpoints.import-service.get-total-demografi}")
    public ApiResponse<BigDecimal> getTotalDemografi() {
        totalDemografi = demografiService.getTotalDemografi();
        return new ApiResponse<>(HttpStatus.OK.value(), "total pivot data demografi", totalDemografi);
    }

    @GetMapping(path = "${application.endpoints.import-service.get-total-kepatuhan}")
    public ApiResponse<BigDecimal> getTotalKepatuhan() {
        totalKepatuhan = kepatuhanService.getTotalKepatuhan();
        return new ApiResponse<>(HttpStatus.OK.value(), "total pivot data kepatuhan", totalKepatuhan);
    }

    @GetMapping(path = "${application.endpoints.import-service.get-total-penerimaan}")
    public ApiResponse<BigDecimal> getTotalPenerimaan() {
        totalPenerimaan = penerimaanService.getTotalPenerimaan();
        return new ApiResponse<>(HttpStatus.OK.value(), "total pivot data penerimaan", totalPenerimaan);
    }

    @GetMapping(path = "${application.endpoints.import-service.get-grand-total}")
    public ApiResponse<BigDecimal> getAllTotal() {
        log.info("HeaderColumnController.getAllTotal import");
        grandTotal = totalDemografi.add(totalKepatuhan).add(totalPenerimaan);
        return new ApiResponse<>(HttpStatus.OK.value(), "total pivot whole data", grandTotal);
    }

}