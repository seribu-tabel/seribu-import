package id.seribu.seribuimport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.WizardRequestDto;
import id.seribu.seribuimport.service.WizardService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class PivotController {

    @Autowired
    WizardService wizardService;

    @PostMapping(path = "${application.endpoints.import-service.wizard-download}", 
        consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource> wizardDownload(@RequestBody WizardRequestDto requestDto) {
        log.info("PivotController.wizardDownload request: {}", requestDto.toString());
        DownloadResponseDto responseDto = wizardService.getExcelFile(requestDto);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(responseDto.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + responseDto.getResource().getFilename() + "\"")
                .body(responseDto.getResource());
    }
}