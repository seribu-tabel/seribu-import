package id.seribu.seribuimport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.ImportResponseDto;
import id.seribu.seribuimport.service.UploadService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ImportController {

    @Autowired
    UploadService uploadService;

    @PostMapping(path = "${application.endpoints.import-service.import-file}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ApiResponse<ImportResponseDto> importExcel(@RequestParam("file") MultipartFile file,
            @RequestParam("dataset") String dataSet, @RequestParam("subyek") String subyekData,
            @RequestParam("sourcedata") String sourceData, @RequestParam("uploadedBy") String uploadedBy) {
        log.info("UploadController.uploadFile...");
        return uploadService.processFile(file, dataSet, subyekData, sourceData, uploadedBy);

    }
}